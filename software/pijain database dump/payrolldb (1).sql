-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2013 at 12:28 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `payrolldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `callowancedetail`
--

CREATE TABLE IF NOT EXISTS `callowancedetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `typeOfAllowance` int(1) NOT NULL COMMENT '1- calculation 2- fixed',
  `calcFormula` int(2) DEFAULT NULL COMMENT 'calculation formula 1-9',
  `bonus` int(1) NOT NULL COMMENT '1- yes 0- no',
  `ot` int(11) NOT NULL COMMENT '1- yes 0 - no',
  `esi` tinyint(1) DEFAULT NULL,
  `epf` tinyint(1) DEFAULT NULL,
  `eps` tinyint(1) DEFAULT NULL,
  `pTax` tinyint(1) DEFAULT NULL,
  `oTax` int(11) NOT NULL COMMENT 'other tax/act 1-yes 0- no',
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company Dynamic Deduction Rules' AUTO_INCREMENT=97 ;

--
-- Dumping data for table `callowancedetail`
--

INSERT INTO `callowancedetail` (`id`, `clientId`, `name`, `typeOfAllowance`, `calcFormula`, `bonus`, `ot`, `esi`, `epf`, `eps`, `pTax`, `oTax`, `isActive`, `inActiveDate`) VALUES
(27, 2, 'BASIC', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(52, 2, 'Others', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(53, 2, 'Medical', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(54, 2, 'CCA', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(55, 2, 'SPL', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(56, 2, 'EDU', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(57, 2, 'LUNCH', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(58, 2, 'CONVENCE', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(59, 2, 'HRA', 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL),
(96, 1, 'BASIC', 1, 0, 0, 0, 1, 1, 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carea_code`
--

CREATE TABLE IF NOT EXISTS `carea_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_code` int(11) NOT NULL,
  `area_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cbankdetails`
--

CREATE TABLE IF NOT EXISTS `cbankdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL COMMENT 'Foreign key from clientdetails',
  `bankName` varchar(30) DEFAULT NULL,
  `accountNumber` varchar(20) DEFAULT NULL,
  `branchName` varchar(50) DEFAULT NULL,
  `micrCode` varchar(50) DEFAULT NULL,
  `ifscCode` varchar(50) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bankdetails` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Client Bank Details' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cbonusrules`
--

CREATE TABLE IF NOT EXISTS `cbonusrules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `percent` double DEFAULT NULL COMMENT '8.33 to 20',
  `maxWageLimit` int(7) DEFAULT NULL COMMENT '10000',
  `wageLimit` int(7) DEFAULT NULL COMMENT '3500',
  `bonusToAll` tinyint(1) DEFAULT NULL COMMENT 'Y - All workers, N - As per rule',
  `mly/yly` varchar(1) DEFAULT NULL COMMENT 'M - Monthly basis, Y - Yearly Basis',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Company Bonus Rules' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ccontractor`
--

CREATE TABLE IF NOT EXISTS `ccontractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL COMMENT 'foreign key from clientdetails',
  `groupId` int(11) DEFAULT NULL COMMENT 'company group id ',
  `name` varchar(50) DEFAULT NULL,
  `sName` varchar(20) DEFAULT NULL COMMENT 'Short Name',
  `address` varchar(500) DEFAULT NULL,
  `registrationNumber` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile1` varchar(15) DEFAULT NULL,
  `mobile2` varchar(15) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_contractor` (`clientId`),
  KEY `FK_contractor_branch` (`groupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ccontractor`
--

INSERT INTO `ccontractor` (`id`, `clientId`, `groupId`, `name`, `sName`, `address`, `registrationNumber`, `email`, `mobile1`, `mobile2`, `startDate`, `isActive`, `inActiveDate`) VALUES
(1, 1, 0, 'Devev', 'ds', '', '', '', '', '', NULL, NULL, '2013-06-10 13:40:23');

-- --------------------------------------------------------

--
-- Table structure for table `cdedfixed`
--

CREATE TABLE IF NOT EXISTS `cdedfixed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `adv` varchar(12) DEFAULT NULL,
  `ded` varchar(12) DEFAULT NULL,
  `fine` varchar(12) DEFAULT NULL,
  `tds` varchar(12) DEFAULT NULL,
  `cded` varchar(12) DEFAULT NULL,
  `loan` varchar(12) DEFAULT NULL,
  `ded1` varchar(12) DEFAULT NULL,
  `ded2` varchar(12) DEFAULT NULL,
  `ded3` varchar(12) DEFAULT NULL,
  `ded4` varchar(12) DEFAULT NULL,
  `ded5` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Company Fixed Deduction Rules' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cdegmaster`
--

CREATE TABLE IF NOT EXISTS `cdegmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT 'designation',
  `type` varchar(20) DEFAULT NULL COMMENT 'Skilled,Un Skilled etc',
  `mCategory` varchar(2) DEFAULT NULL COMMENT 'A,B,C,D,E(Fact Act)',
  `fCategory` varchar(2) DEFAULT NULL COMMENT 'F,G,H,I,J(Fact Act)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Predefined Designations' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cdeptmaster`
--

CREATE TABLE IF NOT EXISTS `cdeptmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL COMMENT 'Foreign Key from Client details',
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `sName` varchar(20) DEFAULT NULL COMMENT 'Short Name',
  `startDate` date DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_department_client` (`clientId`),
  KEY `FK_department_branch` (`groupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `cdeptmaster`
--

INSERT INTO `cdeptmaster` (`id`, `clientId`, `groupId`, `name`, `sName`, `startDate`, `inActiveDate`, `isActive`) VALUES
(1, 6, 0, 'sas', 'Deveasnasasjsahsas', '0000-00-00', NULL, NULL),
(2, 6, 0, 'bns', 'bn', '0000-00-00', NULL, NULL),
(3, 6, 0, 'nsban', 'nbsn', '0000-00-00', NULL, NULL),
(4, 4, 0, 'jhsj', 'h', '0000-00-00', NULL, NULL),
(5, 4, 0, 'ssqhjsj', 'sj', '0000-00-00', NULL, NULL),
(6, 5, 0, 'mshaj', 'hsj', '0000-00-00', NULL, NULL),
(7, 5, 0, 'jshj', 'jshj', '0000-00-00', NULL, NULL),
(8, 5, 0, 'xznzxz', '', '0000-00-00', NULL, NULL),
(9, 1, 0, 'Owner', 'OW', '0000-00-00', NULL, NULL),
(10, 1, 0, 'Sweeper', 'SP', '0000-00-00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cesidetail`
--

CREATE TABLE IF NOT EXISTS `cesidetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `esiRuleId` int(11) DEFAULT NULL,
  `esiAddId` int(11) DEFAULT NULL,
  `esiNo` int(11) NOT NULL,
  `esiTDate` date DEFAULT NULL,
  `esiFDate` date DEFAULT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `esiDownloadPath` varchar(100) DEFAULT NULL,
  `insptrName` varchar(30) DEFAULT NULL,
  `inspDate` date DEFAULT NULL,
  `ecPeriod` varchar(50) DEFAULT NULL,
  `lcPeriod` varchar(50) DEFAULT NULL,
  `inspLNo` varchar(20) DEFAULT NULL,
  `replyNo` varchar(20) DEFAULT NULL,
  `replyDate` date DEFAULT NULL,
  `challanAmt` varchar(20) DEFAULT NULL,
  `challanNo` varchar(20) DEFAULT NULL,
  `cDepDate` date DEFAULT NULL,
  `esiInspPath` varchar(100) DEFAULT NULL,
  `brEsiNoYN` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_esidetail` (`clientId`),
  KEY `FK_esidetail_esiaddress` (`esiAddId`),
  KEY `FK_esidetail_esirule` (`esiRuleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company ESI Details' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cesidetail`
--

INSERT INTO `cesidetail` (`id`, `clientId`, `groupId`, `esiRuleId`, `esiAddId`, `esiNo`, `esiTDate`, `esiFDate`, `userId`, `password`, `esiDownloadPath`, `insptrName`, `inspDate`, `ecPeriod`, `lcPeriod`, `inspLNo`, `replyNo`, `replyDate`, `challanAmt`, `challanNo`, `cDepDate`, `esiInspPath`, `brEsiNoYN`) VALUES
(1, 1, NULL, NULL, NULL, 1000000, '0000-00-00', '0000-00-00', '', '', '', '', '0000-00-00', '', '', '', '', '0000-00-00', '', '', '0000-00-00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cfactactdetail`
--

CREATE TABLE IF NOT EXISTS `cfactactdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `nature` varchar(15) DEFAULT NULL COMMENT 'All details of table as per form no 2',
  `regNo` varchar(20) DEFAULT NULL,
  `renDate` date DEFAULT NULL,
  `renFrm` date DEFAULT NULL,
  `renTo` date DEFAULT NULL,
  `expDate` date DEFAULT NULL,
  `maxNoWrkr` int(7) DEFAULT NULL COMMENT 'Maximum No Of Workers proposed to be employed on any day during the year',
  `maxNoWrkrLastYr` int(7) DEFAULT NULL COMMENT 'Maximum no of workers employed on any one day during last 12 months',
  `avgWrkr` int(7) DEFAULT NULL COMMENT 'No to be ordinaryily employed in factory',
  `hpConn` varchar(10) DEFAULT NULL,
  `mapAprvDetail` varchar(200) DEFAULT NULL,
  `typeOfPremis` varchar(15) DEFAULT NULL,
  `ownerDetail` varchar(200) DEFAULT NULL COMMENT 'premis owner details name and address',
  `authName` varchar(100) DEFAULT NULL COMMENT 'Waste disposal approval authority',
  `ylyLFee` varchar(15) DEFAULT NULL,
  `totFee` varchar(15) DEFAULT NULL COMMENT '1 - Single, 2 - Double, 3 - Tripple',
  `dupLFee` varchar(10) DEFAULT NULL,
  `diffFee` varchar(10) DEFAULT NULL,
  `noOfConn` varchar(10) DEFAULT NULL,
  `ddDetail` varchar(200) DEFAULT NULL COMMENT 'DDNo, Bank,Date,Amount',
  `insptrName` varchar(20) DEFAULT NULL,
  `inspDate` date DEFAULT NULL,
  `inspLNO` varchar(30) DEFAULT NULL,
  `replyNo` varchar(20) DEFAULT NULL,
  `replyDate` date DEFAULT NULL,
  `factInspPath` varchar(100) DEFAULT NULL,
  `shift1` varchar(50) DEFAULT NULL,
  `shift2` varchar(50) DEFAULT NULL,
  `shift3` varchar(50) DEFAULT NULL,
  `generalShift` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_factoryactdetail` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Company Factory Act Details' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  `city_state` varchar(100) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1624 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `city_name`, `city_state`) VALUES
(1, 'Kolhapur', 'Maharashtra'),
(2, 'Port Blair', 'Andaman and Nicobar Islands'),
(3, 'Adilabad', 'Andhra Pradesh'),
(4, 'Adoni', 'Andhra Pradesh'),
(5, 'Amadalavalasa', 'Andhra Pradesh'),
(6, 'Amalapuram', 'Andhra Pradesh'),
(7, 'Anakapalle', 'Andhra Pradesh'),
(8, 'Anantapur', 'Andhra Pradesh'),
(9, 'Badepalle', 'Andhra Pradesh'),
(10, 'Banganapalle', 'Andhra Pradesh'),
(11, 'Bapatla', 'Andhra Pradesh'),
(12, 'Bellampalle', 'Andhra Pradesh'),
(13, 'Bethamcherla', 'Andhra Pradesh'),
(14, 'Bhadrachalam', 'Andhra Pradesh'),
(15, 'Bhainsa', 'Andhra Pradesh'),
(16, 'Bheemunipatnam', 'Andhra Pradesh'),
(17, 'Bhimavaram', 'Andhra Pradesh'),
(18, 'Bhongir', 'Andhra Pradesh'),
(19, 'Bobbili', 'Andhra Pradesh'),
(20, 'Bodhan', 'Andhra Pradesh'),
(21, 'Chilakaluripet', 'Andhra Pradesh'),
(22, 'Chirala', 'Andhra Pradesh'),
(23, 'Chittoor', 'Andhra Pradesh'),
(24, 'Cuddapah', 'Andhra Pradesh'),
(25, 'Devarakonda', 'Andhra Pradesh'),
(26, 'Dharmavaram', 'Andhra Pradesh'),
(27, 'Eluru', 'Andhra Pradesh'),
(28, 'Farooqnagar', 'Andhra Pradesh'),
(29, 'Gadwal', 'Andhra Pradesh'),
(30, 'Gooty', 'Andhra Pradesh'),
(31, 'Gudivada', 'Andhra Pradesh'),
(32, 'Gudur', 'Andhra Pradesh'),
(33, 'Guntakal', 'Andhra Pradesh'),
(34, 'Guntur', 'Andhra Pradesh'),
(35, 'Hanuman Junction', 'Andhra Pradesh'),
(36, 'Hindupur', 'Andhra Pradesh'),
(37, 'Hyderabad', 'Andhra Pradesh'),
(38, 'Ichchapuram', 'Andhra Pradesh'),
(39, 'Jaggaiahpet', 'Andhra Pradesh'),
(40, 'Jagtial', 'Andhra Pradesh'),
(41, 'Jammalamadugu', 'Andhra Pradesh'),
(42, 'Jangaon', 'Andhra Pradesh'),
(43, 'Kadapa', 'Andhra Pradesh'),
(44, 'Kadiri', 'Andhra Pradesh'),
(45, 'Kagaznagar', 'Andhra Pradesh'),
(46, 'Kakinada', 'Andhra Pradesh'),
(47, 'Kalyandurg', 'Andhra Pradesh'),
(48, 'Kamareddy', 'Andhra Pradesh'),
(49, 'Kandukur', 'Andhra Pradesh'),
(50, 'Karimnagar', 'Andhra Pradesh'),
(51, 'Kavali', 'Andhra Pradesh'),
(52, 'Khammam', 'Andhra Pradesh'),
(53, 'Koratla', 'Andhra Pradesh'),
(54, 'Kothagudem', 'Andhra Pradesh'),
(55, 'Kothapeta', 'Andhra Pradesh'),
(56, 'Kovvur', 'Andhra Pradesh'),
(57, 'Kurnool', 'Andhra Pradesh'),
(58, 'Kyathampalle', 'Andhra Pradesh'),
(59, 'Macherla', 'Andhra Pradesh'),
(60, 'Machilipatnam', 'Andhra Pradesh'),
(61, 'Madanapalle', 'Andhra Pradesh'),
(62, 'Mahbubnagar', 'Andhra Pradesh'),
(63, 'Mancherial', 'Andhra Pradesh'),
(64, 'Mandamarri', 'Andhra Pradesh'),
(65, 'Mandapeta', 'Andhra Pradesh'),
(66, 'Manuguru', 'Andhra Pradesh'),
(67, 'Markapur', 'Andhra Pradesh'),
(68, 'Medak', 'Andhra Pradesh'),
(69, 'Miryalaguda', 'Andhra Pradesh'),
(70, 'Mogalthur', 'Andhra Pradesh'),
(71, 'Nagari', 'Andhra Pradesh'),
(72, 'Nagarkurnool', 'Andhra Pradesh'),
(73, 'Nandyal', 'Andhra Pradesh'),
(74, 'Narasapur', 'Andhra Pradesh'),
(75, 'Narasaraopet', 'Andhra Pradesh'),
(76, 'Narayanpet', 'Andhra Pradesh'),
(77, 'Narsipatnam', 'Andhra Pradesh'),
(78, 'Nellore', 'Andhra Pradesh'),
(79, 'Nidadavole', 'Andhra Pradesh'),
(80, 'Nirmal', 'Andhra Pradesh'),
(81, 'Nizamabad', 'Andhra Pradesh'),
(82, 'Nuzvid', 'Andhra Pradesh'),
(83, 'Ongole', 'Andhra Pradesh'),
(84, 'Palacole', 'Andhra Pradesh'),
(85, 'Palasa Kasibugga', 'Andhra Pradesh'),
(86, 'Palwancha', 'Andhra Pradesh'),
(87, 'Parvathipuram', 'Andhra Pradesh'),
(88, 'Pedana', 'Andhra Pradesh'),
(89, 'Peddapuram', 'Andhra Pradesh'),
(90, 'Pithapuram', 'Andhra Pradesh'),
(91, 'Pondur', 'Andhra pradesh'),
(92, 'Ponnur', 'Andhra Pradesh'),
(93, 'Proddatur', 'Andhra Pradesh'),
(94, 'Punganur', 'Andhra Pradesh'),
(95, 'Puttur', 'Andhra Pradesh'),
(96, 'Rajahmundry', 'Andhra Pradesh'),
(97, 'Rajam', 'Andhra Pradesh'),
(98, 'Ramachandrapuram', 'Andhra Pradesh'),
(99, 'Ramagundam', 'Andhra Pradesh'),
(100, 'Rayachoti', 'Andhra Pradesh'),
(101, 'Rayadurg', 'Andhra Pradesh'),
(102, 'Renigunta', 'Andhra Pradesh'),
(103, 'Repalle', 'Andhra Pradesh'),
(104, 'Sadasivpet', 'Andhra Pradesh'),
(105, 'Salur', 'Andhra Pradesh'),
(106, 'Samalkot', 'Andhra Pradesh'),
(107, 'Sangareddy', 'Andhra Pradesh'),
(108, 'Sattenapalle', 'Andhra Pradesh'),
(109, 'Siddipet', 'Andhra Pradesh'),
(110, 'Singapur', 'Andhra Pradesh'),
(111, 'Sircilla', 'Andhra Pradesh'),
(112, 'Srikakulam', 'Andhra Pradesh'),
(113, 'Srikalahasti', 'Andhra Pradesh'),
(115, 'Suryapet', 'Andhra Pradesh'),
(116, 'Tadepalligudem', 'Andhra Pradesh'),
(117, 'Tadpatri', 'Andhra Pradesh'),
(118, 'Tandur', 'Andhra Pradesh'),
(119, 'Tanuku', 'Andhra Pradesh'),
(120, 'Tenali', 'Andhra Pradesh'),
(121, 'Tirupati', 'Andhra Pradesh'),
(122, 'Tuni', 'Andhra Pradesh'),
(123, 'Uravakonda', 'Andhra Pradesh'),
(124, 'Venkatagiri', 'Andhra Pradesh'),
(125, 'Vicarabad', 'Andhra Pradesh'),
(126, 'Vijayawada', 'Andhra Pradesh'),
(127, 'Vinukonda', 'Andhra Pradesh'),
(128, 'Visakhapatnam', 'Andhra Pradesh'),
(129, 'Vizianagaram', 'Andhra Pradesh'),
(130, 'Wanaparthy', 'Andhra Pradesh'),
(131, 'Warangal', 'Andhra Pradesh'),
(132, 'Yellandu', 'Andhra Pradesh'),
(133, 'Yemmiganur', 'Andhra Pradesh'),
(134, 'Yerraguntla', 'Andhra Pradesh'),
(135, 'Zahirabad', 'Andhra Pradesh'),
(136, 'Rajampet', 'Andra Pradesh'),
(137, 'Along', 'Arunachal Pradesh'),
(138, 'Bomdila', 'Arunachal Pradesh'),
(139, 'Itanagar', 'Arunachal Pradesh'),
(140, 'Naharlagun', 'Arunachal Pradesh'),
(141, 'Pasighat', 'Arunachal Pradesh'),
(142, 'Abhayapuri', 'Assam'),
(143, 'Amguri', 'Assam'),
(144, 'Anandnagaar', 'Assam'),
(145, 'Barpeta', 'Assam'),
(146, 'Barpeta Road', 'Assam'),
(147, 'Bilasipara', 'Assam'),
(148, 'Bongaigaon', 'Assam'),
(149, 'Dhekiajuli', 'Assam'),
(150, 'Dhubri', 'Assam'),
(151, 'Dibrugarh', 'Assam'),
(152, 'Digboi', 'Assam'),
(153, 'Diphu', 'Assam'),
(154, 'Dispur', 'Assam'),
(156, 'Gauripur', 'Assam'),
(157, 'Goalpara', 'Assam'),
(158, 'Golaghat', 'Assam'),
(159, 'Guwahati', 'Assam'),
(160, 'Haflong', 'Assam'),
(161, 'Hailakandi', 'Assam'),
(162, 'Hojai', 'Assam'),
(163, 'Jorhat', 'Assam'),
(164, 'Karimganj', 'Assam'),
(165, 'Kokrajhar', 'Assam'),
(166, 'Lanka', 'Assam'),
(167, 'Lumding', 'Assam'),
(168, 'Mangaldoi', 'Assam'),
(169, 'Mankachar', 'Assam'),
(170, 'Margherita', 'Assam'),
(171, 'Mariani', 'Assam'),
(172, 'Marigaon', 'Assam'),
(173, 'Nagaon', 'Assam'),
(174, 'Nalbari', 'Assam'),
(175, 'North Lakhimpur', 'Assam'),
(176, 'Rangia', 'Assam'),
(177, 'Sibsagar', 'Assam'),
(178, 'Silapathar', 'Assam'),
(179, 'Silchar', 'Assam'),
(180, 'Tezpur', 'Assam'),
(181, 'Tinsukia', 'Assam'),
(182, 'Amarpur', 'Bihar'),
(183, 'Araria', 'Bihar'),
(184, 'Areraj', 'Bihar'),
(185, 'Arrah', 'Bihar'),
(186, 'Asarganj', 'Bihar'),
(187, 'Aurangabad', 'Bihar'),
(188, 'Bagaha', 'Bihar'),
(189, 'Bahadurganj', 'Bihar'),
(190, 'Bairgania', 'Bihar'),
(191, 'Bakhtiarpur', 'Bihar'),
(192, 'Banka', 'Bihar'),
(193, 'Banmankhi Bazar', 'Bihar'),
(194, 'Barahiya', 'Bihar'),
(195, 'Barauli', 'Bihar'),
(196, 'Barbigha', 'Bihar'),
(197, 'Barh', 'Bihar'),
(198, 'Begusarai', 'Bihar'),
(199, 'Behea', 'Bihar'),
(200, 'Bettiah', 'Bihar'),
(201, 'Bhabua', 'Bihar'),
(202, 'Bhagalpur', 'Bihar'),
(203, 'Bihar Sharif', 'Bihar'),
(204, 'Bikramganj', 'Bihar'),
(205, 'Bodh Gaya', 'Bihar'),
(206, 'Buxar', 'Bihar'),
(207, 'Chandan Bara', 'Bihar'),
(208, 'Chanpatia', 'Bihar'),
(209, 'Chhapra', 'Bihar'),
(210, 'Colgong', 'Bihar'),
(211, 'Dalsinghsarai', 'Bihar'),
(212, 'Darbhanga', 'Bihar'),
(213, 'Daudnagar', 'Bihar'),
(214, 'Dehri-on-Sone', 'Bihar'),
(215, 'Dhaka', 'Bihar'),
(216, 'Dighwara', 'Bihar'),
(217, 'Dumraon', 'Bihar'),
(218, 'Fatwah', 'Bihar'),
(219, 'Forbesganj', 'Bihar'),
(220, 'Gaya', 'Bihar'),
(221, 'Gogri Jamalpur', 'Bihar'),
(222, 'Gopalganj', 'Bihar'),
(223, 'Hajipur', 'Bihar'),
(224, 'Hilsa', 'Bihar'),
(225, 'Hisua', 'Bihar'),
(226, 'Islampur', 'Bihar'),
(227, 'Jagdispur', 'Bihar'),
(228, 'Jamalpur', 'Bihar'),
(229, 'Jamui', 'Bihar'),
(230, 'Jehanabad', 'Bihar'),
(231, 'Jhajha', 'Bihar'),
(232, 'Jhanjharpur', 'Bihar'),
(233, 'Jogabani', 'Bihar'),
(234, 'Kanti', 'Bihar'),
(235, 'Katihar', 'Bihar'),
(236, 'Khagaria', 'Bihar'),
(237, 'Kharagpur', 'Bihar'),
(238, 'Kishanganj', 'Bihar'),
(239, 'Lakhisarai', 'Bihar'),
(240, 'Lalganj', 'Bihar'),
(241, 'Madhepura', 'Bihar'),
(242, 'Madhubani', 'Bihar'),
(243, 'Maharajganj', 'Bihar'),
(244, 'Mahnar Bazar', 'Bihar'),
(245, 'Makhdumpur', 'Bihar'),
(246, 'Maner', 'Bihar'),
(247, 'Manihari', 'Bihar'),
(248, 'Marhaura', 'Bihar'),
(249, 'Masaurhi', 'Bihar'),
(250, 'Mirganj', 'Bihar'),
(251, 'Mokameh', 'Bihar'),
(252, 'Motihari', 'Bihar'),
(253, 'Motipur', 'Bihar'),
(254, 'Munger', 'Bihar'),
(255, 'Murliganj', 'Bihar'),
(256, 'Muzaffarpur', 'Bihar'),
(257, 'Narkatiaganj', 'Bihar'),
(258, 'Naugachhia', 'Bihar'),
(259, 'Nawada', 'Bihar'),
(260, 'Nokha', 'Bihar'),
(261, 'Patna', 'Bihar'),
(262, 'Piro', 'Bihar'),
(263, 'Purnia', 'Bihar'),
(264, 'Rafiganj', 'Bihar'),
(265, 'Rajgir', 'Bihar'),
(266, 'Ramnagar', 'Bihar'),
(267, 'Raxaul Bazar', 'Bihar'),
(268, 'Revelganj', 'Bihar'),
(269, 'Rosera', 'Bihar'),
(270, 'Saharsa', 'Bihar'),
(271, 'Samastipur', 'Bihar'),
(272, 'Sasaram', 'Bihar'),
(273, 'Sheikhpura', 'Bihar'),
(274, 'Sheohar', 'Bihar'),
(275, 'Sherghati', 'Bihar'),
(276, 'Silao', 'Bihar'),
(277, 'Sitamarhi', 'Bihar'),
(278, 'Siwan', 'Bihar'),
(279, 'Sonepur', 'Bihar'),
(280, 'Sugauli', 'Bihar'),
(281, 'Sultanganj', 'Bihar'),
(282, 'Supaul', 'Bihar'),
(283, 'Warisaliganj', 'Bihar'),
(284, 'Ahiwara', 'Chhattisgarh'),
(285, 'Akaltara', 'Chhattisgarh'),
(286, 'Ambagarh Chowki', 'Chhattisgarh'),
(287, 'Ambikapur', 'Chhattisgarh'),
(288, 'Arang', 'Chhattisgarh'),
(289, 'Bade Bacheli', 'Chhattisgarh'),
(290, 'Balod', 'Chhattisgarh'),
(291, 'Baloda Bazar', 'Chhattisgarh'),
(292, 'Bemetra', 'Chhattisgarh'),
(293, 'Bhatapara', 'Chhattisgarh'),
(294, 'Bilaspur', 'Chhattisgarh'),
(295, 'Birgaon', 'Chhattisgarh'),
(296, 'Champa', 'Chhattisgarh'),
(297, 'Chirmiri', 'Chhattisgarh'),
(298, 'Dalli-Rajhara', 'Chhattisgarh'),
(299, 'Dhamtari', 'Chhattisgarh'),
(300, 'Dipka', 'Chhattisgarh'),
(301, 'Dongargarh', 'Chhattisgarh'),
(302, 'Durg-Bhilai Nagar', 'Chhattisgarh'),
(303, 'Gobranawapara', 'Chhattisgarh'),
(304, 'Jagdalpur', 'Chhattisgarh'),
(305, 'Janjgir', 'Chhattisgarh'),
(306, 'Jashpurnagar', 'Chhattisgarh'),
(307, 'Kanker', 'Chhattisgarh'),
(308, 'Kawardha', 'Chhattisgarh'),
(309, 'Kondagaon', 'Chhattisgarh'),
(310, 'Korba', 'Chhattisgarh'),
(311, 'Mahasamund', 'Chhattisgarh'),
(312, 'Mahendragarh', 'Chhattisgarh'),
(313, 'Mungeli', 'Chhattisgarh'),
(314, 'Naila Janjgir', 'Chhattisgarh'),
(315, 'Raigarh', 'Chhattisgarh'),
(316, 'Raipur', 'Chhattisgarh'),
(317, 'Rajnandgaon', 'Chhattisgarh'),
(318, 'Sakti', 'Chhattisgarh'),
(319, 'Tilda Newra', 'Chhattisgarh'),
(320, 'Amli', 'Dadra & Nagar Haveli'),
(321, 'Silvassa', 'Dadra and Nagar Haveli'),
(322, 'Daman and Diu', 'Daman & Diu'),
(323, 'Daman and Diu', 'Daman & Diu'),
(324, 'Asola', 'Delhi'),
(325, 'Delhi', 'Delhi'),
(326, 'Aldona', 'Goa'),
(327, 'Curchorem Cacora', 'Goa'),
(328, 'Madgaon', 'Goa'),
(329, 'Mapusa', 'Goa'),
(330, 'Margao', 'Goa'),
(331, 'Marmagao', 'Goa'),
(332, 'Panaji', 'Goa'),
(333, 'Ahmedabad', 'Gujarat'),
(334, 'Amreli', 'Gujarat'),
(335, 'Anand', 'Gujarat'),
(336, 'Ankleshwar', 'Gujarat'),
(337, 'Bharuch', 'Gujarat'),
(338, 'Bhavnagar', 'Gujarat'),
(339, 'Bhuj', 'Gujarat'),
(340, 'Cambay', 'Gujarat'),
(341, 'Dahod', 'Gujarat'),
(342, 'Deesa', 'Gujarat'),
(343, '"Dharampur', ' India"'),
(344, 'Dholka', 'Gujarat'),
(345, 'Gandhinagar', 'Gujarat'),
(346, 'Godhra', 'Gujarat'),
(347, 'Himatnagar', 'Gujarat'),
(348, 'Idar', 'Gujarat'),
(349, 'Jamnagar', 'Gujarat'),
(350, 'Junagadh', 'Gujarat'),
(351, 'Kadi', 'Gujarat'),
(352, 'Kalavad', 'Gujarat'),
(353, 'Kalol', 'Gujarat'),
(354, 'Kapadvanj', 'Gujarat'),
(355, 'Karjan', 'Gujarat'),
(356, 'Keshod', 'Gujarat'),
(357, 'Khambhalia', 'Gujarat'),
(358, 'Khambhat', 'Gujarat'),
(359, 'Kheda', 'Gujarat'),
(360, 'Khedbrahma', 'Gujarat'),
(361, 'Kheralu', 'Gujarat'),
(362, 'Kodinar', 'Gujarat'),
(363, 'Lathi', 'Gujarat'),
(364, 'Limbdi', 'Gujarat'),
(365, 'Lunawada', 'Gujarat'),
(366, 'Mahesana', 'Gujarat'),
(367, 'Mahuva', 'Gujarat'),
(368, 'Manavadar', 'Gujarat'),
(369, 'Mandvi', 'Gujarat'),
(370, 'Mangrol', 'Gujarat'),
(371, 'Mansa', 'Gujarat'),
(372, 'Mehmedabad', 'Gujarat'),
(373, 'Modasa', 'Gujarat'),
(374, 'Morvi', 'Gujarat'),
(375, 'Nadiad', 'Gujarat'),
(376, 'Navsari', 'Gujarat'),
(377, 'Padra', 'Gujarat'),
(378, 'Palanpur', 'Gujarat'),
(379, 'Palitana', 'Gujarat'),
(380, 'Pardi', 'Gujarat'),
(381, 'Patan', 'Gujarat'),
(382, 'Petlad', 'Gujarat'),
(383, 'Porbandar', 'Gujarat'),
(384, 'Radhanpur', 'Gujarat'),
(385, 'Rajkot', 'Gujarat'),
(386, 'Rajpipla', 'Gujarat'),
(387, 'Rajula', 'Gujarat'),
(388, 'Ranavav', 'Gujarat'),
(389, 'Rapar', 'Gujarat'),
(390, 'Salaya', 'Gujarat'),
(391, 'Sanand', 'Gujarat'),
(392, 'Savarkundla', 'Gujarat'),
(393, 'Sidhpur', 'Gujarat'),
(394, 'Sihor', 'Gujarat'),
(395, 'Songadh', 'Gujarat'),
(396, 'Surat', 'Gujarat'),
(397, 'Talaja', 'Gujarat'),
(398, 'Thangadh', 'Gujarat'),
(399, 'Tharad', 'Gujarat'),
(400, 'Umbergaon', 'Gujarat'),
(401, 'Umreth', 'Gujarat'),
(402, 'Una', 'Gujarat'),
(403, 'Unjha', 'Gujarat'),
(404, 'Upleta', 'Gujarat'),
(405, 'Vadnagar', 'Gujarat'),
(406, 'Vadodara', 'Gujarat'),
(407, 'Valsad', 'Gujarat'),
(408, 'Vapi', 'Gujarat'),
(409, 'Vapi', 'Gujarat'),
(410, 'Veraval', 'Gujarat'),
(411, 'Vijapur', 'Gujarat'),
(412, 'Viramgam', 'Gujarat'),
(413, 'Visnagar', 'Gujarat'),
(414, 'Vyara', 'Gujarat'),
(415, 'Wadhwan', 'Gujarat'),
(416, 'Wankaner', 'Gujarat'),
(417, 'Adalaj', 'Gujrat'),
(418, 'Adityana', 'Gujrat'),
(419, 'Alang', 'Gujrat'),
(420, 'Ambaji', 'Gujrat'),
(421, 'Ambaliyasan', 'Gujrat'),
(422, 'Andada', 'Gujrat'),
(423, 'Anjar', 'Gujrat'),
(424, 'Anklav', 'Gujrat'),
(425, 'Antaliya', 'Gujrat'),
(426, 'Arambhada', 'Gujrat'),
(427, 'Atul', 'Gujrat'),
(428, 'Ballabhgarh', 'Hariyana'),
(429, 'Ambala', 'Haryana'),
(430, 'Ambala', 'Haryana'),
(431, 'Asankhurd', 'Haryana'),
(432, 'Assandh', 'Haryana'),
(433, 'Ateli', 'Haryana'),
(434, 'Babiyal', 'Haryana'),
(435, 'Bahadurgarh', 'Haryana'),
(436, 'Barwala', 'Haryana'),
(437, 'Bhiwani', 'Haryana'),
(438, 'Charkhi Dadri', 'Haryana'),
(439, 'Cheeka', 'Haryana'),
(440, 'Ellenabad 2', 'Haryana'),
(441, 'Faridabad', 'Haryana'),
(442, 'Fatehabad', 'Haryana'),
(443, 'Ganaur', 'Haryana'),
(444, 'Gharaunda', 'Haryana'),
(445, 'Gohana', 'Haryana'),
(446, 'Gurgaon', 'Haryana'),
(447, 'Haibat(Yamuna Nagar)', 'Haryana'),
(448, 'Hansi', 'Haryana'),
(449, 'Hisar', 'Haryana'),
(450, 'Hodal', 'Haryana'),
(451, 'Jhajjar', 'Haryana'),
(452, 'Jind', 'Haryana'),
(453, 'Kaithal', 'Haryana'),
(454, 'Kalan Wali', 'Haryana'),
(455, 'Kalka', 'Haryana'),
(456, 'Karnal', 'Haryana'),
(457, 'Ladwa', 'Haryana'),
(458, 'Mahendragarh', 'Haryana'),
(459, 'Mandi Dabwali', 'Haryana'),
(460, 'Narnaul', 'Haryana'),
(461, 'Narwana', 'Haryana'),
(462, 'Palwal', 'Haryana'),
(463, 'Panchkula', 'Haryana'),
(464, 'Panipat', 'Haryana'),
(465, 'Pehowa', 'Haryana'),
(466, 'Pinjore', 'Haryana'),
(467, 'Rania', 'Haryana'),
(468, 'Ratia', 'Haryana'),
(469, 'Rewari', 'Haryana'),
(470, 'Rohtak', 'Haryana'),
(471, 'Safidon', 'Haryana'),
(472, 'Samalkha', 'Haryana'),
(473, 'Shahbad', 'Haryana'),
(474, 'Sirsa', 'Haryana'),
(475, 'Sohna', 'Haryana'),
(476, 'Sonipat', 'Haryana'),
(477, 'Taraori', 'Haryana'),
(478, 'Thanesar', 'Haryana'),
(479, 'Tohana', 'Haryana'),
(480, 'Yamunanagar', 'Haryana'),
(481, 'Arki', 'Himachal Pradesh'),
(482, 'Baddi', 'Himachal Pradesh'),
(483, 'Bilaspur', 'Himachal Pradesh'),
(484, 'Chamba', 'Himachal Pradesh'),
(485, 'Dalhousie', 'Himachal Pradesh'),
(486, 'Dharamsala', 'Himachal Pradesh'),
(487, 'Hamirpur', 'Himachal Pradesh'),
(488, 'Mandi', 'Himachal Pradesh'),
(489, 'Nahan', 'Himachal Pradesh'),
(490, 'Shimla', 'Himachal Pradesh'),
(491, 'Solan', 'Himachal Pradesh'),
(492, 'Sundarnagar', 'Himachal Pradesh'),
(493, 'Jammu', 'Jammu & Kashmir'),
(494, 'Achabbal', 'Jammu and Kashmir'),
(495, 'Akhnoor', 'Jammu and Kashmir'),
(496, 'Anantnag', 'Jammu and Kashmir'),
(497, 'Arnia', 'Jammu and Kashmir'),
(498, 'Awantipora', 'Jammu and Kashmir'),
(499, 'Bandipore', 'Jammu and Kashmir'),
(500, 'Baramula', 'Jammu and Kashmir'),
(501, 'Kathua', 'Jammu and Kashmir'),
(502, 'Leh', 'Jammu and Kashmir'),
(503, 'Punch', 'Jammu and Kashmir'),
(504, 'Rajauri', 'Jammu and Kashmir'),
(505, 'Sopore', 'Jammu and Kashmir'),
(506, 'Srinagar', 'Jammu and Kashmir'),
(507, 'Udhampur', 'Jammu and Kashmir'),
(508, 'Amlabad', 'Jharkhand'),
(509, 'Ara', 'Jharkhand'),
(510, 'Barughutu', 'Jharkhand'),
(511, 'Bokaro Steel City', 'Jharkhand'),
(512, 'Chaibasa', 'Jharkhand'),
(513, 'Chakradharpur', 'Jharkhand'),
(514, 'Chandrapura', 'Jharkhand'),
(515, 'Chatra', 'Jharkhand'),
(516, 'Chirkunda', 'Jharkhand'),
(517, 'Churi', 'Jharkhand'),
(518, 'Daltonganj', 'Jharkhand'),
(519, 'Deoghar', 'Jharkhand'),
(520, 'Dhanbad', 'Jharkhand'),
(521, 'Dumka', 'Jharkhand'),
(522, 'Garhwa', 'Jharkhand'),
(523, 'Ghatshila', 'Jharkhand'),
(524, 'Giridih', 'Jharkhand'),
(525, 'Godda', 'Jharkhand'),
(526, 'Gomoh', 'Jharkhand'),
(527, 'Gumia', 'Jharkhand'),
(528, 'Gumla', 'Jharkhand'),
(529, 'Hazaribag', 'Jharkhand'),
(530, 'Hussainabad', 'Jharkhand'),
(531, 'Jamshedpur', 'Jharkhand'),
(532, 'Jamtara', 'Jharkhand'),
(533, 'Jhumri Tilaiya', 'Jharkhand'),
(534, 'Khunti', 'Jharkhand'),
(535, 'Lohardaga', 'Jharkhand'),
(536, 'Madhupur', 'Jharkhand'),
(537, 'Mihijam', 'Jharkhand'),
(538, 'Musabani', 'Jharkhand'),
(539, 'Pakaur', 'Jharkhand'),
(540, 'Patratu', 'Jharkhand'),
(541, 'Phusro', 'Jharkhand'),
(542, 'Ramngarh', 'Jharkhand'),
(543, 'Ranchi', 'Jharkhand'),
(544, 'Sahibganj', 'Jharkhand'),
(545, 'Saunda', 'Jharkhand'),
(546, 'Simdega', 'Jharkhand'),
(547, 'Tenu Dam-cum- Kathhara', 'Jharkhand'),
(548, 'Arasikere', 'Karnataka'),
(549, 'Bangalore', 'Karnataka'),
(550, 'Belgaum', 'Karnataka'),
(551, 'Bellary', 'Karnataka'),
(552, 'Chamrajnagar', 'Karnataka'),
(553, 'Chikkaballapur', 'Karnataka'),
(554, 'Chintamani', 'Karnataka'),
(555, 'Chitradurga', 'Karnataka'),
(556, 'Gulbarga', 'Karnataka'),
(557, 'Gundlupet', 'Karnataka'),
(558, 'Hassan', 'Karnataka'),
(559, 'Hospet', 'Karnataka'),
(560, 'Hubli', 'Karnataka'),
(561, 'Karkala', 'Karnataka'),
(562, 'Karwar', 'Karnataka'),
(563, 'Kolar', 'Karnataka'),
(564, 'Kota', 'Karnataka'),
(565, 'Lakshmeshwar', 'Karnataka'),
(566, 'Lingsugur', 'Karnataka'),
(567, 'Maddur', 'Karnataka'),
(568, 'Madhugiri', 'Karnataka'),
(569, 'Madikeri', 'Karnataka'),
(570, 'Magadi', 'Karnataka'),
(571, 'Mahalingpur', 'Karnataka'),
(572, 'Malavalli', 'Karnataka'),
(573, 'Malur', 'Karnataka'),
(574, 'Mandya', 'Karnataka'),
(575, 'Mangalore', 'Karnataka'),
(576, 'Manvi', 'Karnataka'),
(577, 'Mudalgi', 'Karnataka'),
(578, 'Mudbidri', 'Karnataka'),
(579, 'Muddebihal', 'Karnataka'),
(580, 'Mudhol', 'Karnataka'),
(581, 'Mulbagal', 'Karnataka'),
(582, 'Mundargi', 'Karnataka'),
(583, 'Mysore', 'Karnataka'),
(584, 'Nanjangud', 'Karnataka'),
(585, 'Pavagada', 'Karnataka'),
(586, 'Puttur', 'Karnataka'),
(587, 'Rabkavi Banhatti', 'Karnataka'),
(588, 'Raichur', 'Karnataka'),
(589, 'Ramanagaram', 'Karnataka'),
(590, 'Ramdurg', 'Karnataka'),
(591, 'Ranibennur', 'Karnataka'),
(592, 'Robertson Pet', 'Karnataka'),
(593, 'Ron', 'Karnataka'),
(594, 'Sadalgi', 'Karnataka'),
(595, 'Sagar', 'Karnataka'),
(596, 'Sakleshpur', 'Karnataka'),
(597, 'Sandur', 'Karnataka'),
(598, 'Sankeshwar', 'Karnataka'),
(599, 'Saundatti-Yellamma', 'Karnataka'),
(600, 'Savanur', 'Karnataka'),
(601, 'Sedam', 'Karnataka'),
(602, 'Shahabad', 'Karnataka'),
(603, 'Shahpur', 'Karnataka'),
(604, 'Shiggaon', 'Karnataka'),
(605, 'Shikapur', 'Karnataka'),
(606, 'Shimoga', 'Karnataka'),
(607, 'Shorapur', 'Karnataka'),
(608, 'Shrirangapattana', 'Karnataka'),
(609, 'Sidlaghatta', 'Karnataka'),
(610, 'Sindgi', 'Karnataka'),
(611, 'Sindhnur', 'Karnataka'),
(612, 'Sira', 'Karnataka'),
(613, 'Sirsi', 'Karnataka'),
(614, 'Siruguppa', 'Karnataka'),
(615, 'Srinivaspur', 'Karnataka'),
(616, 'Talikota', 'Karnataka'),
(617, 'Tarikere', 'Karnataka'),
(618, 'Tekkalakota', 'Karnataka'),
(619, 'Terdal', 'Karnataka'),
(620, 'Tiptur', 'Karnataka'),
(621, 'Tumkur', 'Karnataka'),
(622, 'Udupi', 'Karnataka'),
(623, 'Vijayapura', 'Karnataka'),
(624, 'Wadi', 'Karnataka'),
(625, 'Yadgir', 'Karnataka'),
(626, 'Adoor', 'Kerala'),
(627, 'Akathiyoor', 'Kerala'),
(628, 'Alappuzha', 'Kerala'),
(629, 'Ancharakandy', 'Kerala'),
(630, 'Aroor', 'Kerala'),
(631, 'Ashtamichira', 'Kerala'),
(632, 'Attingal', 'Kerala'),
(633, 'Avinissery', 'Kerala'),
(634, 'Chalakudy', 'Kerala'),
(635, 'Changanassery', 'Kerala'),
(636, 'Chendamangalam', 'Kerala'),
(637, 'Chengannur', 'Kerala'),
(638, 'Cherthala', 'Kerala'),
(639, 'Cheruthazham', 'Kerala'),
(640, 'Chittur-Thathamangalam', 'Kerala'),
(641, 'Chockli', 'Kerala'),
(642, 'Erattupetta', 'Kerala'),
(643, 'Guruvayoor', 'Kerala'),
(644, 'Irinjalakuda', 'Kerala'),
(645, 'Kadirur', 'Kerala'),
(646, 'Kalliasseri', 'Kerala'),
(647, 'Kalpetta', 'Kerala'),
(648, 'Kanhangad', 'Kerala'),
(649, 'Kanjikkuzhi', 'Kerala'),
(650, 'Kannur', 'Kerala'),
(651, 'Kasaragod', 'Kerala'),
(652, 'Kayamkulam', 'Kerala'),
(653, 'Kochi', 'Kerala'),
(654, 'Kodungallur', 'Kerala'),
(655, 'Kollam', 'Kerala'),
(656, 'Koothuparamba', 'Kerala'),
(657, 'Kothamangalam', 'Kerala'),
(658, 'Kottayam', 'Kerala'),
(659, 'Kozhikode', 'Kerala'),
(660, 'Kunnamkulam', 'Kerala'),
(661, 'Malappuram', 'Kerala'),
(662, 'Mattannur', 'Kerala'),
(663, 'Mavelikkara', 'Kerala'),
(664, 'Mavoor', 'Kerala'),
(665, 'Muvattupuzha', 'Kerala'),
(666, 'Nedumangad', 'Kerala'),
(667, 'Neyyattinkara', 'Kerala'),
(668, 'Ottappalam', 'Kerala'),
(669, 'Palai', 'Kerala'),
(670, 'Palakkad', 'Kerala'),
(671, 'Panniyannur', 'Kerala'),
(672, 'Pappinisseri', 'Kerala'),
(673, 'Paravoor', 'Kerala'),
(674, 'Pathanamthitta', 'Kerala'),
(675, 'Payyannur', 'Kerala'),
(676, 'Peringathur', 'Kerala'),
(677, 'Perinthalmanna', 'Kerala'),
(678, 'Perumbavoor', 'Kerala'),
(679, 'Ponnani', 'Kerala'),
(680, 'Punalur', 'Kerala'),
(681, 'Quilandy', 'Kerala'),
(682, 'Shoranur', 'Kerala'),
(683, 'Taliparamba', 'Kerala'),
(684, 'Thiruvalla', 'Kerala'),
(685, 'Thiruvananthapuram', 'Kerala'),
(686, 'Thodupuzha', 'Kerala'),
(687, 'Thrissur', 'Kerala'),
(688, 'Tirur', 'Kerala'),
(689, 'Vadakara', 'Kerala'),
(690, 'Vaikom', 'Kerala'),
(691, 'Varkala', 'Kerala'),
(692, 'Kavaratti', 'Lakshadweep'),
(693, 'Ashok Nagar', 'Madhya Pradesh'),
(694, 'Balaghat', 'Madhya Pradesh'),
(695, 'Betul', 'Madhya Pradesh'),
(696, 'Bhopal', 'Madhya Pradesh'),
(697, 'Burhanpur', 'Madhya Pradesh'),
(698, 'Chhatarpur', 'Madhya Pradesh'),
(699, 'Dabra', 'Madhya Pradesh'),
(700, 'Datia', 'Madhya Pradesh'),
(701, 'Dewas', 'Madhya Pradesh'),
(702, 'Dhar', 'Madhya Pradesh'),
(703, 'Fatehabad', 'Madhya Pradesh'),
(704, 'Gwalior', 'Madhya Pradesh'),
(705, 'Indore', 'Madhya Pradesh'),
(706, 'Itarsi', 'Madhya Pradesh'),
(707, 'Jabalpur', 'Madhya Pradesh'),
(708, 'Katni', 'Madhya Pradesh'),
(709, 'Kotma', 'Madhya Pradesh'),
(710, 'Lahar', 'Madhya Pradesh'),
(711, 'Lundi', 'Madhya Pradesh'),
(712, 'Maharajpur', 'Madhya Pradesh'),
(713, 'Mahidpur', 'Madhya Pradesh'),
(714, 'Maihar', 'Madhya Pradesh'),
(715, 'Malajkhand', 'Madhya Pradesh'),
(716, 'Manasa', 'Madhya Pradesh'),
(717, 'Manawar', 'Madhya Pradesh'),
(718, 'Mandideep', 'Madhya Pradesh'),
(719, 'Mandla', 'Madhya Pradesh'),
(720, 'Mandsaur', 'Madhya Pradesh'),
(721, 'Mauganj', 'Madhya Pradesh'),
(722, 'Mhow Cantonment', 'Madhya Pradesh'),
(723, 'Mhowgaon', 'Madhya Pradesh'),
(724, 'Morena', 'Madhya Pradesh'),
(725, 'Multai', 'Madhya Pradesh'),
(726, 'Murwara', 'Madhya Pradesh'),
(727, 'Nagda', 'Madhya Pradesh'),
(728, 'Nainpur', 'Madhya Pradesh'),
(729, 'Narsinghgarh', 'Madhya Pradesh'),
(730, 'Narsinghgarh', 'Madhya Pradesh'),
(731, 'Neemuch', 'Madhya Pradesh'),
(732, 'Nepanagar', 'Madhya Pradesh'),
(733, 'Niwari', 'Madhya Pradesh'),
(734, 'Nowgong', 'Madhya Pradesh'),
(735, 'Nowrozabad', 'Madhya Pradesh'),
(736, 'Pachore', 'Madhya Pradesh'),
(737, 'Pali', 'Madhya Pradesh'),
(738, 'Panagar', 'Madhya Pradesh'),
(739, 'Pandhurna', 'Madhya Pradesh'),
(740, 'Panna', 'Madhya Pradesh'),
(741, 'Pasan', 'Madhya Pradesh'),
(742, 'Pipariya', 'Madhya Pradesh'),
(743, 'Pithampur', 'Madhya Pradesh'),
(744, 'Porsa', 'Madhya Pradesh'),
(745, 'Prithvipur', 'Madhya Pradesh'),
(746, 'Raghogarh-Vijaypur', 'Madhya Pradesh'),
(747, 'Rahatgarh', 'Madhya Pradesh'),
(748, 'Raisen', 'Madhya Pradesh'),
(749, 'Rajgarh', 'Madhya Pradesh'),
(750, 'Ratlam', 'Madhya Pradesh'),
(751, 'Rau', 'Madhya Pradesh'),
(752, 'Rehli', 'Madhya Pradesh'),
(753, 'Rewa', 'Madhya Pradesh'),
(754, 'Sabalgarh', 'Madhya Pradesh'),
(755, 'Sagar', 'Madhya Pradesh'),
(756, 'Sanawad', 'Madhya Pradesh'),
(757, 'Sarangpur', 'Madhya Pradesh'),
(758, 'Sarni', 'Madhya Pradesh'),
(759, 'Satna', 'Madhya Pradesh'),
(760, 'Sausar', 'Madhya Pradesh'),
(761, 'Sehore', 'Madhya Pradesh'),
(762, 'Sendhwa', 'Madhya Pradesh'),
(763, 'Seoni', 'Madhya Pradesh'),
(764, 'Seoni-Malwa', 'Madhya Pradesh'),
(765, 'Shahdol', 'Madhya Pradesh'),
(766, 'Shajapur', 'Madhya Pradesh'),
(767, 'Shamgarh', 'Madhya Pradesh'),
(768, 'Sheopur', 'Madhya Pradesh'),
(769, 'Shivpuri', 'Madhya Pradesh'),
(770, 'Shujalpur', 'Madhya Pradesh'),
(771, 'Sidhi', 'Madhya Pradesh'),
(772, 'Sihora', 'Madhya Pradesh'),
(773, 'Singrauli', 'Madhya Pradesh'),
(774, 'Sironj', 'Madhya Pradesh'),
(775, 'Sohagpur', 'Madhya Pradesh'),
(776, 'Tarana', 'Madhya Pradesh'),
(777, 'Tikamgarh', 'Madhya Pradesh'),
(778, 'Ujhani', 'Madhya Pradesh'),
(779, 'Ujjain', 'Madhya Pradesh'),
(780, 'Umaria', 'Madhya Pradesh'),
(781, 'Vidisha', 'Madhya Pradesh'),
(782, 'Wara Seoni', 'Madhya Pradesh'),
(783, 'Ahmednagar', 'Maharashtra'),
(784, 'Akola', 'Maharashtra'),
(785, 'Amravati', 'Maharashtra'),
(786, 'Aurangabad', 'Maharashtra'),
(787, 'Baramati', 'Maharashtra'),
(788, 'Chalisgaon', 'Maharashtra'),
(789, 'Chinchani', 'Maharashtra'),
(790, 'Devgarh', 'Maharashtra'),
(791, 'Dhule', 'Maharashtra'),
(792, 'Dombivli', 'Maharashtra'),
(793, 'Durgapur', 'Maharashtra'),
(794, 'Ichalkaranji', 'Maharashtra'),
(795, 'Jalna', 'Maharashtra'),
(796, 'Kalyan', 'Maharashtra'),
(797, 'Latur', 'Maharashtra'),
(798, 'Loha', 'Maharashtra'),
(799, 'Lonar', 'Maharashtra'),
(800, 'Lonavla', 'Maharashtra'),
(801, 'Mahad', 'Maharashtra'),
(802, 'Mahuli', 'Maharashtra'),
(803, 'Malegaon', 'Maharashtra'),
(804, 'Malkapur', 'Maharashtra'),
(805, 'Manchar', 'Maharashtra'),
(806, 'Mangalvedhe', 'Maharashtra'),
(807, 'Mangrulpir', 'Maharashtra'),
(808, 'Manjlegaon', 'Maharashtra'),
(809, 'Manmad', 'Maharashtra'),
(810, 'Manwath', 'Maharashtra'),
(811, 'Mehkar', 'Maharashtra'),
(812, 'Mhaswad', 'Maharashtra'),
(813, 'Miraj', 'Maharashtra'),
(814, 'Morshi', 'Maharashtra'),
(815, 'Mukhed', 'Maharashtra'),
(816, 'Mul', 'Maharashtra'),
(817, 'Mumbai', 'Maharashtra'),
(818, 'Murtijapur', 'Maharashtra'),
(819, 'Nagpur', 'Maharashtra'),
(820, 'Nalasopara', 'Maharashtra'),
(821, 'Nanded-Waghala', 'Maharashtra'),
(822, 'Nandgaon', 'Maharashtra'),
(823, 'Nandura', 'Maharashtra'),
(824, 'Nandurbar', 'Maharashtra'),
(825, 'Narkhed', 'Maharashtra'),
(826, 'Nashik', 'Maharashtra'),
(827, 'Navi Mumbai', 'Maharashtra'),
(828, 'Nawapur', 'Maharashtra'),
(829, 'Nilanga', 'Maharashtra'),
(830, 'Osmanabad', 'Maharashtra'),
(831, 'Ozar', 'Maharashtra'),
(832, 'Pachora', 'Maharashtra'),
(833, 'Paithan', 'Maharashtra'),
(834, 'Palghar', 'Maharashtra'),
(835, 'Pandharkaoda', 'Maharashtra'),
(836, 'Pandharpur', 'Maharashtra'),
(837, 'Panvel', 'Maharashtra'),
(838, 'Parbhani', 'Maharashtra'),
(839, 'Parli', 'Maharashtra'),
(840, 'Parola', 'Maharashtra'),
(841, 'Partur', 'Maharashtra'),
(842, 'Pathardi', 'Maharashtra'),
(843, 'Pathri', 'Maharashtra'),
(844, 'Patur', 'Maharashtra'),
(845, 'Pauni', 'Maharashtra'),
(846, 'Pen', 'Maharashtra'),
(847, 'Phaltan', 'Maharashtra'),
(848, 'Pulgaon', 'Maharashtra'),
(849, 'Pune', 'Maharashtra'),
(850, 'Purna', 'Maharashtra'),
(851, 'Pusad', 'Maharashtra'),
(852, 'Rahuri', 'Maharashtra'),
(853, 'Rajura', 'Maharashtra'),
(854, 'Ramtek', 'Maharashtra'),
(855, 'Ratnagiri', 'Maharashtra'),
(856, 'Raver', 'Maharashtra'),
(857, 'Risod', 'Maharashtra'),
(858, 'Sailu', 'Maharashtra'),
(859, 'Sangamner', 'Maharashtra'),
(860, 'Sangli', 'Maharashtra'),
(861, 'Sangole', 'Maharashtra'),
(862, 'Sasvad', 'Maharashtra'),
(863, 'Satana', 'Maharashtra'),
(864, 'Satara', 'Maharashtra'),
(865, 'Savner', 'Maharashtra'),
(866, 'Sawantwadi', 'Maharashtra'),
(867, 'Shahade', 'Maharashtra'),
(868, 'Shegaon', 'Maharashtra'),
(869, 'Shendurjana', 'Maharashtra'),
(870, 'Shirdi', 'Maharashtra'),
(871, 'Shirpur-Warwade', 'Maharashtra'),
(872, 'Shirur', 'Maharashtra'),
(873, 'Shrigonda', 'Maharashtra'),
(874, 'Shrirampur', 'Maharashtra'),
(875, 'Sillod', 'Maharashtra'),
(876, 'Sinnar', 'Maharashtra'),
(877, 'Solapur', 'Maharashtra'),
(878, 'Soyagaon', 'Maharashtra'),
(879, 'Talegaon Dabhade', 'Maharashtra'),
(880, 'Talode', 'Maharashtra'),
(881, 'Tasgaon', 'Maharashtra'),
(882, 'Tirora', 'Maharashtra'),
(883, 'Tuljapur', 'Maharashtra'),
(884, 'Tumsar', 'Maharashtra'),
(885, 'Uran', 'Maharashtra'),
(886, 'Uran Islampur', 'Maharashtra'),
(887, 'Wadgaon Road', 'Maharashtra'),
(888, 'Wai', 'Maharashtra'),
(889, 'Wani', 'Maharashtra'),
(890, 'Wardha', 'Maharashtra'),
(891, 'Warora', 'Maharashtra'),
(892, 'Warud', 'Maharashtra'),
(893, 'Washim', 'Maharashtra'),
(894, 'Yevla', 'Maharashtra'),
(895, 'Uchgaon', 'Maharastra'),
(896, 'Udgir', 'Maharastra'),
(897, 'Umarga', 'Maharastra'),
(898, 'Umarkhed', 'Maharastra'),
(899, 'Umred', 'Maharastra'),
(900, 'Vadgaon Kasba', 'Maharastra'),
(901, 'Vaijapur', 'Maharastra'),
(902, 'Vasai', 'Maharastra'),
(903, 'Virar', 'Maharastra'),
(904, 'Vita', 'Maharastra'),
(905, 'Yavatmal', 'Maharastra'),
(906, 'Yawal', 'Maharastra'),
(907, 'Imphal', 'Manipur'),
(908, 'Kakching', 'Manipur'),
(909, 'Lilong', 'Manipur'),
(910, 'Mayang Imphal', 'Manipur'),
(911, 'Thoubal', 'Manipur'),
(912, 'Jowai', 'Meghalaya'),
(913, 'Nongstoin', 'Meghalaya'),
(914, 'Shillong', 'Meghalaya'),
(915, 'Tura', 'Meghalaya'),
(916, 'Aizawl', 'Mizoram'),
(917, 'Champhai', 'Mizoram'),
(918, 'Lunglei', 'Mizoram'),
(919, 'Saiha', 'Mizoram'),
(920, 'Dimapur', 'Nagaland'),
(921, 'Kohima', 'Nagaland'),
(922, 'Mokokchung', 'Nagaland'),
(923, 'Tuensang', 'Nagaland'),
(924, 'Wokha', 'Nagaland'),
(925, 'Zunheboto', 'Nagaland'),
(950, 'Anandapur', 'Orissa'),
(951, 'Anugul', 'Orissa'),
(952, 'Asika', 'Orissa'),
(953, 'Balangir', 'Orissa'),
(954, 'Balasore', 'Orissa'),
(955, 'Baleshwar', 'Orissa'),
(956, 'Bamra', 'Orissa'),
(957, 'Barbil', 'Orissa'),
(958, 'Bargarh', 'Orissa'),
(959, 'Bargarh', 'Orissa'),
(960, 'Baripada', 'Orissa'),
(961, 'Basudebpur', 'Orissa'),
(962, 'Belpahar', 'Orissa'),
(963, 'Bhadrak', 'Orissa'),
(964, 'Bhawanipatna', 'Orissa'),
(965, 'Bhuban', 'Orissa'),
(966, 'Bhubaneswar', 'Orissa'),
(967, 'Biramitrapur', 'Orissa'),
(968, 'Brahmapur', 'Orissa'),
(969, 'Brajrajnagar', 'Orissa'),
(970, 'Byasanagar', 'Orissa'),
(971, 'Cuttack', 'Orissa'),
(972, 'Debagarh', 'Orissa'),
(973, 'Dhenkanal', 'Orissa'),
(974, 'Gunupur', 'Orissa'),
(975, 'Hinjilicut', 'Orissa'),
(976, 'Jagatsinghapur', 'Orissa'),
(977, 'Jajapur', 'Orissa'),
(978, 'Jaleswar', 'Orissa'),
(979, 'Jatani', 'Orissa'),
(980, 'Jeypur', 'Orissa'),
(981, 'Jharsuguda', 'Orissa'),
(982, 'Joda', 'Orissa'),
(983, 'Kantabanji', 'Orissa'),
(984, 'Karanjia', 'Orissa'),
(985, 'Kendrapara', 'Orissa'),
(986, 'Kendujhar', 'Orissa'),
(987, 'Khordha', 'Orissa'),
(988, 'Koraput', 'Orissa'),
(989, 'Malkangiri', 'Orissa'),
(990, 'Nabarangapur', 'Orissa'),
(991, 'Paradip', 'Orissa'),
(992, 'Parlakhemundi', 'Orissa'),
(993, 'Pattamundai', 'Orissa'),
(994, 'Phulabani', 'Orissa'),
(995, 'Puri', 'Orissa'),
(996, 'Rairangpur', 'Orissa'),
(997, 'Rajagangapur', 'Orissa'),
(998, 'Raurkela', 'Orissa'),
(999, 'Rayagada', 'Orissa'),
(1000, 'Sambalpur', 'Orissa'),
(1001, 'Soro', 'Orissa'),
(1002, 'Sunabeda', 'Orissa'),
(1003, 'Sundargarh', 'Orissa'),
(1004, 'Talcher', 'Orissa'),
(1005, 'Titlagarh', 'Orissa'),
(1006, 'Umarkote', 'Orissa'),
(1007, 'Karaikal', 'Pondicherry'),
(1008, 'Mahe', 'Pondicherry'),
(1009, 'Pondicherry', 'Pondicherry'),
(1010, 'Yanam', 'Pondicherry'),
(1011, 'Ahmedgarh', 'Punjab'),
(1012, 'Amritsar', 'Punjab'),
(1013, 'Barnala', 'Punjab'),
(1014, 'Batala', 'Punjab'),
(1015, 'Bathinda', 'Punjab'),
(1016, 'Bhagha Purana', 'Punjab'),
(1017, 'Budhlada', 'Punjab'),
(1018, 'Chandigarh', 'Punjab'),
(1019, 'Dasua', 'Punjab'),
(1020, 'Dhuri', 'Punjab'),
(1021, 'Dinanagar', 'Punjab'),
(1022, 'Faridkot', 'Punjab'),
(1023, 'Fazilka', 'Punjab'),
(1024, 'Firozpur', 'Punjab'),
(1025, 'Firozpur Cantt.', 'Punjab'),
(1026, 'Giddarbaha', 'Punjab'),
(1027, 'Gobindgarh', 'Punjab'),
(1028, 'Gurdaspur', 'Punjab'),
(1029, 'Hoshiarpur', 'Punjab'),
(1030, 'Jagraon', 'Punjab'),
(1031, 'Jaitu', 'Punjab'),
(1032, 'Jalalabad', 'Punjab'),
(1033, 'Jalandhar', 'Punjab'),
(1034, 'Jalandhar Cantt.', 'Punjab'),
(1035, 'Jandiala', 'Punjab'),
(1036, 'Kapurthala', 'Punjab'),
(1037, 'Karoran', 'Punjab'),
(1038, 'Kartarpur', 'Punjab'),
(1039, 'Khanna', 'Punjab'),
(1040, 'Kharar', 'Punjab'),
(1041, 'Kot Kapura', 'Punjab'),
(1042, 'Kurali', 'Punjab'),
(1043, 'Longowal', 'Punjab'),
(1044, 'Ludhiana', 'Punjab'),
(1045, 'Malerkotla', 'Punjab'),
(1046, 'Malout', 'Punjab'),
(1047, 'Mansa', 'Punjab'),
(1048, 'Maur', 'Punjab'),
(1049, 'Moga', 'Punjab'),
(1050, 'Mohali', 'Punjab'),
(1051, 'Morinda', 'Punjab'),
(1052, 'Mukerian', 'Punjab'),
(1053, 'Muktsar', 'Punjab'),
(1054, 'Nabha', 'Punjab'),
(1055, 'Nakodar', 'Punjab'),
(1056, 'Nangal', 'Punjab'),
(1057, 'Nawanshahr', 'Punjab'),
(1058, 'Pathankot', 'Punjab'),
(1059, 'Patiala', 'Punjab'),
(1060, 'Patran', 'Punjab'),
(1061, 'Patti', 'Punjab'),
(1062, 'Phagwara', 'Punjab'),
(1063, 'Phillaur', 'Punjab'),
(1064, 'Qadian', 'Punjab'),
(1065, 'Raikot', 'Punjab'),
(1066, 'Rajpura', 'Punjab'),
(1067, 'Rampura Phul', 'Punjab'),
(1068, 'Rupnagar', 'Punjab'),
(1069, 'Samana', 'Punjab'),
(1070, 'Sangrur', 'Punjab'),
(1071, 'Sirhind Fatehgarh Sahib', 'Punjab'),
(1072, 'Sujanpur', 'Punjab'),
(1073, 'Sunam', 'Punjab'),
(1074, 'Talwara', 'Punjab'),
(1075, 'Tarn Taran', 'Punjab'),
(1076, 'Urmar Tanda', 'Punjab'),
(1077, 'Zira', 'Punjab'),
(1078, 'Zirakpur', 'Punjab'),
(1079, 'Bali', 'Rajastan'),
(1080, 'Banswara', 'Rajastan'),
(1081, 'Ajmer', 'Rajasthan'),
(1082, 'Alwar', 'Rajasthan'),
(1083, 'Bandikui', 'Rajasthan'),
(1084, 'Baran', 'Rajasthan'),
(1085, 'Barmer', 'Rajasthan'),
(1086, 'Bikaner', 'Rajasthan'),
(1087, 'Fatehpur', 'Rajasthan'),
(1088, 'Jaipur', 'Rajasthan'),
(1089, 'Jaisalmer', 'Rajasthan'),
(1090, 'Jodhpur', 'Rajasthan'),
(1091, 'Kota', 'Rajasthan'),
(1092, 'Lachhmangarh', 'Rajasthan'),
(1093, 'Ladnu', 'Rajasthan'),
(1094, 'Lakheri', 'Rajasthan'),
(1095, 'Lalsot', 'Rajasthan'),
(1096, 'Losal', 'Rajasthan'),
(1097, 'Makrana', 'Rajasthan'),
(1098, 'Malpura', 'Rajasthan'),
(1099, 'Mandalgarh', 'Rajasthan'),
(1100, 'Mandawa', 'Rajasthan'),
(1101, 'Mangrol', 'Rajasthan'),
(1102, 'Merta City', 'Rajasthan'),
(1103, 'Mount Abu', 'Rajasthan'),
(1104, 'Nadbai', 'Rajasthan'),
(1105, 'Nagar', 'Rajasthan'),
(1106, 'Nagaur', 'Rajasthan'),
(1107, 'Nargund', 'Rajasthan'),
(1108, 'Nasirabad', 'Rajasthan'),
(1109, 'Nathdwara', 'Rajasthan'),
(1110, 'Navalgund', 'Rajasthan'),
(1111, 'Nawalgarh', 'Rajasthan'),
(1112, 'Neem-Ka-Thana', 'Rajasthan'),
(1113, 'Nelamangala', 'Rajasthan'),
(1114, 'Nimbahera', 'Rajasthan'),
(1115, 'Nipani', 'Rajasthan'),
(1116, 'Niwai', 'Rajasthan'),
(1117, 'Nohar', 'Rajasthan'),
(1118, 'Nokha', 'Rajasthan'),
(1119, 'Pali', 'Rajasthan'),
(1120, 'Phalodi', 'Rajasthan'),
(1121, 'Phulera', 'Rajasthan'),
(1122, 'Pilani', 'Rajasthan'),
(1123, 'Pilibanga', 'Rajasthan'),
(1124, 'Pindwara', 'Rajasthan'),
(1125, 'Pipar City', 'Rajasthan'),
(1126, 'Prantij', 'Rajasthan'),
(1127, 'Pratapgarh', 'Rajasthan'),
(1128, 'Raisinghnagar', 'Rajasthan'),
(1129, 'Rajakhera', 'Rajasthan'),
(1130, 'Rajaldesar', 'Rajasthan'),
(1131, 'Rajgarh (Alwar)', 'Rajasthan'),
(1132, 'Rajgarh (Churu', 'Rajasthan'),
(1133, 'Rajsamand', 'Rajasthan'),
(1134, 'Ramganj Mandi', 'Rajasthan'),
(1135, 'Ramngarh', 'Rajasthan'),
(1136, 'Ratangarh', 'Rajasthan'),
(1137, 'Rawatbhata', 'Rajasthan'),
(1138, 'Rawatsar', 'Rajasthan'),
(1139, 'Reengus', 'Rajasthan'),
(1140, 'Sadri', 'Rajasthan'),
(1141, 'Sadulshahar', 'Rajasthan'),
(1142, 'Sagwara', 'Rajasthan'),
(1143, 'Sambhar', 'Rajasthan'),
(1144, 'Sanchore', 'Rajasthan'),
(1145, 'Sangaria', 'Rajasthan'),
(1146, 'Sardarshahar', 'Rajasthan'),
(1147, 'Sawai Madhopur', 'Rajasthan'),
(1148, 'Shahpura', 'Rajasthan'),
(1149, 'Shahpura', 'Rajasthan'),
(1150, 'Sheoganj', 'Rajasthan'),
(1151, 'Sikar', 'Rajasthan'),
(1152, 'Sirohi', 'Rajasthan'),
(1153, 'Sojat', 'Rajasthan'),
(1154, 'Sri Madhopur', 'Rajasthan'),
(1155, 'Sujangarh', 'Rajasthan'),
(1156, 'Sumerpur', 'Rajasthan'),
(1157, 'Suratgarh', 'Rajasthan'),
(1158, 'Taranagar', 'Rajasthan'),
(1159, 'Todabhim', 'Rajasthan'),
(1160, 'Todaraisingh', 'Rajasthan'),
(1161, 'Tonk', 'Rajasthan'),
(1162, 'Udaipur', 'Rajasthan'),
(1163, 'Udaipurwati', 'Rajasthan'),
(1164, 'Vijainagar', 'Rajasthan'),
(1165, 'Gangtok', 'Sikkim'),
(1166, 'Calcutta', 'West Bengal'),
(1167, 'Arakkonam', 'Tamil Nadu'),
(1168, 'Arcot', 'Tamil Nadu'),
(1169, 'Aruppukkottai', 'Tamil Nadu'),
(1170, 'Bhavani', 'Tamil Nadu'),
(1171, 'Chengalpattu', 'Tamil Nadu'),
(1172, 'Chennai', 'Tamil Nadu'),
(1173, 'Chinna salem', 'Tamil nadu'),
(1174, 'Coimbatore', 'Tamil Nadu'),
(1175, 'Coonoor', 'Tamil Nadu'),
(1176, 'Cuddalore', 'Tamil Nadu'),
(1177, 'Dharmapuri', 'Tamil Nadu'),
(1178, 'Dindigul', 'Tamil Nadu'),
(1179, 'Erode', 'Tamil Nadu'),
(1180, 'Gudalur', 'Tamil Nadu'),
(1181, 'Gudalur', 'Tamil Nadu'),
(1182, 'Gudalur', 'Tamil Nadu'),
(1183, 'Kanchipuram', 'Tamil Nadu'),
(1184, 'Karaikudi', 'Tamil Nadu'),
(1185, 'Karungal', 'Tamil Nadu'),
(1186, 'Karur', 'Tamil Nadu'),
(1187, 'Kollankodu', 'Tamil Nadu'),
(1188, 'Lalgudi', 'Tamil Nadu'),
(1189, 'Madurai', 'Tamil Nadu'),
(1190, 'Nagapattinam', 'Tamil Nadu'),
(1191, 'Nagercoil', 'Tamil Nadu'),
(1192, 'Namagiripettai', 'Tamil Nadu'),
(1193, 'Namakkal', 'Tamil Nadu'),
(1194, 'Nandivaram-Guduvancheri', 'Tamil Nadu'),
(1195, 'Nanjikottai', 'Tamil Nadu'),
(1196, 'Natham', 'Tamil Nadu'),
(1197, 'Nellikuppam', 'Tamil Nadu'),
(1198, 'Neyveli', 'Tamil Nadu'),
(1199, 'O'' Valley', 'Tamil Nadu'),
(1200, 'Oddanchatram', 'Tamil Nadu'),
(1201, 'P.N.Patti', 'Tamil Nadu'),
(1202, 'Pacode', 'Tamil Nadu'),
(1203, 'Padmanabhapuram', 'Tamil Nadu'),
(1204, 'Palani', 'Tamil Nadu'),
(1205, 'Palladam', 'Tamil Nadu'),
(1206, 'Pallapatti', 'Tamil Nadu'),
(1207, 'Pallikonda', 'Tamil Nadu'),
(1208, 'Panagudi', 'Tamil Nadu'),
(1209, 'Panruti', 'Tamil Nadu'),
(1210, 'Paramakudi', 'Tamil Nadu'),
(1211, 'Parangipettai', 'Tamil Nadu'),
(1212, 'Pattukkottai', 'Tamil Nadu'),
(1213, 'Perambalur', 'Tamil Nadu'),
(1214, 'Peravurani', 'Tamil Nadu'),
(1215, 'Periyakulam', 'Tamil Nadu'),
(1216, 'Periyasemur', 'Tamil Nadu'),
(1217, 'Pernampattu', 'Tamil Nadu'),
(1218, 'Pollachi', 'Tamil Nadu'),
(1219, 'Polur', 'Tamil Nadu'),
(1220, 'Ponneri', 'Tamil Nadu'),
(1221, 'Pudukkottai', 'Tamil Nadu'),
(1222, 'Pudupattinam', 'Tamil Nadu'),
(1223, 'Puliyankudi', 'Tamil Nadu'),
(1224, 'Punjaipugalur', 'Tamil Nadu'),
(1225, 'Rajapalayam', 'Tamil Nadu'),
(1226, 'Ramanathapuram', 'Tamil Nadu'),
(1227, 'Rameshwaram', 'Tamil Nadu'),
(1228, 'Rasipuram', 'Tamil Nadu'),
(1229, 'Salem', 'Tamil Nadu'),
(1230, 'Sankarankoil', 'Tamil Nadu'),
(1231, 'Sankari', 'Tamil Nadu'),
(1232, 'Sathyamangalam', 'Tamil Nadu'),
(1233, 'Sattur', 'Tamil Nadu'),
(1234, 'Shenkottai', 'Tamil Nadu'),
(1235, 'Sholavandan', 'Tamil Nadu'),
(1236, 'Sholingur', 'Tamil Nadu'),
(1237, 'Sirkali', 'Tamil Nadu'),
(1238, 'Sivaganga', 'Tamil Nadu'),
(1239, 'Sivagiri', 'Tamil Nadu'),
(1240, 'Sivakasi', 'Tamil Nadu'),
(1241, 'Srivilliputhur', 'Tamil Nadu'),
(1242, 'Surandai', 'Tamil Nadu'),
(1243, 'Suriyampalayam', 'Tamil Nadu'),
(1244, 'Tenkasi', 'Tamil Nadu'),
(1245, 'Thammampatti', 'Tamil Nadu'),
(1246, 'Thanjavur', 'Tamil Nadu'),
(1247, 'Tharamangalam', 'Tamil Nadu'),
(1248, 'Tharangambadi', 'Tamil Nadu'),
(1249, 'Theni Allinagaram', 'Tamil Nadu'),
(1250, 'Thirumangalam', 'Tamil Nadu'),
(1251, 'Thirunindravur', 'Tamil Nadu'),
(1252, 'Thiruparappu', 'Tamil Nadu'),
(1253, 'Thirupuvanam', 'Tamil Nadu'),
(1254, 'Thiruthuraipoondi', 'Tamil Nadu'),
(1255, 'Thiruvallur', 'Tamil Nadu'),
(1256, 'Thiruvarur', 'Tamil Nadu'),
(1257, 'Thoothukudi', 'Tamil Nadu'),
(1258, 'Thuraiyur', 'Tamil Nadu'),
(1259, 'Tindivanam', 'Tamil Nadu'),
(1260, 'Tiruchendur', 'Tamil Nadu'),
(1261, 'Tiruchengode', 'Tamil Nadu'),
(1262, 'Tiruchirappalli', 'Tamil Nadu'),
(1263, 'Tirukalukundram', 'Tamil Nadu'),
(1264, 'Tirukkoyilur', 'Tamil Nadu'),
(1265, 'Tirunelveli', 'Tamil Nadu'),
(1266, 'Tirupathur', 'Tamil Nadu'),
(1267, 'Tirupathur', 'Tamil Nadu'),
(1268, 'Tiruppur', 'Tamil Nadu'),
(1269, 'Tiruttani', 'Tamil Nadu'),
(1270, 'Tiruvannamalai', 'Tamil Nadu'),
(1271, 'Tiruvethipuram', 'Tamil Nadu'),
(1272, 'Tittakudi', 'Tamil Nadu'),
(1273, 'Udhagamandalam', 'Tamil Nadu'),
(1274, 'Udumalaipettai', 'Tamil Nadu'),
(1275, 'Unnamalaikadai', 'Tamil Nadu'),
(1276, 'Usilampatti', 'Tamil Nadu'),
(1277, 'Uthamapalayam', 'Tamil Nadu'),
(1278, 'Uthiramerur', 'Tamil Nadu'),
(1279, 'Vadakkuvalliyur', 'Tamil Nadu'),
(1280, 'Vadalur', 'Tamil Nadu'),
(1281, 'Vadipatti', 'Tamil Nadu'),
(1282, 'Valparai', 'Tamil Nadu'),
(1283, 'Vandavasi', 'Tamil Nadu'),
(1284, 'Vaniyambadi', 'Tamil Nadu'),
(1285, 'Vedaranyam', 'Tamil Nadu'),
(1286, 'Vellakoil', 'Tamil Nadu'),
(1287, 'Vellore', 'Tamil Nadu'),
(1288, 'Vikramasingapuram', 'Tamil Nadu'),
(1289, 'Viluppuram', 'Tamil Nadu'),
(1290, 'Virudhachalam', 'Tamil Nadu'),
(1291, 'Virudhunagar', 'Tamil Nadu'),
(1292, 'Viswanatham', 'Tamil Nadu'),
(1293, 'Agartala', 'Tripura'),
(1294, 'Badharghat', 'Tripura'),
(1295, 'Dharmanagar', 'Tripura'),
(1296, 'Indranagar', 'Tripura'),
(1297, 'Jogendranagar', 'Tripura'),
(1298, 'Kailasahar', 'Tripura'),
(1299, 'Khowai', 'Tripura'),
(1300, 'Pratapgarh', 'Tripura'),
(1301, 'Udaipur', 'Tripura'),
(1302, 'Achhnera', 'Uttar Pradesh'),
(1303, 'Adari', 'Uttar Pradesh'),
(1304, 'Agra', 'Uttar Pradesh'),
(1305, 'Aligarh', 'Uttar Pradesh'),
(1306, 'Allahabad', 'Uttar Pradesh'),
(1307, 'Amroha', 'Uttar Pradesh'),
(1308, 'Azamgarh', 'Uttar Pradesh'),
(1309, 'Bahraich', 'Uttar Pradesh'),
(1310, 'Ballia', 'Uttar Pradesh'),
(1311, 'Balrampur', 'Uttar Pradesh'),
(1312, 'Banda', 'Uttar Pradesh'),
(1313, 'Bareilly', 'Uttar Pradesh'),
(1314, 'Chandausi', 'Uttar Pradesh'),
(1315, 'Dadri', 'Uttar Pradesh'),
(1316, 'Deoria', 'Uttar Pradesh'),
(1317, 'Etawah', 'Uttar Pradesh'),
(1318, 'Fatehabad', 'Uttar Pradesh'),
(1319, 'Fatehpur', 'Uttar Pradesh'),
(1320, 'Fatehpur', 'Uttar Pradesh'),
(1321, 'Greater Noida', 'Uttar Pradesh'),
(1322, 'Hamirpur', 'Uttar Pradesh'),
(1323, 'Hardoi', 'Uttar Pradesh'),
(1324, 'Jajmau', 'Uttar Pradesh'),
(1325, 'Jaunpur', 'Uttar Pradesh'),
(1326, 'Jhansi', 'Uttar Pradesh'),
(1327, 'Kalpi', 'Uttar Pradesh'),
(1328, 'Kanpur', 'Uttar Pradesh'),
(1329, 'Kota', 'Uttar Pradesh'),
(1330, 'Laharpur', 'Uttar Pradesh'),
(1331, 'Lakhimpur', 'Uttar Pradesh'),
(1332, 'Lal Gopalganj Nindaura', 'Uttar Pradesh'),
(1333, 'Lalganj', 'Uttar Pradesh'),
(1334, 'Lalitpur', 'Uttar Pradesh'),
(1335, 'Lar', 'Uttar Pradesh'),
(1336, 'Loni', 'Uttar Pradesh'),
(1337, 'Lucknow', 'Uttar Pradesh'),
(1338, 'Mathura', 'Uttar Pradesh'),
(1339, 'Meerut', 'Uttar Pradesh'),
(1340, 'Modinagar', 'Uttar Pradesh'),
(1341, 'Muradnagar', 'Uttar Pradesh'),
(1342, 'Nagina', 'Uttar Pradesh'),
(1343, 'Najibabad', 'Uttar Pradesh'),
(1344, 'Nakur', 'Uttar Pradesh'),
(1345, 'Nanpara', 'Uttar Pradesh'),
(1346, 'Naraura', 'Uttar Pradesh'),
(1347, 'Naugawan Sadat', 'Uttar Pradesh'),
(1348, 'Nautanwa', 'Uttar Pradesh'),
(1349, 'Nawabganj', 'Uttar Pradesh'),
(1350, 'Nehtaur', 'Uttar Pradesh'),
(1351, 'NOIDA', 'Uttar Pradesh'),
(1352, 'Noorpur', 'Uttar Pradesh'),
(1353, 'Obra', 'Uttar Pradesh'),
(1354, 'Orai', 'Uttar Pradesh'),
(1355, 'Padrauna', 'Uttar Pradesh'),
(1356, 'Palia Kalan', 'Uttar Pradesh'),
(1357, 'Parasi', 'Uttar Pradesh'),
(1358, 'Phulpur', 'Uttar Pradesh'),
(1359, 'Pihani', 'Uttar Pradesh'),
(1360, 'Pilibhit', 'Uttar Pradesh'),
(1361, 'Pilkhuwa', 'Uttar Pradesh'),
(1362, 'Powayan', 'Uttar Pradesh'),
(1363, 'Pukhrayan', 'Uttar Pradesh'),
(1364, 'Puranpur', 'Uttar Pradesh'),
(1365, 'Purquazi', 'Uttar Pradesh'),
(1366, 'Purwa', 'Uttar Pradesh'),
(1367, 'Rae Bareli', 'Uttar Pradesh'),
(1368, 'Rampur', 'Uttar Pradesh'),
(1369, 'Rampur Maniharan', 'Uttar Pradesh'),
(1370, 'Rasra', 'Uttar Pradesh'),
(1371, 'Rath', 'Uttar Pradesh'),
(1372, 'Renukoot', 'Uttar Pradesh'),
(1373, 'Reoti', 'Uttar Pradesh'),
(1374, 'Robertsganj', 'Uttar Pradesh'),
(1375, 'Rudauli', 'Uttar Pradesh'),
(1376, 'Rudrapur', 'Uttar Pradesh'),
(1377, 'Sadabad', 'Uttar Pradesh'),
(1378, 'Safipur', 'Uttar Pradesh'),
(1379, 'Saharanpur', 'Uttar Pradesh'),
(1380, 'Sahaspur', 'Uttar Pradesh'),
(1381, 'Sahaswan', 'Uttar Pradesh'),
(1382, 'Sahawar', 'Uttar Pradesh'),
(1383, 'Sahjanwa', 'Uttar Pradesh'),
(1384, '"Saidpur', ' Ghazipur"'),
(1385, 'Sambhal', 'Uttar Pradesh'),
(1386, 'Samdhan', 'Uttar Pradesh'),
(1387, 'Samthar', 'Uttar Pradesh'),
(1388, 'Sandi', 'Uttar Pradesh'),
(1389, 'Sandila', 'Uttar Pradesh'),
(1390, 'Sardhana', 'Uttar Pradesh'),
(1391, 'Seohara', 'Uttar Pradesh'),
(1392, '"Shahabad', ' Hardoi"'),
(1393, '"Shahabad', ' Rampur"'),
(1394, 'Shahganj', 'Uttar Pradesh'),
(1395, 'Shahjahanpur', 'Uttar Pradesh'),
(1396, 'Shamli', 'Uttar Pradesh'),
(1397, '"Shamsabad', ' Agra"'),
(1398, '"Shamsabad', ' Farrukhabad"'),
(1399, 'Sherkot', 'Uttar Pradesh'),
(1400, '"Shikarpur', ' Bulandshahr"'),
(1401, 'Shikohabad', 'Uttar Pradesh'),
(1402, 'Shishgarh', 'Uttar Pradesh'),
(1403, 'Siana', 'Uttar Pradesh'),
(1404, 'Sikanderpur', 'Uttar Pradesh'),
(1405, 'Sikandra Rao', 'Uttar Pradesh'),
(1406, 'Sikandrabad', 'Uttar Pradesh'),
(1407, 'Sirsaganj', 'Uttar Pradesh'),
(1408, 'Sirsi', 'Uttar Pradesh'),
(1409, 'Sitapur', 'Uttar Pradesh'),
(1410, 'Soron', 'Uttar Pradesh'),
(1411, 'Suar', 'Uttar Pradesh'),
(1412, 'Sultanpur', 'Uttar Pradesh'),
(1413, 'Sumerpur', 'Uttar Pradesh'),
(1414, 'Tanda', 'Uttar Pradesh'),
(1415, 'Tanda', 'Uttar Pradesh'),
(1416, 'Tetri Bazar', 'Uttar Pradesh'),
(1417, 'Thakurdwara', 'Uttar Pradesh'),
(1418, 'Thana Bhawan', 'Uttar Pradesh'),
(1419, 'Tilhar', 'Uttar Pradesh'),
(1420, 'Tirwaganj', 'Uttar Pradesh'),
(1421, 'Tulsipur', 'Uttar Pradesh'),
(1422, 'Tundla', 'Uttar Pradesh'),
(1423, 'Unnao', 'Uttar Pradesh'),
(1424, 'Utraula', 'Uttar Pradesh'),
(1425, 'Varanasi', 'Uttar Pradesh'),
(1426, 'Vrindavan', 'Uttar Pradesh'),
(1427, 'Warhapur', 'Uttar Pradesh'),
(1428, 'Zaidpur', 'Uttar Pradesh'),
(1429, 'Zamania', 'Uttar Pradesh'),
(1430, 'Almora', 'Uttarakhand'),
(1431, 'Bazpur', 'Uttarakhand'),
(1432, 'Chamba', 'Uttarakhand'),
(1433, 'Dehradun', 'Uttarakhand'),
(1434, 'Haldwani', 'Uttarakhand'),
(1435, 'Haridwar', 'Uttarakhand'),
(1436, 'Jaspur', 'Uttarakhand'),
(1437, 'Kashipur', 'Uttarakhand'),
(1438, 'kichha', 'Uttarakhand'),
(1439, 'Kotdwara', 'Uttarakhand'),
(1440, 'Manglaur', 'Uttarakhand'),
(1441, 'Mussoorie', 'Uttarakhand'),
(1442, 'Nagla', 'Uttarakhand'),
(1443, 'Nainital', 'Uttarakhand'),
(1444, 'Pauri', 'Uttarakhand'),
(1445, 'Pithoragarh', 'Uttarakhand'),
(1446, 'Ramnagar', 'Uttarakhand'),
(1447, 'Rishikesh', 'Uttarakhand'),
(1448, 'Roorkee', 'Uttarakhand'),
(1449, 'Rudrapur', 'Uttarakhand'),
(1450, 'Sitarganj', 'Uttarakhand'),
(1451, 'Tehri', 'Uttarakhand'),
(1452, 'Muzaffarnagar', 'Uttarpradesh'),
(1453, '"Adra', ' Purulia"'),
(1454, 'Alipurduar', 'West Bengal'),
(1455, 'Arambagh', 'West Bengal'),
(1456, 'Asansol', 'West Bengal'),
(1457, 'Baharampur', 'West Bengal'),
(1458, 'Bally', 'West Bengal'),
(1459, 'Balurghat', 'West Bengal'),
(1460, 'Bankura', 'West Bengal'),
(1461, 'Barakar', 'West Bengal'),
(1462, 'Barasat', 'West Bengal'),
(1463, 'Bardhaman', 'West Bengal'),
(1464, 'Bidhan Nagar', 'West Bengal'),
(1465, 'Chinsura', 'West Bengal'),
(1466, 'Contai', 'West Bengal'),
(1467, 'Cooch Behar', 'West Bengal'),
(1468, 'Darjeeling', 'West Bengal'),
(1469, 'Durgapur', 'West Bengal'),
(1470, 'Haldia', 'West Bengal'),
(1471, 'Howrah', 'West Bengal'),
(1472, 'Islampur', 'West Bengal'),
(1473, 'Jhargram', 'West Bengal'),
(1474, 'Kharagpur', 'West Bengal'),
(1475, 'Kolkata', 'West Bengal'),
(1476, 'Mainaguri', 'West Bengal'),
(1477, 'Mal', 'West Bengal'),
(1478, 'Mathabhanga', 'West Bengal'),
(1479, 'Medinipur', 'West Bengal'),
(1480, 'Memari', 'West Bengal'),
(1481, 'Monoharpur', 'West Bengal'),
(1482, 'Murshidabad', 'West Bengal'),
(1483, 'Nabadwip', 'West Bengal'),
(1484, 'Naihati', 'West Bengal'),
(1485, 'Panchla', 'West Bengal'),
(1486, 'Pandua', 'West Bengal'),
(1487, 'Paschim Punropara', 'West Bengal'),
(1488, 'Purulia', 'West Bengal'),
(1489, 'Raghunathpur', 'West Bengal'),
(1490, 'Raiganj', 'West Bengal'),
(1491, 'Rampurhat', 'West Bengal'),
(1492, 'Ranaghat', 'West Bengal'),
(1493, 'Sainthia', 'West Bengal'),
(1494, 'Santipur', 'West Bengal'),
(1495, 'Siliguri', 'West Bengal'),
(1496, 'Sonamukhi', 'West Bengal'),
(1497, 'Srirampore', 'West Bengal'),
(1498, 'Suri', 'West Bengal'),
(1499, 'Taki', 'West Bengal'),
(1500, 'Tamluk', 'West Bengal'),
(1501, 'Tarakeswar', 'West Bengal'),
(1502, 'Chikmagalur', 'Karnataka'),
(1503, 'Davanagere', 'Karnataka'),
(1504, 'Dharwad', 'Karnataka'),
(1505, 'Gadag', 'Karnataka'),
(1506, 'Chennai', 'Tamil Nadu'),
(1507, 'Coimbatore', 'Tamil Nadu'),
(1508, 'Barrackpur', 'unknown'),
(1509, 'Barwani', 'unknown'),
(1510, 'Basna', 'unknown'),
(1511, 'Bawal', 'unknown'),
(1512, 'Beawar', 'unknown'),
(1513, 'Berhampur', 'unknown'),
(1514, 'Bhajanpura', 'unknown'),
(1515, 'Bhandara', 'unknown'),
(1516, 'Bharatpur', 'unknown'),
(1517, 'Bharthana', 'unknown'),
(1518, 'Bhilai', 'unknown'),
(1519, 'Bhilwara', 'unknown'),
(1520, 'Bhinmal', 'unknown'),
(1521, 'Bhiwandi', 'unknown'),
(1522, 'Bhusawal', 'unknown'),
(1523, 'Bidar', 'unknown'),
(1524, 'Bijnaur', 'unknown'),
(1525, 'Bilara', 'unknown'),
(1527, 'Budaun', 'unknown'),
(1528, 'Bulandshahr', 'unknown'),
(1529, 'Burla', 'unknown'),
(1532, 'Chakeri', 'unknown'),
(1533, 'Champawat', 'unknown'),
(1534, 'Chandil', 'unknown'),
(1535, 'Chandrapur', 'unknown'),
(1536, 'Chapirevula', 'unknown'),
(1537, 'Charkhari', 'unknown'),
(1538, 'Charkhi Dadri', 'unknown'),
(1539, 'Chhindwara', 'unknown'),
(1540, 'Chiplun', 'unknown'),
(1541, 'Chitrakoot', 'unknown'),
(1542, 'Churu', 'unknown'),
(1543, 'Dalkhola', 'unknown'),
(1544, 'Damoh', 'unknown'),
(1545, 'Daund', 'unknown'),
(1546, 'Dehgam', 'unknown'),
(1547, 'Devgarh', 'unknown'),
(1548, 'Dhulian', 'unknown'),
(1549, 'Dumdum', 'unknown'),
(1550, 'Dwarka1', 'unknown'),
(1551, 'Etah', 'unknown'),
(1552, 'Faizabad', 'unknown'),
(1553, 'Falna', 'unknown'),
(1554, 'Farrukhabad', 'unknown'),
(1555, 'Fatehgarh', 'unknown'),
(1556, 'Fatehpur Chaurasi', 'unknown'),
(1557, 'Fatehpur Sikri', 'unknown'),
(1558, 'Firozabad', 'unknown'),
(1559, 'Gadchiroli', 'unknown'),
(1560, 'Gandhidham', 'unknown'),
(1561, 'Ganjam', 'unknown'),
(1562, 'Ghatampur', 'unknown'),
(1563, 'Ghatanji', 'unknown'),
(1564, 'Ghaziabad', 'unknown'),
(1565, 'Ghazipur', 'unknown'),
(1566, 'Goa Velha', 'unknown'),
(1567, 'Gokak', 'unknown'),
(1568, 'Gondiya', 'unknown'),
(1569, 'Gorakhpur', 'unknown'),
(1571, 'Guna', 'unknown'),
(1572, 'Hanumangarh', 'unknown'),
(1573, 'Harda', 'unknown'),
(1574, 'Harsawa', 'unknown'),
(1575, 'Hastinapur', 'unknown'),
(1576, 'Hathras', 'unknown'),
(1579, 'Jagadhri', 'unknown'),
(1580, 'Jais', 'unknown'),
(1581, 'Jaitaran', 'unknown'),
(1582, 'Jalgaon', 'unknown'),
(1583, 'Jalore', 'unknown'),
(1584, 'Jhabua', 'unknown'),
(1585, 'Jhalawar', 'unknown'),
(1586, 'Jhunjhunu', 'unknown'),
(1588, 'Junnar', 'unknown'),
(1589, 'Kailaras', 'unknown'),
(1590, 'Kalburgi', 'unknown'),
(1591, 'Kalimpong', 'unknown'),
(1592, 'Kamthi', 'unknown'),
(1593, 'Kanpur', 'unknown'),
(1594, 'Karad', 'unknown'),
(1595, 'Keylong', 'unknown'),
(1596, 'Kheri', 'unknown'),
(1598, 'Khurai', 'unknown'),
(1600, 'Kodad', 'unknown'),
(1601, 'Konnagar', 'unknown'),
(1602, 'Krishnanagar', 'unknown'),
(1603, 'Kuchinda', 'unknown'),
(1605, 'Madhyamgram', 'unknown'),
(1606, 'Mahabaleswar', 'unknown'),
(1608, 'Mahoba', 'unknown'),
(1609, 'Mahwa', 'unknown'),
(1614, 'Manesar', 'unknown'),
(1615, 'Mangalagiri', 'unknown'),
(1616, 'Mira-Bhayandar', 'unknown'),
(1617, 'Mirzapur', 'unknown'),
(1618, 'Mithapur', 'unknown'),
(1619, 'Mohania', 'unknown'),
(1620, 'Mokama', 'unknown'),
(1621, 'Moradabad', 'unknown'),
(1622, 'Mukatsar', 'unknown'),
(1623, 'Nagalapuram', 'unknown');

-- --------------------------------------------------------

--
-- Table structure for table `cleavedetails`
--

CREATE TABLE IF NOT EXISTS `cleavedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(15) DEFAULT NULL,
  `shortName` varchar(20) NOT NULL,
  `yn` varchar(1) NOT NULL COMMENT '"name" leave applicable y- yes n- no',
  `earnType` text NOT NULL,
  `earnCalc` int(7) DEFAULT NULL COMMENT '1- calculation 2-fixed',
  `formula` int(1) NOT NULL,
  `maxLimit` int(7) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_leavedetails` (`clientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company Leave Details' AUTO_INCREMENT=23 ;

--
-- Dumping data for table `cleavedetails`
--

INSERT INTO `cleavedetails` (`id`, `clientId`, `name`, `shortName`, `yn`, `earnType`, `earnCalc`, `formula`, `maxLimit`, `isActive`, `inActiveDate`) VALUES
(5, 3, 'Casual Leave', 'CL', 'Y', 'M', 2, 0, 0, NULL, NULL),
(6, 3, 'Medical Leave', 'ML', 'Y', 'M', 2, 0, 0, NULL, NULL),
(7, 3, 'Earn Leave', 'EL', 'Y', 'Y', 1, 0, 0, NULL, NULL),
(8, 3, 'Privilege Leave', 'PL', 'Y', 'Y', 2, 0, 0, NULL, NULL),
(9, 3, 'Special Leave', 'SPL', 'Y', 'M', 2, 0, 0, NULL, NULL),
(10, 3, 'Other', 'OL', 'Y', 'M', 2, 0, 0, NULL, NULL),
(11, 6, 'Casual Leave', 'CL', 'N', 'N', -1, 0, -1, NULL, NULL),
(12, 6, 'Medical Leave', 'ML', 'Y', 'Y', 1, 0, 100, NULL, NULL),
(13, 6, 'Earn Leave', 'EL', 'N', 'N', -1, 0, -1, NULL, NULL),
(14, 6, 'Privilege Leave', 'PL', 'N', 'N', -1, 0, -1, NULL, NULL),
(15, 6, 'Special Leave', 'SPL', 'N', 'N', -1, 0, -1, NULL, NULL),
(16, 6, 'Other', 'OL', 'N', 'N', -1, 0, -1, NULL, NULL),
(17, 1, 'Casual Leave', 'CL', 'N', 'N', -1, 0, -1, NULL, NULL),
(18, 1, 'Medical Leave', 'ML', 'N', 'N', -1, 0, -1, NULL, NULL),
(19, 1, 'Earn Leave', 'EL', 'Y', 'M', 2, 0, 0, NULL, NULL),
(20, 1, 'Privilege Leave', 'PL', 'N', 'N', -1, 0, -1, NULL, NULL),
(21, 1, 'Special Leave', 'SPL', 'N', 'N', -1, 0, -1, NULL, NULL),
(22, 1, 'Other', 'OL', 'N', 'N', -1, 0, -1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clientdetails`
--

CREATE TABLE IF NOT EXISTS `clientdetails` (
  `id` int(11) NOT NULL,
  `groupId` int(7) DEFAULT NULL COMMENT '(G+)Id of head branch',
  `govtAddId` int(11) DEFAULT NULL COMMENT 'foreign key from govtofficeaddress table',
  `minWageId` int(11) DEFAULT NULL COMMENT 'foreign key from mwgovtrule',
  `minWegeState` varchar(30) DEFAULT NULL COMMENT 'name of state',
  `name` varchar(150) NOT NULL DEFAULT '' COMMENT 'Company Name',
  `sName` varchar(15) DEFAULT NULL COMMENT 'Short Company Name',
  `nature` varchar(50) DEFAULT NULL COMMENT 'Work type of company',
  `address` varchar(500) DEFAULT NULL COMMENT 'Company Address',
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `cAddress` varchar(500) DEFAULT NULL COMMENT 'Communication Address',
  `areaCode` varchar(15) DEFAULT NULL,
  `companyEmail` varchar(100) DEFAULT NULL,
  `phNo1` varchar(15) DEFAULT NULL,
  `phNo2` varchar(15) DEFAULT NULL,
  `email1` varchar(100) DEFAULT NULL,
  `email2` varchar(100) DEFAULT NULL,
  `mobile1` varchar(15) DEFAULT NULL,
  `mobile2` varchar(15) DEFAULT NULL,
  `person1` varchar(100) DEFAULT NULL,
  `person2` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `NoOffWeek` double DEFAULT NULL COMMENT 'off days in a week',
  `typeOfOff` varchar(15) DEFAULT NULL COMMENT 'Fixed or Rotational',
  `typeOfWage` varchar(2) NOT NULL COMMENT 'D- daily M- monthly B-Both',
  `esiNo` varchar(20) NOT NULL,
  `epfNo` varchar(20) NOT NULL,
  `factActNo` varchar(20) NOT NULL,
  `esiYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `epfYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `epsYN` tinyint(1) DEFAULT NULL,
  `pTaxYN` tinyint(1) DEFAULT NULL COMMENT 'Profesional tax',
  `factActYN` tinyint(1) DEFAULT NULL,
  `branchYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `deptYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `contYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable(For contractor)',
  `allowanceYN` int(11) NOT NULL COMMENT '1-allowance applicable 0-not applicable',
  `oTaxYN` int(11) NOT NULL COMMENT 'Other Tax',
  `othAct` int(4) DEFAULT NULL,
  `bonusYN` int(11) NOT NULL,
  `othActNo` varchar(15) DEFAULT NULL,
  `advYN` int(11) NOT NULL,
  `dedYN` int(11) NOT NULL,
  `tdsYN` int(11) NOT NULL,
  `canteenYN` int(11) NOT NULL,
  `loanYN` int(11) NOT NULL,
  `otherded1YN` int(11) NOT NULL,
  `otherded2YN` int(11) NOT NULL,
  `otherded1Name` varchar(20) NOT NULL,
  `otherded2Name` varchar(20) NOT NULL,
  `panCard` varchar(15) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL COMMENT '0 - InActive 1 - Active',
  `activeDate` timestamp NULL DEFAULT NULL,
  `inActiveDate` timestamp NULL DEFAULT NULL,
  `compImage` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Client Details';

--
-- Dumping data for table `clientdetails`
--

INSERT INTO `clientdetails` (`id`, `groupId`, `govtAddId`, `minWageId`, `minWegeState`, `name`, `sName`, `nature`, `address`, `city`, `pin`, `district`, `state`, `cAddress`, `areaCode`, `companyEmail`, `phNo1`, `phNo2`, `email1`, `email2`, `mobile1`, `mobile2`, `person1`, `person2`, `website`, `NoOffWeek`, `typeOfOff`, `typeOfWage`, `esiNo`, `epfNo`, `factActNo`, `esiYN`, `epfYN`, `epsYN`, `pTaxYN`, `factActYN`, `branchYN`, `deptYN`, `contYN`, `allowanceYN`, `oTaxYN`, `othAct`, `bonusYN`, `othActNo`, `advYN`, `dedYN`, `tdsYN`, `canteenYN`, `loanYN`, `otherded1YN`, `otherded2YN`, `otherded1Name`, `otherded2Name`, `panCard`, `isActive`, `activeDate`, `inActiveDate`, `compImage`) VALUES
(1, NULL, NULL, NULL, NULL, 'Deven Bhooshan Cloud Computing Softwares Services private limited', 'DB', 'Service Based', 'B-1/62 Raj Enclave Rajendra Nagar', '1090', '342001', NULL, 'Rajasthan', 'Same', '0', 'devenbhooshan@gmail..com', '9799912120', '9799912120', 'rd@rd.org', 'DR@dr.ord', '9799912120', '9799912120', 'RD', 'DR', 'db.org', NULL, NULL, 'B', '', '', '', 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, NULL, 1, 1, 1, 1, 0, 0, 0, '', '', NULL, NULL, '2013-05-22 12:53:00', '2013-05-22 12:53:00', NULL),
(2, 0, 0, 0, '', 'Pradeep Jain fsfafsasd', 'PI Jain', '', '', '1090', '324005', '', 'Rajasthan', '', '0', 'pijain24@gmail.com', '', '', '', '', '', '', '', '', '', 0, '', 'M', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-22 13:08:15', '2013-05-22 13:08:15', NULL),
(3, 0, 0, 0, '', 'Reeta', 'RD', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', 'B', '', '', '', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, '', 1, 1, 1, 1, 1, 1, 0, 'Deven', '', '', 0, '2013-05-28 07:26:31', '2013-05-28 07:26:31', NULL),
(4, 0, 0, 0, '', 'RD', 'RD', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', 'D', '', '', '', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, '', 1, 1, 1, 1, 1, 0, 0, '', '', '', 0, '2013-05-29 09:30:02', '2013-05-29 09:30:02', NULL),
(5, 0, 0, 0, '', 'sas', 'sasa', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-29 09:41:12', '2013-05-29 09:41:12', NULL),
(6, 0, 0, 0, '', 'agsf', 'sfgas', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-29 09:45:49', '2013-05-29 09:45:49', NULL),
(7, 0, 0, 0, '', 'Hello Textiles', 'HT', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-29 09:47:17', '2013-05-29 09:47:17', NULL),
(8, 0, 0, 0, '', 'sasa', 'asassa', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-29 09:50:42', '2013-05-29 09:50:42', NULL),
(9, 0, 0, 0, '', 'sasa', 'sasas', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-29 09:51:16', '2013-05-29 09:51:16', NULL),
(10, 0, 0, 0, '', 'Deven', 'DB', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-05-29 10:17:01', '2013-05-29 10:17:01', NULL),
(11, 0, 0, 0, '', 'Dagur Placement Service', 'dafdsf', '', '', '0', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', 0, '', 'D', '', '', '', 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-06-25 07:09:03', '2013-06-25 07:09:03', NULL),
(12, 0, 0, 0, '', 'Dagur Placement Service', 'DPS', ' ', '  ', '0', '  ', '', ' ', ' ', '0', '', ' ', '  ', ' ', '  ', ' ', '  ', '  ', ' ', ' ', 0, '', 'D', '', '', '', 1, 1, 1, 1, 1, NULL, 1, 1, 0, 1, 1, 1, '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, '2013-06-25 07:10:10', '2013-06-25 07:10:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `covertimerule`
--

CREATE TABLE IF NOT EXISTS `covertimerule` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `sdh` int(1) DEFAULT NULL COMMENT '1-single 2- double 3- half',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `covertimerule`
--

INSERT INTO `covertimerule` (`id`, `clientId`, `sdh`) VALUES
(1, 1, 2),
(3, 2, 10000),
(4, 6, 3),
(5, 3, 3),
(6, 13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cowrdetail`
--

CREATE TABLE IF NOT EXISTS `cowrdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `ownerMang` int(2) DEFAULT NULL COMMENT '0 - Owner , 1 - Manager',
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `dateOfJoining` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `mobile1` varchar(15) DEFAULT NULL,
  `mobile2` varchar(15) DEFAULT NULL,
  `inActiveDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ownersdetail` (`clientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `cowrdetail`
--

INSERT INTO `cowrdetail` (`id`, `clientId`, `ownerMang`, `name`, `designation`, `address`, `dateOfJoining`, `email1`, `email2`, `mobile1`, `mobile2`, `inActiveDate`, `isActive`) VALUES
(27, 1, 0, 'asasas', '-1', '', '2013-05-25 13:59:53', '', '', '', '', '0000-00-00 00:00:00', NULL),
(28, 1, 1, 'zxzx', '-1', 'xzxzx', '2013-05-25 14:00:03', '', '', '', '', '0000-00-00 00:00:00', NULL),
(29, 2, 0, 'PI', '-1', '', '2013-05-25 14:01:08', '', '', '', '', '0000-00-00 00:00:00', NULL),
(30, 2, 0, 'SJ', '-1', '', '2013-05-25 14:01:08', '', '', '', '', '0000-00-00 00:00:00', NULL),
(31, 2, 1, 'RJ', '-1', '', '2013-05-25 14:01:20', '', '', '', '', '0000-00-00 00:00:00', NULL),
(32, 2, 0, 'HJ', '-1', '', '2013-05-25 14:01:40', '', '', '', '', '0000-00-00 00:00:00', NULL),
(33, 1, 0, 'asas', '-1', 'assasggahsas', '2013-05-25 14:35:18', '', '', '', '', '0000-00-00 00:00:00', NULL),
(34, 4, 0, '', '-1', '', '2013-05-29 09:30:53', '', '', '', '', '0000-00-00 00:00:00', NULL),
(35, 4, 0, '', '-1', 'asasa', '2013-05-29 09:31:20', '', '', '', '', '0000-00-00 00:00:00', NULL),
(36, 4, 0, '', '', '', '2013-05-29 09:31:20', '', '', '', '', '0000-00-00 00:00:00', NULL),
(37, 10, 1, 'sasa', '-1', '', '2013-05-29 10:17:30', '', '', '', '', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpfdetail`
--

CREATE TABLE IF NOT EXISTS `cpfdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `pfRuleId` int(11) DEFAULT NULL,
  `govtAddId` int(11) DEFAULT NULL,
  `pfNo` varchar(20) DEFAULT NULL,
  `pfTDate` date DEFAULT NULL COMMENT 'Temp Coverage Date',
  `pfFDate` date DEFAULT NULL COMMENT 'Final Coverage Date',
  `pfRate` int(11) NOT NULL COMMENT '1-full wage 2-Acc to gove rules 3-worker-to-worker 4-company rule',
  `pfWage` varchar(20) NOT NULL COMMENT 'if pfrate=4 then what will be the value of wage',
  `userId` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `pfDownloadPath` varchar(100) DEFAULT NULL,
  `insptrName` varchar(50) DEFAULT NULL,
  `inspDate` date DEFAULT NULL,
  `inspMonth` date DEFAULT NULL,
  `period` varchar(50) DEFAULT NULL COMMENT 'inspection period from and to',
  `inspNo` varchar(20) DEFAULT NULL,
  `replyNo` varchar(20) DEFAULT NULL,
  `replyDate` date DEFAULT NULL,
  `challanAmt` varchar(20) DEFAULT NULL,
  `challanNo` varchar(20) DEFAULT NULL,
  `cDeptDate` date DEFAULT NULL COMMENT 'challan deposite date',
  `pfInspPath` varchar(100) DEFAULT NULL,
  `brPfNoYN` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pfdetail` (`govtAddId`),
  KEY `FK_pfdetail_client` (`clientId`),
  KEY `FK_pfdetail_pfrule` (`pfRuleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company PF Details' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cpfdetail`
--

INSERT INTO `cpfdetail` (`id`, `clientId`, `pfRuleId`, `govtAddId`, `pfNo`, `pfTDate`, `pfFDate`, `pfRate`, `pfWage`, `userId`, `password`, `pfDownloadPath`, `insptrName`, `inspDate`, `inspMonth`, `period`, `inspNo`, `replyNo`, `replyDate`, `challanAmt`, `challanNo`, `cDeptDate`, `pfInspPath`, `brPfNoYN`) VALUES
(1, 1, NULL, NULL, '1234567', '0000-00-00', '0000-00-00', 3, '-1', '', '', '', '', '0000-00-00', '0000-00-00', '', '', '', '0000-00-00', '', '', '0000-00-00', '', 0),
(2, 2, NULL, NULL, '162512462516111', '0000-00-00', '0000-00-00', 1, '-1', 'deven', 'yuqywuqw', 'localhost', 'Deven', '0000-00-00', '0000-00-00', '12', '9799912120', '878787', '0000-00-00', '10000', '1000000', '0000-00-00', 'q', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cresichallan`
--

CREATE TABLE IF NOT EXISTS `cresichallan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `ruleId` int(11) DEFAULT NULL COMMENT 'Foreign Key from esigovrule',
  `month` varchar(6) DEFAULT NULL,
  `type` int(2) DEFAULT NULL COMMENT 'Regular, Inspection, Int,Demage',
  `noWrker` int(8) DEFAULT NULL,
  `noExWrker` int(8) DEFAULT NULL,
  `contWrker` int(8) DEFAULT NULL,
  `contExWrker` int(8) DEFAULT NULL,
  `wage` int(12) DEFAULT NULL,
  `exWage` int(12) DEFAULT NULL,
  `contWage` int(12) DEFAULT NULL,
  `contExWage` int(12) DEFAULT NULL,
  `wrkerShare` int(10) DEFAULT NULL,
  `emprShare` int(10) DEFAULT NULL,
  `chNo` varchar(15) DEFAULT NULL,
  `chGDate` date DEFAULT NULL,
  `chDepDate` date DEFAULT NULL,
  `chDepType` varchar(15) DEFAULT NULL,
  `onlineId` int(11) DEFAULT NULL COMMENT 'Online Transaction Id',
  `bankName` varchar(15) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crpfchallan`
--

CREATE TABLE IF NOT EXISTS `crpfchallan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `ruleId` int(11) DEFAULT NULL,
  `month` varchar(6) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `wrker` int(8) DEFAULT NULL,
  `exWrker` int(8) DEFAULT NULL,
  `conWrker` int(8) DEFAULT NULL,
  `conExWrker` int(8) DEFAULT NULL,
  `exWage` int(12) DEFAULT NULL,
  `exConWage` int(12) DEFAULT NULL,
  `epfWage` int(12) DEFAULT NULL COMMENT 'A/C 01',
  `epsWage` int(12) DEFAULT NULL COMMENT 'A/C 02',
  `insWage` int(12) DEFAULT NULL COMMENT 'A/C 21',
  `ac1W` int(10) DEFAULT NULL,
  `ac1E` int(10) DEFAULT NULL,
  `ac2E` int(10) DEFAULT NULL,
  `ac10E` int(10) DEFAULT NULL,
  `ac21E` int(10) DEFAULT NULL,
  `ac22E` int(10) DEFAULT NULL,
  `total` int(10) DEFAULT NULL,
  `chNo` varchar(15) DEFAULT NULL,
  `chGDate` date DEFAULT NULL,
  `chDepDate` date DEFAULT NULL,
  `chDepType` varchar(15) DEFAULT NULL,
  `onlineId` varchar(15) DEFAULT NULL,
  `bankName` varchar(15) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cwagestrudynamic`
--

CREATE TABLE IF NOT EXISTS `cwagestrudynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `sName` varchar(5) DEFAULT NULL COMMENT 'Short Name',
  `esiYN` varchar(1) DEFAULT NULL COMMENT 'ESI Applicable or not ',
  `epfYN` varchar(1) DEFAULT NULL COMMENT 'EPF Applicable or not',
  `epsYN` varchar(1) DEFAULT NULL COMMENT 'EPS Applicable or not',
  `pTax` varchar(1) DEFAULT NULL COMMENT 'P Tax Applicable or not',
  `ot` varchar(1) DEFAULT NULL COMMENT 'Over Time Applicable or not',
  `gratuity` varchar(1) DEFAULT NULL,
  `bonus` varchar(1) DEFAULT NULL,
  `others` varchar(1) DEFAULT NULL,
  `type` int(2) DEFAULT NULL COMMENT 'Fixed , Calculated',
  `calc` varchar(2) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `esi_file_path`
--

CREATE TABLE IF NOT EXISTS `esi_file_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `path` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gendepartmaster`
--

CREATE TABLE IF NOT EXISTS `gendepartmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deptName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gendesgmaster`
--

CREATE TABLE IF NOT EXISTS `gendesgmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT 'designation',
  `type` varchar(20) DEFAULT NULL COMMENT 'Skilled,Un Skilled etc',
  `mCategory` varchar(2) DEFAULT NULL COMMENT 'A,B,C,D,E(Fact Act)',
  `fCategory` varchar(2) DEFAULT NULL COMMENT 'F,G,H,I,J(Fact Act)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `genowrdegmaster`
--

CREATE TABLE IF NOT EXISTS `genowrdegmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'for designation of owner dropdown',
  `designation` varchar(30) DEFAULT NULL COMMENT 'like partner , propriter etc',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `genowrdegmaster`
--

INSERT INTO `genowrdegmaster` (`id`, `designation`) VALUES
(1, 'Propewriter'),
(2, 'Partner'),
(3, 'Director'),
(4, 'Managing Director'),
(5, 'Chairman'),
(6, 'Secratory'),
(7, 'President'),
(8, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `genskillmaster`
--

CREATE TABLE IF NOT EXISTS `genskillmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'For dropdown like skilled un skilled etc',
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `genskillmaster`
--

INSERT INTO `genskillmaster` (`id`, `name`) VALUES
(1, 'Un Skilled'),
(2, 'Semi Skilled'),
(3, 'Skilled'),
(4, 'Super Skill');

-- --------------------------------------------------------

--
-- Table structure for table `govtesirule`
--

CREATE TABLE IF NOT EXISTS `govtesirule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activeDate` date DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL COMMENT 'Presently not using',
  `workerShare` double DEFAULT NULL,
  `emprShare` double DEFAULT NULL,
  `maxLimit` int(11) DEFAULT NULL,
  `minLimit` int(11) DEFAULT NULL,
  `maxHLimit` int(11) DEFAULT NULL COMMENT 'for handicaped(25000)',
  `inActDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `govtesirule`
--

INSERT INTO `govtesirule` (`id`, `activeDate`, `category`, `workerShare`, `emprShare`, `maxLimit`, `minLimit`, `maxHLimit`, `inActDate`, `isActive`) VALUES
(1, '2008-08-01', NULL, 1.75, 4.75, 15000, 3000, 25000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `govtminwagerule`
--

CREATE TABLE IF NOT EXISTS `govtminwagerule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) DEFAULT NULL COMMENT 'Treated as id',
  `type` varchar(20) DEFAULT NULL,
  `dRate` int(7) DEFAULT NULL,
  `mRate` int(7) DEFAULT NULL,
  `roDRate` int(7) DEFAULT NULL,
  `roMRate` int(7) DEFAULT NULL,
  `activeDate` date DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `govtminwagerule`
--

INSERT INTO `govtminwagerule` (`id`, `state`, `type`, `dRate`, `mRate`, `roDRate`, `roMRate`, `activeDate`, `isActive`) VALUES
(1, 'Rajasthan', 'UnSkilled', 166, 4316, 166, 4350, '0000-00-00', '0'),
(2, 'Rajasthan', 'SemiSkilled', 176, 4576, 176, 4600, '0000-00-00', '0'),
(3, 'Rajasthan', 'Skilled', 186, 4836, 176, 4850, '0000-00-00', '0'),
(4, 'Rajasthan', 'Super Skilled', 236, 6136, 236, 6150, '0000-00-00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `govtoffaddress`
--

CREATE TABLE IF NOT EXISTS `govtoffaddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(30) DEFAULT NULL COMMENT 'ESI Pf labour factory act correspondence addresses',
  `city` varchar(30) DEFAULT NULL,
  `area` varchar(30) DEFAULT NULL,
  `eOffHead` varchar(40) DEFAULT NULL COMMENT 'e - esi',
  `eOffAdd` varchar(200) DEFAULT NULL,
  `eOffType` varchar(15) DEFAULT NULL,
  `pOffHead` varchar(40) DEFAULT NULL COMMENT 'p - pf',
  `pOffAdd` varchar(200) DEFAULT NULL,
  `pOffType` varchar(15) DEFAULT NULL,
  `lOffHead` varchar(40) DEFAULT NULL COMMENT 'l - labor',
  `lOffAdd` varchar(200) DEFAULT NULL,
  `lOffType` varchar(15) DEFAULT NULL,
  `fOffHead` varchar(40) DEFAULT NULL,
  `fOffAdd` varchar(200) DEFAULT NULL COMMENT 'f - factory act',
  `fOffType` varchar(15) DEFAULT NULL,
  `exOffHead` varchar(40) DEFAULT NULL COMMENT 'ex - employment exchange',
  `exOffAdd` varchar(200) DEFAULT NULL,
  `exOffType` varchar(15) DEFAULT NULL,
  `ppFlag` tinyint(1) DEFAULT NULL COMMENT '0 - Public, 1 - Private',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `govtpfrule`
--

CREATE TABLE IF NOT EXISTS `govtpfrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actDate` varchar(20) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `AcNo1W` varchar(20) DEFAULT NULL,
  `AcNo1E` varchar(20) DEFAULT NULL,
  `AcNo2E` varchar(20) DEFAULT NULL,
  `AcNo10E` varchar(20) DEFAULT NULL,
  `maxLimitAc10` int(7) DEFAULT NULL COMMENT '541',
  `AcNo21E` varchar(20) DEFAULT NULL,
  `AcNo21I` varchar(20) DEFAULT NULL,
  `AcNo22E` varchar(20) DEFAULT NULL,
  `AcNo22I` varchar(20) DEFAULT NULL,
  `maxEpfLimit` varchar(10) DEFAULT NULL,
  `maxHEpfLimit` varchar(20) NOT NULL,
  `maxPnsLimit` varchar(10) DEFAULT NULL,
  `maxIWrkrLimit` varchar(10) DEFAULT NULL COMMENT 'for international worker',
  `inActiveDate` varchar(20) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `govtpfrule`
--

INSERT INTO `govtpfrule` (`id`, `actDate`, `category`, `AcNo1W`, `AcNo1E`, `AcNo2E`, `AcNo10E`, `maxLimitAc10`, `AcNo21E`, `AcNo21I`, `AcNo22E`, `AcNo22I`, `maxEpfLimit`, `maxHEpfLimit`, `maxPnsLimit`, `maxIWrkrLimit`, `inActiveDate`, `isActive`) VALUES
(1, NULL, NULL, '12', '3.67', '1.1', '8.33', 541, '.5', '0', '.01', '.005', '6500', '25000', '6500', '25000', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `govtptaxrule`
--

CREATE TABLE IF NOT EXISTS `govtptaxrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actDate` date DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `workerShare` double DEFAULT NULL,
  `maxLimit` int(11) DEFAULT NULL,
  `minLimit` int(11) DEFAULT NULL,
  `maxHLimit` int(11) DEFAULT NULL COMMENT '?',
  `inActDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `monthlytransact`
--

CREATE TABLE IF NOT EXISTS `monthlytransact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerid` int(11) NOT NULL,
  `month` date NOT NULL,
  `deduction` varchar(20) NOT NULL,
  `salary` varchar(20) NOT NULL,
  `allowances` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pfleftcode`
--

CREATE TABLE IF NOT EXISTS `pfleftcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `pfcode` varchar(20) NOT NULL,
  `lastDateRequired` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pfleftcode`
--

INSERT INTO `pfleftcode` (`id`, `Name`, `pfcode`, `lastDateRequired`) VALUES
(1, 'Left', 'C', 1),
(2, 'Superannuation', 'S', 1),
(3, 'Retirement', 'R', 1),
(4, 'Death in Service', 'D', 1),
(5, 'Permanent Disablement', 'P', 1);

-- --------------------------------------------------------

--
-- Table structure for table `state_list`
--

CREATE TABLE IF NOT EXISTS `state_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `state_list`
--

INSERT INTO `state_list` (`id`, `state`) VALUES
(1, 'ANDAMAN AND NICOBAR ISLANDS'),
(2, 'ANDHRA PRADESH'),
(3, 'ARUNACHAL PRADESH'),
(4, 'ASSAM'),
(5, 'BIHAR'),
(6, 'CHATTISGARH'),
(7, 'CHANDIGARH'),
(8, 'DAMAN AND DIU'),
(9, 'DELHI'),
(10, 'DADRA AND NAGAR HAVELI'),
(11, 'GOA'),
(12, 'GUJARAT'),
(13, 'HIMACHAL PRADESH'),
(14, 'HARYANA'),
(15, 'JAMMU AND KASHMIR'),
(16, 'JHARKHAND'),
(17, 'KERALA'),
(18, 'KARNATAKA'),
(19, 'LAKSHADWEEP'),
(20, 'MEGHALAYA'),
(21, 'MAHARASHTRA'),
(22, 'MANIPUR'),
(23, 'MADHYA PRADESH'),
(24, 'MIZORAM'),
(25, 'NAGALAND'),
(26, 'ORISSA'),
(27, 'PUNJAB'),
(28, 'PONDICHERRY'),
(29, 'RAJASTHAN'),
(30, 'SIKKIM'),
(31, 'TAMIL NADU'),
(32, 'TRIPURA'),
(33, 'UTTARAKHAND'),
(34, 'UTTAR PRADESH'),
(35, 'WEST BENGAL');

-- --------------------------------------------------------

--
-- Table structure for table `tpdzerocode`
--

CREATE TABLE IF NOT EXISTS `tpdzerocode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) NOT NULL,
  `esicode` int(11) NOT NULL,
  `lastDateRequired` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tpdzerocode`
--

INSERT INTO `tpdzerocode` (`id`, `Name`, `esicode`, `lastDateRequired`) VALUES
(1, 'On Leave', 1, 0),
(2, 'Left Service', 2, 1),
(3, 'Retired', 3, 1),
(4, 'Expired', 5, 1),
(5, 'Strike/Lockout', 9, 0),
(6, 'Suspension of work', 8, 0),
(7, 'Retrenchment', 10, 1),
(8, 'No work', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_master`
--

CREATE TABLE IF NOT EXISTS `transaction_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `workerId` int(11) NOT NULL,
  `esiRuleId` int(11) NOT NULL,
  `epfRuleId` int(11) NOT NULL,
  `miWageRuleId` int(11) NOT NULL,
  `bonusRuleId` int(11) NOT NULL,
  `wid` int(11) NOT NULL,
  `dw` int(11) NOT NULL,
  `el` int(11) NOT NULL,
  `pl` int(11) NOT NULL,
  `cl` int(11) NOT NULL,
  `ml` int(11) NOT NULL,
  `fhd` int(11) NOT NULL,
  `wh` int(11) NOT NULL,
  `tpd` int(3) NOT NULL,
  `mDays` int(11) NOT NULL,
  `breakDay` int(11) NOT NULL,
  `esiWage` varchar(20) NOT NULL,
  `epfWage` varchar(20) NOT NULL,
  `pnsWage` varchar(11) NOT NULL,
  `pf21Wage` varchar(11) NOT NULL,
  `epfWageArear` varchar(20) NOT NULL,
  `pnsWageArear` varchar(20) NOT NULL,
  `pf21WageArear` varchar(20) NOT NULL,
  `bonusWage` varchar(20) NOT NULL,
  `esiContriWorker` varchar(11) NOT NULL,
  `esiContriClient` varchar(11) NOT NULL,
  `epfContriWorker` varchar(11) NOT NULL,
  `epfContriClient` varchar(11) NOT NULL,
  `pensionByEmployer` varchar(11) NOT NULL,
  `epfWrArear` int(11) NOT NULL,
  `epsEmpArear` int(11) NOT NULL,
  `epfEmpArear` int(11) NOT NULL,
  `pTax` varchar(11) NOT NULL,
  `oTax` varchar(11) NOT NULL,
  `DED` varchar(11) NOT NULL,
  `ADV` varchar(11) NOT NULL,
  `TDS` varchar(11) NOT NULL,
  `LOAN` varchar(20) NOT NULL,
  `CANTEEN` varchar(11) NOT NULL,
  `OTHERDED1` varchar(11) NOT NULL,
  `OTHERDED2` varchar(11) NOT NULL,
  `BASIC` varchar(11) NOT NULL,
  `CCA` varchar(11) NOT NULL,
  `CONVENCE` varchar(11) NOT NULL,
  `DA` varchar(11) NOT NULL,
  `EDU` varchar(11) NOT NULL,
  `HRA` varchar(11) NOT NULL,
  `LUNCH` varchar(11) NOT NULL,
  `MEDICAL` varchar(11) NOT NULL,
  `SPL` varchar(11) NOT NULL,
  `OTHERS` varchar(11) NOT NULL,
  `OTHER1` varchar(11) NOT NULL,
  `OTHER2` varchar(11) NOT NULL,
  `OTHER1AREAR` varchar(11) NOT NULL,
  `OTHER2AREAR` varchar(11) NOT NULL,
  `OTHERSAREAR` varchar(11) NOT NULL,
  `SPLAREAR` varchar(11) NOT NULL,
  `MEDICALAREAR` varchar(11) NOT NULL,
  `LUNCHAREAR` varchar(11) NOT NULL,
  `HRAAREAR` varchar(11) NOT NULL,
  `EDUAREAR` varchar(11) NOT NULL,
  `DAAREAR` varchar(11) NOT NULL,
  `CONVENCEAREAR` varchar(11) NOT NULL,
  `CCAAREAR` varchar(11) NOT NULL,
  `BASICAREAR` varchar(11) NOT NULL,
  `PI` int(20) NOT NULL,
  `netPayment` varchar(11) NOT NULL,
  `month` varchar(11) NOT NULL,
  `year` varchar(10) NOT NULL,
  `deptCode` varchar(11) NOT NULL,
  `esiReasonCode` varchar(11) NOT NULL,
  `pfReasonCode` varchar(11) NOT NULL,
  `lastdatesi` varchar(11) NOT NULL,
  `lastdateepf` varchar(11) NOT NULL,
  `joinDate` varchar(11) NOT NULL,
  `statusEsiYN` int(11) NOT NULL,
  `statusEpfYN` int(11) NOT NULL,
  `statusPnsYN` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `transaction_master`
--

INSERT INTO `transaction_master` (`id`, `clientId`, `workerId`, `esiRuleId`, `epfRuleId`, `miWageRuleId`, `bonusRuleId`, `wid`, `dw`, `el`, `pl`, `cl`, `ml`, `fhd`, `wh`, `tpd`, `mDays`, `breakDay`, `esiWage`, `epfWage`, `pnsWage`, `pf21Wage`, `epfWageArear`, `pnsWageArear`, `pf21WageArear`, `bonusWage`, `esiContriWorker`, `esiContriClient`, `epfContriWorker`, `epfContriClient`, `pensionByEmployer`, `epfWrArear`, `epsEmpArear`, `epfEmpArear`, `pTax`, `oTax`, `DED`, `ADV`, `TDS`, `LOAN`, `CANTEEN`, `OTHERDED1`, `OTHERDED2`, `BASIC`, `CCA`, `CONVENCE`, `DA`, `EDU`, `HRA`, `LUNCH`, `MEDICAL`, `SPL`, `OTHERS`, `OTHER1`, `OTHER2`, `OTHER1AREAR`, `OTHER2AREAR`, `OTHERSAREAR`, `SPLAREAR`, `MEDICALAREAR`, `LUNCHAREAR`, `HRAAREAR`, `EDUAREAR`, `DAAREAR`, `CONVENCEAREAR`, `CCAAREAR`, `BASICAREAR`, `PI`, `netPayment`, `month`, `year`, `deptCode`, `esiReasonCode`, `pfReasonCode`, `lastdatesi`, `lastdateepf`, `joinDate`, `statusEsiYN`, `statusEpfYN`, `statusPnsYN`) VALUES
(80, 1, 1, 0, 0, 0, 0, 0, 10, 1, 0, 0, 0, 1, 1, 13, 31, 0, '6290', '419', '419', '419', '0', '', '', '', '111', '299', '50', '', '35', 0, 0, 0, '', '', '', '', '', '', '', '', '', '6290', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '12', '2012', '0', '', '-8', '', '', '12-01-2013', 1, 1, 1),
(81, 1, 11, 0, 0, 0, 0, 0, 10, 1, 0, 0, 0, 1, 1, 13, 31, 0, '6290', '419', '0', '419', '0', '', '', '', '111', '299', '50', '', '0', 0, 0, 0, '', '', '', '', '', '', '', '', '', '6290', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '12', '2012', '0', '', '-8', '', '', '  12-03-201', 1, 1, 0),
(82, 1, 11, 0, 0, 0, 0, 0, 10, 1, 0, 0, 0, 1, 1, 13, 31, 0, '6290', '419', '0', '419', '0', '', '', '', '111', '299', '50', '', '0', 0, 0, 0, '', '', '', '', '', '', '', '', '', '6290', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '1', '2013', '0', '', '-8', '', '', '  12-03-201', 1, 1, 0),
(83, 1, 10, 0, 0, 0, 0, 0, 10, 1, 0, 0, 0, 1, 1, 13, 31, 0, '4194', '419', '0', '419', '0', '', '', '', '74', '200', '50', '', '0', 0, 0, 0, '', '', '', '', '', '', '', '', '', '4194', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1, '', '12', '2012', '0', '', '-8', '', '', ' ', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wattendancerecord`
--

CREATE TABLE IF NOT EXISTS `wattendancerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerid` int(11) NOT NULL,
  `month` date NOT NULL,
  `present` int(11) NOT NULL,
  `absent` int(11) NOT NULL,
  `leave` int(11) NOT NULL,
  `festival` int(11) NOT NULL,
  `suspend` int(11) NOT NULL,
  `rest` int(11) NOT NULL,
  `halfday` int(11) NOT NULL,
  `sick` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `ontour` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Attendance records of the workers' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wfamilydetail`
--

CREATE TABLE IF NOT EXISTS `wfamilydetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `relation` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `recidingYN` varchar(2) DEFAULT NULL COMMENT 'whether lives with him or not',
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `dist` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `wfamilydetail`
--

INSERT INTO `wfamilydetail` (`id`, `workerId`, `name`, `relation`, `dob`, `recidingYN`, `address`, `city`, `pin`, `dist`, `state`) VALUES
(2, 1, 'Reeta', '', '0000-00-00', '0', '', '1090', 'asasa', '', 'Rajasthan'),
(3, 1, 'Deven Bhooshan', '', '0000-00-00', '0', 'asas', '1137', 'asasasasas', '', 'Rajasthan'),
(4, 1, 'Deven Bhooshan', '', '0000-00-00', '0', '', '', '', '', ''),
(5, 3, 'gfg', '', '0000-00-00', '1', '', '0', '', '', ''),
(6, 3, 'asdfgh', '', '0000-00-00', '0', 'gfg', '1090', '', '', 'Rajasthan'),
(7, 3, 'qwerty', '', '0000-00-00', '1', '', '1090', '', '', 'Rajasthan'),
(8, 2, ' jhjh', '  ', '0000-00-00', '1', ' ', '0', 'dsddsd', 'asas', 'hhgh'),
(9, 2, 'jjhjh', '', '0000-00-00', '0', '', '', '', '', ''),
(10, 5, 'Deven Bhooshan', 'ajsaj', '0000-00-00', '1', ' ', '0', '', ' ', ''),
(11, 5, 'sjashj', '', '0000-00-00', '1', '', '1090', '', '', 'Rajasthan'),
(12, 5, 'jaksjas', 'sjhj', '0000-00-00', '1', '', '1090', '', '', ''),
(13, 6, '', '', '0000-00-00', '1', '', '0', '', '', ''),
(14, 6, '', '', '0000-00-00', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wnomineedetail`
--

CREATE TABLE IF NOT EXISTS `wnomineedetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `relation` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `dist` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `perShare` int(6) DEFAULT NULL,
  `gNameAdd` varchar(500) DEFAULT NULL,
  `esi` varchar(10) DEFAULT NULL,
  `epf` varchar(10) DEFAULT NULL,
  `eps` varchar(10) DEFAULT NULL,
  `labDept` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `wnomineedetail`
--

INSERT INTO `wnomineedetail` (`id`, `workerId`, `name`, `relation`, `dob`, `address`, `city`, `pin`, `dist`, `state`, `perShare`, `gNameAdd`, `esi`, `epf`, `eps`, `labDept`) VALUES
(1, 3, 'sas', 'dsdsdds', '0000-00-00', 'sasas', '1090', '', '', 'Rajasthan', 0, '', '', '', '', ''),
(2, 3, 'asas', 'sasas', '0000-00-00', 's', '1377', 's', 's', 'Uttar Pradesh', 0, '', '', '', '', ''),
(3, 1, 'sgagsfsa', '', '0000-00-00', '', '1293', '', '', 'Tripura', 0, '', 'asas', '', 'asas', ''),
(4, 1, 'zxxzxz', '', '0000-00-00', 'sasas', '1303', '', '', 'Uttar Pradesh', 0, '', '', '', '', ''),
(5, 2, '', 'sds', '0000-00-00', '', '0', '', '', '', 0, '', '', '', '', ''),
(6, 2, '', '', '0000-00-00', '', '', '', '', '', 0, '', '', '', '', ''),
(7, 5, '', '', '1993-12-20', '', '0', '', '', '', 0, '', '', '', '', ''),
(8, 2145, '', '', '0000-00-00', '', '', '', '', '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `workerdetail`
--

CREATE TABLE IF NOT EXISTS `workerdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `deptId` int(11) DEFAULT NULL,
  `designationId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `FH` varchar(2) DEFAULT NULL COMMENT 'Father or Husband',
  `fName` varchar(50) DEFAULT NULL,
  `hName` varchar(50) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `dob` varchar(20) DEFAULT NULL,
  `qualification` varchar(30) DEFAULT NULL,
  `disability` varchar(10) DEFAULT NULL,
  `offDay1` varchar(15) DEFAULT NULL,
  `offDay2` varchar(15) DEFAULT NULL,
  `iWrker` varchar(1) DEFAULT NULL,
  `basicRate` varchar(20) NOT NULL COMMENT 'basic Rate ',
  `mobNo` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `pAdd` varchar(200) DEFAULT NULL COMMENT 'p - permanent',
  `pCity` varchar(50) DEFAULT NULL,
  `pPin` varchar(10) DEFAULT NULL,
  `pState` varchar(50) DEFAULT NULL,
  `cAdd` varchar(200) DEFAULT NULL COMMENT 'c - current address',
  `cCity` varchar(50) DEFAULT NULL,
  `cPin` varchar(10) DEFAULT NULL,
  `cState` varchar(50) DEFAULT NULL,
  `esiYN` tinyint(1) DEFAULT NULL,
  `epfYN` tinyint(1) DEFAULT NULL,
  `epsYN` varchar(5) DEFAULT NULL,
  `esiWage` varchar(20) NOT NULL,
  `epfWage` varchar(20) NOT NULL,
  `epsNoMonth` varchar(20) NOT NULL COMMENT 'EPS no month',
  `volEpfYN` tinyint(1) DEFAULT NULL COMMENT 'voluntary pf yn',
  `volEpfPer` int(2) DEFAULT NULL,
  `esiNo` varchar(30) DEFAULT NULL,
  `esiDisp` varchar(80) DEFAULT NULL COMMENT 'dispencery',
  `pfNo` varchar(30) DEFAULT NULL,
  `fdoj` varchar(20) DEFAULT NULL,
  `fDojEsi` varchar(20) DEFAULT NULL,
  `fDojEps` varchar(20) DEFAULT NULL,
  `fDojEpf` varchar(20) DEFAULT NULL,
  `doj` varchar(20) DEFAULT NULL,
  `dojEsi` varchar(20) DEFAULT NULL,
  `dojEps` varchar(20) DEFAULT NULL,
  `dojEpf` varchar(20) DEFAULT NULL,
  `doeEsi` varchar(20) DEFAULT NULL,
  `exitReasonEsi` varchar(15) DEFAULT NULL,
  `doeEps` varchar(20) DEFAULT NULL,
  `exitReasonEps` varchar(15) DEFAULT NULL,
  `doeEpf` varchar(20) DEFAULT NULL,
  `exitReasonEpf` varchar(15) DEFAULT NULL,
  `tdsCategory` varchar(15) DEFAULT NULL COMMENT 'men, women ,sr citizen',
  `eid` varchar(20) DEFAULT NULL COMMENT 'eid as per pf new table',
  `eidName` varchar(20) DEFAULT NULL,
  `aadhar` varchar(40) DEFAULT NULL,
  `bankAcNo` varchar(20) DEFAULT NULL,
  `ifsc` varchar(15) DEFAULT NULL,
  `img` blob,
  `esiImg` blob,
  `thumbImg` blob,
  `sigImg` blob,
  `isLiableOt` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `workerdetail`
--

INSERT INTO `workerdetail` (`id`, `groupId`, `clientId`, `deptId`, `designationId`, `name`, `FH`, `fName`, `hName`, `gender`, `status`, `dob`, `qualification`, `disability`, `offDay1`, `offDay2`, `iWrker`, `basicRate`, `mobNo`, `email`, `pAdd`, `pCity`, `pPin`, `pState`, `cAdd`, `cCity`, `cPin`, `cState`, `esiYN`, `epfYN`, `epsYN`, `esiWage`, `epfWage`, `epsNoMonth`, `volEpfYN`, `volEpfPer`, `esiNo`, `esiDisp`, `pfNo`, `fdoj`, `fDojEsi`, `fDojEps`, `fDojEpf`, `doj`, `dojEsi`, `dojEps`, `dojEpf`, `doeEsi`, `exitReasonEsi`, `doeEps`, `exitReasonEps`, `doeEpf`, `exitReasonEpf`, `tdsCategory`, `eid`, `eidName`, `aadhar`, `bankAcNo`, `ifsc`, `img`, `esiImg`, `thumbImg`, `sigImg`, `isLiableOt`) VALUES
(1, NULL, 1, NULL, NULL, 'Deven Bhooshan', 'F', ' ', ' ', 'M', 'M', '22-12-1943', ' ', 'Y', '1', '1', 'Y', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '1', '15000', '1000', '', 0, 0, '123456789', '', NULL, NULL, '', NULL, NULL, '12-01-2012', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, NULL, 1, NULL, NULL, 'Nishant Kumar', 'F', ' ', ' ', 'M', 'M', '22-12-1946', ' ', 'Y', '1', '1', 'Y', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '0', '10000', '1000', '', 0, 0, '123456789', '', NULL, NULL, '', NULL, NULL, ' ', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, NULL, 1, NULL, NULL, 'Pradeep jain', 'F', ' ', ' ', 'M', 'M', ' 12-03-96', '  ', 'Y', '1', '1', 'Y', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '0', '15000', '1000', '', 0, 0, '121212121212', '', NULL, NULL, '', NULL, NULL, '  12-03-2012', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wwagestrudynamic`
--

CREATE TABLE IF NOT EXISTS `wwagestrudynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Dynamic',
  `workerId` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `sName` varchar(5) DEFAULT NULL COMMENT 'Short name',
  `rate` int(7) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `activeDate` date DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=243 ;

--
-- Dumping data for table `wwagestrudynamic`
--

INSERT INTO `wwagestrudynamic` (`id`, `workerId`, `name`, `sName`, `rate`, `isActive`, `activeDate`, `inActiveDate`) VALUES
(223, 6, 'BASIC', 'BASIC', 1000, NULL, NULL, NULL),
(225, 7, 'BASIC', 'BASIC', 15000, NULL, NULL, NULL),
(227, 8, 'BASIC', 'BASIC', 5000, NULL, NULL, NULL),
(228, 9, 'BASIC', 'BASIC', 15000, NULL, NULL, NULL),
(239, 11, 'BASIC', 'BASIC', 15000, NULL, NULL, NULL),
(241, 1, 'BASIC', 'BASIC', 15000, NULL, NULL, NULL),
(242, 10, 'BASIC', 'BASIC', 10000, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ccontractor`
--
ALTER TABLE `ccontractor`
  ADD CONSTRAINT `FK_contractor` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cdeptmaster`
--
ALTER TABLE `cdeptmaster`
  ADD CONSTRAINT `FK_department_client` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cesidetail`
--
ALTER TABLE `cesidetail`
  ADD CONSTRAINT `FK_esidetail` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`),
  ADD CONSTRAINT `FK_esidetail_esiaddress` FOREIGN KEY (`esiAddId`) REFERENCES `govtoffaddress` (`id`),
  ADD CONSTRAINT `FK_esidetail_esirule` FOREIGN KEY (`esiRuleId`) REFERENCES `govtesirule` (`id`);

--
-- Constraints for table `cfactactdetail`
--
ALTER TABLE `cfactactdetail`
  ADD CONSTRAINT `FK_factoryactdetail` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cleavedetails`
--
ALTER TABLE `cleavedetails`
  ADD CONSTRAINT `FK_leavedetails` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cowrdetail`
--
ALTER TABLE `cowrdetail`
  ADD CONSTRAINT `FK_ownersdetail` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cpfdetail`
--
ALTER TABLE `cpfdetail`
  ADD CONSTRAINT `FK_pfdetail_client` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`),
  ADD CONSTRAINT `FK_pfdetail_pfrule` FOREIGN KEY (`pfRuleId`) REFERENCES `govtpfrule` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
