-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2013 at 12:28 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `payrolldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `callowancedetail`
--

CREATE TABLE IF NOT EXISTS `callowancedetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `typeOfAllowance` int(1) NOT NULL COMMENT '1- calculation 2- fixed',
  `calcFormula` int(2) DEFAULT NULL COMMENT 'calculation formula 1-9',
  `bonus` int(1) NOT NULL COMMENT '1- yes 0- no',
  `ot` int(11) NOT NULL COMMENT '1- yes 0 - no',
  `esi` tinyint(1) DEFAULT NULL,
  `epf` tinyint(1) DEFAULT NULL,
  `eps` tinyint(1) DEFAULT NULL,
  `pTax` tinyint(1) DEFAULT NULL,
  `oTax` int(11) NOT NULL COMMENT 'other tax/act 1-yes 0- no',
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company Dynamic Deduction Rules' AUTO_INCREMENT=97 ;

-- --------------------------------------------------------

--
-- Table structure for table `carea_code`
--

CREATE TABLE IF NOT EXISTS `carea_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_code` int(11) NOT NULL,
  `area_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cbankdetails`
--

CREATE TABLE IF NOT EXISTS `cbankdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL COMMENT 'Foreign key from clientdetails',
  `bankName` varchar(30) DEFAULT NULL,
  `accountNumber` varchar(20) DEFAULT NULL,
  `branchName` varchar(50) DEFAULT NULL,
  `micrCode` varchar(50) DEFAULT NULL,
  `ifscCode` varchar(50) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bankdetails` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Client Bank Details' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cbonusrules`
--

CREATE TABLE IF NOT EXISTS `cbonusrules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `percent` double DEFAULT NULL COMMENT '8.33 to 20',
  `maxWageLimit` int(7) DEFAULT NULL COMMENT '10000',
  `wageLimit` int(7) DEFAULT NULL COMMENT '3500',
  `bonusToAll` tinyint(1) DEFAULT NULL COMMENT 'Y - All workers, N - As per rule',
  `mly/yly` varchar(1) DEFAULT NULL COMMENT 'M - Monthly basis, Y - Yearly Basis',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Company Bonus Rules' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ccontractor`
--

CREATE TABLE IF NOT EXISTS `ccontractor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL COMMENT 'foreign key from clientdetails',
  `groupId` int(11) DEFAULT NULL COMMENT 'company group id ',
  `name` varchar(50) DEFAULT NULL,
  `sName` varchar(20) DEFAULT NULL COMMENT 'Short Name',
  `address` varchar(500) DEFAULT NULL,
  `registrationNumber` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mobile1` varchar(15) DEFAULT NULL,
  `mobile2` varchar(15) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_contractor` (`clientId`),
  KEY `FK_contractor_branch` (`groupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `cdedfixed`
--

CREATE TABLE IF NOT EXISTS `cdedfixed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `adv` varchar(12) DEFAULT NULL,
  `ded` varchar(12) DEFAULT NULL,
  `fine` varchar(12) DEFAULT NULL,
  `tds` varchar(12) DEFAULT NULL,
  `cded` varchar(12) DEFAULT NULL,
  `loan` varchar(12) DEFAULT NULL,
  `ded1` varchar(12) DEFAULT NULL,
  `ded2` varchar(12) DEFAULT NULL,
  `ded3` varchar(12) DEFAULT NULL,
  `ded4` varchar(12) DEFAULT NULL,
  `ded5` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Company Fixed Deduction Rules' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cdegmaster`
--

CREATE TABLE IF NOT EXISTS `cdegmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT 'designation',
  `type` varchar(20) DEFAULT NULL COMMENT 'Skilled,Un Skilled etc',
  `mCategory` varchar(2) DEFAULT NULL COMMENT 'A,B,C,D,E(Fact Act)',
  `fCategory` varchar(2) DEFAULT NULL COMMENT 'F,G,H,I,J(Fact Act)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Predefined Designations' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cdeptmaster`
--

CREATE TABLE IF NOT EXISTS `cdeptmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL COMMENT 'Foreign Key from Client details',
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `sName` varchar(20) DEFAULT NULL COMMENT 'Short Name',
  `startDate` date DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_department_client` (`clientId`),
  KEY `FK_department_branch` (`groupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `cesidetail`
--

CREATE TABLE IF NOT EXISTS `cesidetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `esiRuleId` int(11) DEFAULT NULL,
  `esiAddId` int(11) DEFAULT NULL,
  `esiNo` int(11) NOT NULL,
  `esiTDate` date DEFAULT NULL,
  `esiFDate` date DEFAULT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `esiDownloadPath` varchar(100) DEFAULT NULL,
  `insptrName` varchar(30) DEFAULT NULL,
  `inspDate` date DEFAULT NULL,
  `ecPeriod` varchar(50) DEFAULT NULL,
  `lcPeriod` varchar(50) DEFAULT NULL,
  `inspLNo` varchar(20) DEFAULT NULL,
  `replyNo` varchar(20) DEFAULT NULL,
  `replyDate` date DEFAULT NULL,
  `challanAmt` varchar(20) DEFAULT NULL,
  `challanNo` varchar(20) DEFAULT NULL,
  `cDepDate` date DEFAULT NULL,
  `esiInspPath` varchar(100) DEFAULT NULL,
  `brEsiNoYN` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_esidetail` (`clientId`),
  KEY `FK_esidetail_esiaddress` (`esiAddId`),
  KEY `FK_esidetail_esirule` (`esiRuleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company ESI Details' AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `cfactactdetail`
--

CREATE TABLE IF NOT EXISTS `cfactactdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `nature` varchar(15) DEFAULT NULL COMMENT 'All details of table as per form no 2',
  `regNo` varchar(20) DEFAULT NULL,
  `renDate` date DEFAULT NULL,
  `renFrm` date DEFAULT NULL,
  `renTo` date DEFAULT NULL,
  `expDate` date DEFAULT NULL,
  `maxNoWrkr` int(7) DEFAULT NULL COMMENT 'Maximum No Of Workers proposed to be employed on any day during the year',
  `maxNoWrkrLastYr` int(7) DEFAULT NULL COMMENT 'Maximum no of workers employed on any one day during last 12 months',
  `avgWrkr` int(7) DEFAULT NULL COMMENT 'No to be ordinaryily employed in factory',
  `hpConn` varchar(10) DEFAULT NULL,
  `mapAprvDetail` varchar(200) DEFAULT NULL,
  `typeOfPremis` varchar(15) DEFAULT NULL,
  `ownerDetail` varchar(200) DEFAULT NULL COMMENT 'premis owner details name and address',
  `authName` varchar(100) DEFAULT NULL COMMENT 'Waste disposal approval authority',
  `ylyLFee` varchar(15) DEFAULT NULL,
  `totFee` varchar(15) DEFAULT NULL COMMENT '1 - Single, 2 - Double, 3 - Tripple',
  `dupLFee` varchar(10) DEFAULT NULL,
  `diffFee` varchar(10) DEFAULT NULL,
  `noOfConn` varchar(10) DEFAULT NULL,
  `ddDetail` varchar(200) DEFAULT NULL COMMENT 'DDNo, Bank,Date,Amount',
  `insptrName` varchar(20) DEFAULT NULL,
  `inspDate` date DEFAULT NULL,
  `inspLNO` varchar(30) DEFAULT NULL,
  `replyNo` varchar(20) DEFAULT NULL,
  `replyDate` date DEFAULT NULL,
  `factInspPath` varchar(100) DEFAULT NULL,
  `shift1` varchar(50) DEFAULT NULL,
  `shift2` varchar(50) DEFAULT NULL,
  `shift3` varchar(50) DEFAULT NULL,
  `generalShift` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_factoryactdetail` (`clientId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Company Factory Act Details' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  `city_state` varchar(100) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1624 ;

-- --------------------------------------------------------

--
-- Table structure for table `cleavedetails`
--

CREATE TABLE IF NOT EXISTS `cleavedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(15) DEFAULT NULL,
  `shortName` varchar(20) NOT NULL,
  `yn` varchar(1) NOT NULL COMMENT '"name" leave applicable y- yes n- no',
  `earnType` text NOT NULL,
  `earnCalc` int(7) DEFAULT NULL COMMENT '1- calculation 2-fixed',
  `formula` int(1) NOT NULL,
  `maxLimit` int(7) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_leavedetails` (`clientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company Leave Details' AUTO_INCREMENT=23 ;

-- --------------------------------------------------------

--
-- Table structure for table `clientdetails`
--

CREATE TABLE IF NOT EXISTS `clientdetails` (
  `id` int(11) NOT NULL,
  `groupId` int(7) DEFAULT NULL COMMENT '(G+)Id of head branch',
  `govtAddId` int(11) DEFAULT NULL COMMENT 'foreign key from govtofficeaddress table',
  `minWageId` int(11) DEFAULT NULL COMMENT 'foreign key from mwgovtrule',
  `minWegeState` varchar(30) DEFAULT NULL COMMENT 'name of state',
  `name` varchar(150) NOT NULL DEFAULT '' COMMENT 'Company Name',
  `sName` varchar(15) DEFAULT NULL COMMENT 'Short Company Name',
  `nature` varchar(50) DEFAULT NULL COMMENT 'Work type of company',
  `address` varchar(500) DEFAULT NULL COMMENT 'Company Address',
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `cAddress` varchar(500) DEFAULT NULL COMMENT 'Communication Address',
  `areaCode` varchar(15) DEFAULT NULL,
  `companyEmail` varchar(100) DEFAULT NULL,
  `phNo1` varchar(15) DEFAULT NULL,
  `phNo2` varchar(15) DEFAULT NULL,
  `email1` varchar(100) DEFAULT NULL,
  `email2` varchar(100) DEFAULT NULL,
  `mobile1` varchar(15) DEFAULT NULL,
  `mobile2` varchar(15) DEFAULT NULL,
  `person1` varchar(100) DEFAULT NULL,
  `person2` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `NoOffWeek` double DEFAULT NULL COMMENT 'off days in a week',
  `typeOfOff` varchar(15) DEFAULT NULL COMMENT 'Fixed or Rotational',
  `typeOfWage` varchar(2) NOT NULL COMMENT 'D- daily M- monthly B-Both',
  `esiNo` varchar(20) NOT NULL,
  `epfNo` varchar(20) NOT NULL,
  `factActNo` varchar(20) NOT NULL,
  `esiYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `epfYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `epsYN` tinyint(1) DEFAULT NULL,
  `pTaxYN` tinyint(1) DEFAULT NULL COMMENT 'Profesional tax',
  `factActYN` tinyint(1) DEFAULT NULL,
  `branchYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `deptYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable',
  `contYN` tinyint(1) DEFAULT NULL COMMENT '0 - Not Applicable, 1 - Applicable(For contractor)',
  `allowanceYN` int(11) NOT NULL COMMENT '1-allowance applicable 0-not applicable',
  `oTaxYN` int(11) NOT NULL COMMENT 'Other Tax',
  `othAct` int(4) DEFAULT NULL,
  `bonusYN` int(11) NOT NULL,
  `othActNo` varchar(15) DEFAULT NULL,
  `advYN` int(11) NOT NULL,
  `dedYN` int(11) NOT NULL,
  `tdsYN` int(11) NOT NULL,
  `canteenYN` int(11) NOT NULL,
  `loanYN` int(11) NOT NULL,
  `otherded1YN` int(11) NOT NULL,
  `otherded2YN` int(11) NOT NULL,
  `otherded1Name` varchar(20) NOT NULL,
  `otherded2Name` varchar(20) NOT NULL,
  `panCard` varchar(15) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL COMMENT '0 - InActive 1 - Active',
  `activeDate` timestamp NULL DEFAULT NULL,
  `inActiveDate` timestamp NULL DEFAULT NULL,
  `compImage` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Client Details';

-- --------------------------------------------------------

--
-- Table structure for table `covertimerule`
--

CREATE TABLE IF NOT EXISTS `covertimerule` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `sdh` int(1) DEFAULT NULL COMMENT '1-single 2- double 3- half',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `cowrdetail`
--

CREATE TABLE IF NOT EXISTS `cowrdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `ownerMang` int(2) DEFAULT NULL COMMENT '0 - Owner , 1 - Manager',
  `name` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `dateOfJoining` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email1` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `mobile1` varchar(15) DEFAULT NULL,
  `mobile2` varchar(15) DEFAULT NULL,
  `inActiveDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ownersdetail` (`clientId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Table structure for table `cpfdetail`
--

CREATE TABLE IF NOT EXISTS `cpfdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `pfRuleId` int(11) DEFAULT NULL,
  `govtAddId` int(11) DEFAULT NULL,
  `pfNo` varchar(20) DEFAULT NULL,
  `pfTDate` date DEFAULT NULL COMMENT 'Temp Coverage Date',
  `pfFDate` date DEFAULT NULL COMMENT 'Final Coverage Date',
  `pfRate` int(11) NOT NULL COMMENT '1-full wage 2-Acc to gove rules 3-worker-to-worker 4-company rule',
  `pfWage` varchar(20) NOT NULL COMMENT 'if pfrate=4 then what will be the value of wage',
  `userId` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `pfDownloadPath` varchar(100) DEFAULT NULL,
  `insptrName` varchar(50) DEFAULT NULL,
  `inspDate` date DEFAULT NULL,
  `inspMonth` date DEFAULT NULL,
  `period` varchar(50) DEFAULT NULL COMMENT 'inspection period from and to',
  `inspNo` varchar(20) DEFAULT NULL,
  `replyNo` varchar(20) DEFAULT NULL,
  `replyDate` date DEFAULT NULL,
  `challanAmt` varchar(20) DEFAULT NULL,
  `challanNo` varchar(20) DEFAULT NULL,
  `cDeptDate` date DEFAULT NULL COMMENT 'challan deposite date',
  `pfInspPath` varchar(100) DEFAULT NULL,
  `brPfNoYN` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pfdetail` (`govtAddId`),
  KEY `FK_pfdetail_client` (`clientId`),
  KEY `FK_pfdetail_pfrule` (`pfRuleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Company PF Details' AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `cresichallan`
--

CREATE TABLE IF NOT EXISTS `cresichallan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `ruleId` int(11) DEFAULT NULL COMMENT 'Foreign Key from esigovrule',
  `month` varchar(6) DEFAULT NULL,
  `type` int(2) DEFAULT NULL COMMENT 'Regular, Inspection, Int,Demage',
  `noWrker` int(8) DEFAULT NULL,
  `noExWrker` int(8) DEFAULT NULL,
  `contWrker` int(8) DEFAULT NULL,
  `contExWrker` int(8) DEFAULT NULL,
  `wage` int(12) DEFAULT NULL,
  `exWage` int(12) DEFAULT NULL,
  `contWage` int(12) DEFAULT NULL,
  `contExWage` int(12) DEFAULT NULL,
  `wrkerShare` int(10) DEFAULT NULL,
  `emprShare` int(10) DEFAULT NULL,
  `chNo` varchar(15) DEFAULT NULL,
  `chGDate` date DEFAULT NULL,
  `chDepDate` date DEFAULT NULL,
  `chDepType` varchar(15) DEFAULT NULL,
  `onlineId` int(11) DEFAULT NULL COMMENT 'Online Transaction Id',
  `bankName` varchar(15) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crpfchallan`
--

CREATE TABLE IF NOT EXISTS `crpfchallan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `ruleId` int(11) DEFAULT NULL,
  `month` varchar(6) DEFAULT NULL,
  `type` int(2) DEFAULT NULL,
  `wrker` int(8) DEFAULT NULL,
  `exWrker` int(8) DEFAULT NULL,
  `conWrker` int(8) DEFAULT NULL,
  `conExWrker` int(8) DEFAULT NULL,
  `exWage` int(12) DEFAULT NULL,
  `exConWage` int(12) DEFAULT NULL,
  `epfWage` int(12) DEFAULT NULL COMMENT 'A/C 01',
  `epsWage` int(12) DEFAULT NULL COMMENT 'A/C 02',
  `insWage` int(12) DEFAULT NULL COMMENT 'A/C 21',
  `ac1W` int(10) DEFAULT NULL,
  `ac1E` int(10) DEFAULT NULL,
  `ac2E` int(10) DEFAULT NULL,
  `ac10E` int(10) DEFAULT NULL,
  `ac21E` int(10) DEFAULT NULL,
  `ac22E` int(10) DEFAULT NULL,
  `total` int(10) DEFAULT NULL,
  `chNo` varchar(15) DEFAULT NULL,
  `chGDate` date DEFAULT NULL,
  `chDepDate` date DEFAULT NULL,
  `chDepType` varchar(15) DEFAULT NULL,
  `onlineId` varchar(15) DEFAULT NULL,
  `bankName` varchar(15) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cwagestrudynamic`
--

CREATE TABLE IF NOT EXISTS `cwagestrudynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `sName` varchar(5) DEFAULT NULL COMMENT 'Short Name',
  `esiYN` varchar(1) DEFAULT NULL COMMENT 'ESI Applicable or not ',
  `epfYN` varchar(1) DEFAULT NULL COMMENT 'EPF Applicable or not',
  `epsYN` varchar(1) DEFAULT NULL COMMENT 'EPS Applicable or not',
  `pTax` varchar(1) DEFAULT NULL COMMENT 'P Tax Applicable or not',
  `ot` varchar(1) DEFAULT NULL COMMENT 'Over Time Applicable or not',
  `gratuity` varchar(1) DEFAULT NULL,
  `bonus` varchar(1) DEFAULT NULL,
  `others` varchar(1) DEFAULT NULL,
  `type` int(2) DEFAULT NULL COMMENT 'Fixed , Calculated',
  `calc` varchar(2) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `esi_file_path`
--

CREATE TABLE IF NOT EXISTS `esi_file_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `path` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gendepartmaster`
--

CREATE TABLE IF NOT EXISTS `gendepartmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deptName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gendesgmaster`
--

CREATE TABLE IF NOT EXISTS `gendesgmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT 'designation',
  `type` varchar(20) DEFAULT NULL COMMENT 'Skilled,Un Skilled etc',
  `mCategory` varchar(2) DEFAULT NULL COMMENT 'A,B,C,D,E(Fact Act)',
  `fCategory` varchar(2) DEFAULT NULL COMMENT 'F,G,H,I,J(Fact Act)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `genowrdegmaster`
--

CREATE TABLE IF NOT EXISTS `genowrdegmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'for designation of owner dropdown',
  `designation` varchar(30) DEFAULT NULL COMMENT 'like partner , propriter etc',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `genskillmaster`
--

CREATE TABLE IF NOT EXISTS `genskillmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'For dropdown like skilled un skilled etc',
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `govtesirule`
--

CREATE TABLE IF NOT EXISTS `govtesirule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activeDate` date DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL COMMENT 'Presently not using',
  `workerShare` double DEFAULT NULL,
  `emprShare` double DEFAULT NULL,
  `maxLimit` int(11) DEFAULT NULL,
  `minLimit` int(11) DEFAULT NULL,
  `maxHLimit` int(11) DEFAULT NULL COMMENT 'for handicaped(25000)',
  `inActDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `govtminwagerule`
--

CREATE TABLE IF NOT EXISTS `govtminwagerule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) DEFAULT NULL COMMENT 'Treated as id',
  `type` varchar(20) DEFAULT NULL,
  `dRate` int(7) DEFAULT NULL,
  `mRate` int(7) DEFAULT NULL,
  `roDRate` int(7) DEFAULT NULL,
  `roMRate` int(7) DEFAULT NULL,
  `activeDate` date DEFAULT NULL,
  `isActive` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `govtoffaddress`
--

CREATE TABLE IF NOT EXISTS `govtoffaddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(30) DEFAULT NULL COMMENT 'ESI Pf labour factory act correspondence addresses',
  `city` varchar(30) DEFAULT NULL,
  `area` varchar(30) DEFAULT NULL,
  `eOffHead` varchar(40) DEFAULT NULL COMMENT 'e - esi',
  `eOffAdd` varchar(200) DEFAULT NULL,
  `eOffType` varchar(15) DEFAULT NULL,
  `pOffHead` varchar(40) DEFAULT NULL COMMENT 'p - pf',
  `pOffAdd` varchar(200) DEFAULT NULL,
  `pOffType` varchar(15) DEFAULT NULL,
  `lOffHead` varchar(40) DEFAULT NULL COMMENT 'l - labor',
  `lOffAdd` varchar(200) DEFAULT NULL,
  `lOffType` varchar(15) DEFAULT NULL,
  `fOffHead` varchar(40) DEFAULT NULL,
  `fOffAdd` varchar(200) DEFAULT NULL COMMENT 'f - factory act',
  `fOffType` varchar(15) DEFAULT NULL,
  `exOffHead` varchar(40) DEFAULT NULL COMMENT 'ex - employment exchange',
  `exOffAdd` varchar(200) DEFAULT NULL,
  `exOffType` varchar(15) DEFAULT NULL,
  `ppFlag` tinyint(1) DEFAULT NULL COMMENT '0 - Public, 1 - Private',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `govtpfrule`
--

CREATE TABLE IF NOT EXISTS `govtpfrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actDate` varchar(20) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `AcNo1W` varchar(20) DEFAULT NULL,
  `AcNo1E` varchar(20) DEFAULT NULL,
  `AcNo2E` varchar(20) DEFAULT NULL,
  `AcNo10E` varchar(20) DEFAULT NULL,
  `maxLimitAc10` int(7) DEFAULT NULL COMMENT '541',
  `AcNo21E` varchar(20) DEFAULT NULL,
  `AcNo21I` varchar(20) DEFAULT NULL,
  `AcNo22E` varchar(20) DEFAULT NULL,
  `AcNo22I` varchar(20) DEFAULT NULL,
  `maxEpfLimit` varchar(10) DEFAULT NULL,
  `maxHEpfLimit` varchar(20) NOT NULL,
  `maxPnsLimit` varchar(10) DEFAULT NULL,
  `maxIWrkrLimit` varchar(10) DEFAULT NULL COMMENT 'for international worker',
  `inActiveDate` varchar(20) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `govtptaxrule`
--

CREATE TABLE IF NOT EXISTS `govtptaxrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actDate` date DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `workerShare` double DEFAULT NULL,
  `maxLimit` int(11) DEFAULT NULL,
  `minLimit` int(11) DEFAULT NULL,
  `maxHLimit` int(11) DEFAULT NULL COMMENT '?',
  `inActDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `monthlytransact`
--

CREATE TABLE IF NOT EXISTS `monthlytransact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerid` int(11) NOT NULL,
  `month` date NOT NULL,
  `deduction` varchar(20) NOT NULL,
  `salary` varchar(20) NOT NULL,
  `allowances` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pfleftcode`
--

CREATE TABLE IF NOT EXISTS `pfleftcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  `pfcode` varchar(20) NOT NULL,
  `lastDateRequired` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `state_list`
--

CREATE TABLE IF NOT EXISTS `state_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `tpdzerocode`
--

CREATE TABLE IF NOT EXISTS `tpdzerocode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) NOT NULL,
  `esicode` int(11) NOT NULL,
  `lastDateRequired` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_master`
--

CREATE TABLE IF NOT EXISTS `transaction_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `workerId` int(11) NOT NULL,
  `esiRuleId` int(11) NOT NULL,
  `epfRuleId` int(11) NOT NULL,
  `miWageRuleId` int(11) NOT NULL,
  `bonusRuleId` int(11) NOT NULL,
  `wid` int(11) NOT NULL,
  `dw` int(11) NOT NULL,
  `el` int(11) NOT NULL,
  `pl` int(11) NOT NULL,
  `cl` int(11) NOT NULL,
  `ml` int(11) NOT NULL,
  `fhd` int(11) NOT NULL,
  `wh` int(11) NOT NULL,
  `tpd` int(3) NOT NULL,
  `mDays` int(11) NOT NULL,
  `breakDay` int(11) NOT NULL,
  `esiWage` varchar(20) NOT NULL,
  `epfWage` varchar(20) NOT NULL,
  `pnsWage` varchar(11) NOT NULL,
  `pf21Wage` varchar(11) NOT NULL,
  `epfWageArear` varchar(20) NOT NULL,
  `pnsWageArear` varchar(20) NOT NULL,
  `pf21WageArear` varchar(20) NOT NULL,
  `bonusWage` varchar(20) NOT NULL,
  `esiContriWorker` varchar(11) NOT NULL,
  `esiContriClient` varchar(11) NOT NULL,
  `epfContriWorker` varchar(11) NOT NULL,
  `epfContriClient` varchar(11) NOT NULL,
  `pensionByEmployer` varchar(11) NOT NULL,
  `epfWrArear` int(11) NOT NULL,
  `epsEmpArear` int(11) NOT NULL,
  `epfEmpArear` int(11) NOT NULL,
  `pTax` varchar(11) NOT NULL,
  `oTax` varchar(11) NOT NULL,
  `DED` varchar(11) NOT NULL,
  `ADV` varchar(11) NOT NULL,
  `TDS` varchar(11) NOT NULL,
  `LOAN` varchar(20) NOT NULL,
  `CANTEEN` varchar(11) NOT NULL,
  `OTHERDED1` varchar(11) NOT NULL,
  `OTHERDED2` varchar(11) NOT NULL,
  `BASIC` varchar(11) NOT NULL,
  `CCA` varchar(11) NOT NULL,
  `CONVENCE` varchar(11) NOT NULL,
  `DA` varchar(11) NOT NULL,
  `EDU` varchar(11) NOT NULL,
  `HRA` varchar(11) NOT NULL,
  `LUNCH` varchar(11) NOT NULL,
  `MEDICAL` varchar(11) NOT NULL,
  `SPL` varchar(11) NOT NULL,
  `OTHERS` varchar(11) NOT NULL,
  `OTHER1` varchar(11) NOT NULL,
  `OTHER2` varchar(11) NOT NULL,
  `OTHER1AREAR` varchar(11) NOT NULL,
  `OTHER2AREAR` varchar(11) NOT NULL,
  `OTHERSAREAR` varchar(11) NOT NULL,
  `SPLAREAR` varchar(11) NOT NULL,
  `MEDICALAREAR` varchar(11) NOT NULL,
  `LUNCHAREAR` varchar(11) NOT NULL,
  `HRAAREAR` varchar(11) NOT NULL,
  `EDUAREAR` varchar(11) NOT NULL,
  `DAAREAR` varchar(11) NOT NULL,
  `CONVENCEAREAR` varchar(11) NOT NULL,
  `CCAAREAR` varchar(11) NOT NULL,
  `BASICAREAR` varchar(11) NOT NULL,
  `PI` int(20) NOT NULL,
  `netPayment` varchar(11) NOT NULL,
  `month` varchar(11) NOT NULL,
  `year` varchar(10) NOT NULL,
  `deptCode` varchar(11) NOT NULL,
  `esiReasonCode` varchar(11) NOT NULL,
  `pfReasonCode` varchar(11) NOT NULL,
  `lastdatesi` varchar(11) NOT NULL,
  `lastdateepf` varchar(11) NOT NULL,
  `joinDate` varchar(11) NOT NULL,
  `statusEsiYN` int(11) NOT NULL,
  `statusEpfYN` int(11) NOT NULL,
  `statusPnsYN` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Table structure for table `wattendancerecord`
--

CREATE TABLE IF NOT EXISTS `wattendancerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerid` int(11) NOT NULL,
  `month` date NOT NULL,
  `present` int(11) NOT NULL,
  `absent` int(11) NOT NULL,
  `leave` int(11) NOT NULL,
  `festival` int(11) NOT NULL,
  `suspend` int(11) NOT NULL,
  `rest` int(11) NOT NULL,
  `halfday` int(11) NOT NULL,
  `sick` int(11) NOT NULL,
  `left` int(11) NOT NULL,
  `ontour` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Attendance records of the workers' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wfamilydetail`
--

CREATE TABLE IF NOT EXISTS `wfamilydetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `relation` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `recidingYN` varchar(2) DEFAULT NULL COMMENT 'whether lives with him or not',
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `dist` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `wnomineedetail`
--

CREATE TABLE IF NOT EXISTS `wnomineedetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workerId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `relation` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(10) DEFAULT NULL,
  `dist` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `perShare` int(6) DEFAULT NULL,
  `gNameAdd` varchar(500) DEFAULT NULL,
  `esi` varchar(10) DEFAULT NULL,
  `epf` varchar(10) DEFAULT NULL,
  `eps` varchar(10) DEFAULT NULL,
  `labDept` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `workerdetail`
--

CREATE TABLE IF NOT EXISTS `workerdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `deptId` int(11) DEFAULT NULL,
  `designationId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `FH` varchar(2) DEFAULT NULL COMMENT 'Father or Husband',
  `fName` varchar(50) DEFAULT NULL,
  `hName` varchar(50) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `dob` varchar(20) DEFAULT NULL,
  `qualification` varchar(30) DEFAULT NULL,
  `disability` varchar(10) DEFAULT NULL,
  `offDay1` varchar(15) DEFAULT NULL,
  `offDay2` varchar(15) DEFAULT NULL,
  `iWrker` varchar(1) DEFAULT NULL,
  `basicRate` varchar(20) NOT NULL COMMENT 'basic Rate ',
  `mobNo` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `pAdd` varchar(200) DEFAULT NULL COMMENT 'p - permanent',
  `pCity` varchar(50) DEFAULT NULL,
  `pPin` varchar(10) DEFAULT NULL,
  `pState` varchar(50) DEFAULT NULL,
  `cAdd` varchar(200) DEFAULT NULL COMMENT 'c - current address',
  `cCity` varchar(50) DEFAULT NULL,
  `cPin` varchar(10) DEFAULT NULL,
  `cState` varchar(50) DEFAULT NULL,
  `esiYN` tinyint(1) DEFAULT NULL,
  `epfYN` tinyint(1) DEFAULT NULL,
  `epsYN` varchar(5) DEFAULT NULL,
  `esiWage` varchar(20) NOT NULL,
  `epfWage` varchar(20) NOT NULL,
  `epsNoMonth` varchar(20) NOT NULL COMMENT 'EPS no month',
  `volEpfYN` tinyint(1) DEFAULT NULL COMMENT 'voluntary pf yn',
  `volEpfPer` int(2) DEFAULT NULL,
  `esiNo` varchar(30) DEFAULT NULL,
  `esiDisp` varchar(80) DEFAULT NULL COMMENT 'dispencery',
  `pfNo` varchar(30) DEFAULT NULL,
  `fdoj` varchar(20) DEFAULT NULL,
  `fDojEsi` varchar(20) DEFAULT NULL,
  `fDojEps` varchar(20) DEFAULT NULL,
  `fDojEpf` varchar(20) DEFAULT NULL,
  `doj` varchar(20) DEFAULT NULL,
  `dojEsi` varchar(20) DEFAULT NULL,
  `dojEps` varchar(20) DEFAULT NULL,
  `dojEpf` varchar(20) DEFAULT NULL,
  `doeEsi` varchar(20) DEFAULT NULL,
  `exitReasonEsi` varchar(15) DEFAULT NULL,
  `doeEps` varchar(20) DEFAULT NULL,
  `exitReasonEps` varchar(15) DEFAULT NULL,
  `doeEpf` varchar(20) DEFAULT NULL,
  `exitReasonEpf` varchar(15) DEFAULT NULL,
  `tdsCategory` varchar(15) DEFAULT NULL COMMENT 'men, women ,sr citizen',
  `eid` varchar(20) DEFAULT NULL COMMENT 'eid as per pf new table',
  `eidName` varchar(20) DEFAULT NULL,
  `aadhar` varchar(40) DEFAULT NULL,
  `bankAcNo` varchar(20) DEFAULT NULL,
  `ifsc` varchar(15) DEFAULT NULL,
  `img` blob,
  `esiImg` blob,
  `thumbImg` blob,
  `sigImg` blob,
  `isLiableOt` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `wwagestrudynamic`
--

CREATE TABLE IF NOT EXISTS `wwagestrudynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Dynamic',
  `workerId` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `sName` varchar(5) DEFAULT NULL COMMENT 'Short name',
  `rate` int(7) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `activeDate` date DEFAULT NULL,
  `inActiveDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=243 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ccontractor`
--
ALTER TABLE `ccontractor`
  ADD CONSTRAINT `FK_contractor` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cdeptmaster`
--
ALTER TABLE `cdeptmaster`
  ADD CONSTRAINT `FK_department_client` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cesidetail`
--
ALTER TABLE `cesidetail`
  ADD CONSTRAINT `FK_esidetail` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`),
  ADD CONSTRAINT `FK_esidetail_esiaddress` FOREIGN KEY (`esiAddId`) REFERENCES `govtoffaddress` (`id`),
  ADD CONSTRAINT `FK_esidetail_esirule` FOREIGN KEY (`esiRuleId`) REFERENCES `govtesirule` (`id`);

--
-- Constraints for table `cfactactdetail`
--
ALTER TABLE `cfactactdetail`
  ADD CONSTRAINT `FK_factoryactdetail` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cleavedetails`
--
ALTER TABLE `cleavedetails`
  ADD CONSTRAINT `FK_leavedetails` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cowrdetail`
--
ALTER TABLE `cowrdetail`
  ADD CONSTRAINT `FK_ownersdetail` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`);

--
-- Constraints for table `cpfdetail`
--
ALTER TABLE `cpfdetail`
  ADD CONSTRAINT `FK_pfdetail_client` FOREIGN KEY (`clientId`) REFERENCES `clientdetails` (`id`),
  ADD CONSTRAINT `FK_pfdetail_pfrule` FOREIGN KEY (`pfRuleId`) REFERENCES `govtpfrule` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
