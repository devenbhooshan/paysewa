<?
require "dbconnect.php";
session_start();
if(isset($_SESSION['compid'])){
	$compid=$_SESSION['compid'];
	$compname=$_SESSION['compname'];
	$resultread=mysql_query("select * from messagebox where `TO`=".$compid." and `READ`='Y'");
	$resultunread=mysql_query("select * from messagebox where `TO`=".$compid." and `READ`='N'");
	$sentmessage=mysql_query("select * from messagebox where `FROM`=".$compid);
?>
<html>
    <head>
        <title>messagebox</title>
		<script type="text/javascript" language="javascript" src="jquery.js"></script>
		<script class="jsbin" src="datatable.js"></script>
		<script>
		/* 
		 * Example init
		 */
		$(document).ready(function(){
			$('#example').dataTable();
		});
		$(document).ready(function(){
			$('#example2').dataTable();
		});
		$(document).ready(function(){
			$('#example3').dataTable();
		});
		</script>
		
		<script type="text/javascript">
		/* 
		 * for lightbox
		 */
		function showlightbox(id)
		{
			document.getElementById(id).style.display='block';
			document.getElementById('fade').style.display='block';
		}
		function hidelightbox(id)
		{
			document.getElementById(id).style.display='none';
			document.getElementById('fade').style.display='none';
		}
		function showreply(id)
		{
			if(document.getElementById(id).style.display=='none'){
				document.getElementById(id).style.display='block';
			}
			else{
				document.getElementById(id).style.display='none';
			}
		}
		function check(id){
			var reply = document.getElementById(id).value;
			if(reply==""){
				alert("Reply box empty");
				return false; 
			}
        }
		</script>
		
        <link rel='stylesheet' href='minbootstrap.css' type='text/css' media='print, projection, screen' />
		 <link rel='stylesheet' href='lightbox.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='datatable.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='header.css' type='text/css' media='print, projection, screen' />
        <style>
            body{margin:0px; padding:0px; color:black; font-family: 'Helvetica Neue';}
			.bold{
			font-weight:bold
			}
			th.center{
				text-align:center;
				background-color:#c0c0c0;
			}
			td.center{
				text-align:center;
			}
        </style>
		<script>
		</script>
	</head>
	<body>

	<div id="fade" class="black_overlay"></div>
	<?//header
	require 'header.php';
	//unread messages----------------------------------------------------------------------
	$norowsunread=mysql_num_rows($resultunread);
	echo '<div style="width:1250px;font-weight:bold;padding:30px;padding-top:0px" class="container well">';
	echo '<p style="font-weight:bold;margin:0px;padding:0px"align="center">Unread Messages</p>';
	$id=0;
	if($norowsunread>0){
		echo '<table  style="width:100%;margin-bottom:10px;padding-top:0px" cellpadding="0" cellspacing="0"  class="display" id="example2">';
			echo '<thead>
				  <tr >
					<th>FROM</th>
					<th>SUBJECT</th>
					<th>TIME</th>
					<th>FILE</th>
				  </tr>
				</thead>';
			echo '<tbody>';
			$i=0;
			while($row=mysql_fetch_array($resultunread)){
				if($i==0){
					echo '<tr class="odd gradeA">';
					$i=1;
				}
				else{
					$i=0;
					echo '<tr class="even gradeA">';
				}
				echo '<td class="center" width="25%">'.$row['FROM'].'</td>
					<td class="center" width="25%"><a color="black" href="#" onclick="showlightbox('.$id.')">'.$row['SUBJECT'].'</a></td>
					<td class="center" width="25%">'.$row['TIME'].'</td>';
					if($row['FILE']!=null){
						echo '<td class="center" width="25%"><a href="'.$row['FILE'].'" >File</a></td></tr>';
					}
					else{
						echo '<td class="center" width="25%">No Attachment</td></tr>';
					}
					
				//lightbox for showing message details
				?>
				
				<div id='<?echo $id?>' class="white_content">					
					<p align="center">Message</p>
					<div class="well2">
						<?echo $row['MESSAGE']?>
					</div>
					<button class="btn btn-info" type="button" onclick="showreply('<?echo "unread".$id."reply";?>')">Reply</button>
					<div style="display:none;" id='<?echo "unread".$id."reply";?>' class="well2">
						<form action="replymessage.php" enctype="multipart/form-data" method="post" onsubmit="return check('<?echo "unread".$id."replytext";?>')">
							<input type="hidden" name="from" value="<?echo $compid?>">
							<input type="hidden" name="to" value="<?echo $row['FROM']?>">
							<input type="hidden" name="time" value="<?echo $row['TIME']?>">
							<input type="hidden" name="subject" value="<?echo $row['SUBJECT']?>">
							<textarea id='<?echo "unread".$id."replytext";?>' name="reply" rows="5" cols="75"></textarea>
							<input name="ufile" type="file" id="ufile" size="50" /><hr>
						<div align="center"><button class="btn-small btn-info">Send</button></div>
						</form>
					</div>
					<form action="readmessage.php" method="post">
						<input type="hidden" name="from" value="<?echo $row['FROM']?>">
						<input type="hidden" name="to" value="<?echo $row['TO']?>">
						<input type="hidden" name="time" value="<?echo $row['TIME']?>">
						<div align="center"><button class="btn btn-info" onclick = "hidelightbox('<?echo $id?>')">Close</button></div>
					</form>
				</div>
						
						
						
				<?
				$id++;
			}
			echo '</tbody>';
		echo '</table>';
	}
	else echo 'No Messages';
	echo '</div>';
	//read messages------------------------------------------------------------------------
	$norowsread=mysql_num_rows($resultread);
	echo '<div style="width:1250px;font-weight:bold;padding:30px;padding-top:0px" class="container well">';
	echo '<p style="font-weight:bold;margin:0px;padding:0px"align="center">Read Messages</p>';
	if($norowsread>0){
		echo '<table  style="width:100%;margin-bottom:10px;padding-top:0px" cellpadding="0" cellspacing="0"  class="display" id="example">';
			echo '<thead>
				  <tr >
					<th>FROM</th>
					<th>SUBJECT</th>
					<th>TIME</th>
					<th>FILE</th>
				  </tr>
				</thead>';
			echo '<tbody>';
			$i=0;
			while($row=mysql_fetch_array($resultread)){
				if($i==0){
					echo '<tr class="odd gradeA">';
					$i=1;
				}
				else{
					$i=0;
					echo '<tr class="even gradeA">';
				}
				echo '<td class="center" width="25%">'.$row['FROM'].'</td>
					<td class="center" width="25%"><a color="black" href="#" onclick="showlightbox('.$id.')">'.$row['SUBJECT'].'</a></td>
					<td class="center" width="25%">'.$row['TIME'].'</td>';
					if($row['FILE']!=null){
						echo '<td class="center" width="25%"><a href="'.$row['FILE'].'" >File</a></td></tr>';
					}
					else{
						echo '<td class="center" width="25%">No Attachment</td></tr>';
					}
				//lightbox for showing message details
				?>
				
				<div id='<?echo $id?>' class="white_content">					
					<p align="center">Message</p>
					<div class="well2">
						<?echo $row['MESSAGE']?>
					</div>
					<button class="btn btn-info" type="button" onclick="showreply('<?echo "read".$id."reply";?>')">Reply</button>
					<div style="display:none;" id='<?echo "read".$id."reply";?>' class="well2">
						<form action="replymessage.php" enctype="multipart/form-data" method="post" onsubmit="return check('<?echo "read".$id."replytext";?>')">
							<input type="hidden" name="from" value="<?echo $compid?>">
							<input type="hidden" name="to" value="<?echo $row['FROM']?>">
							<input type="hidden" name="time" value="<?echo $row['TIME']?>">
							<input type="hidden" name="subject" value="<?echo $row['SUBJECT']?>">
							<textarea id='<?echo "read".$id."replytext";?>' name="reply" rows="5" cols="75"></textarea>
							<input name="ufile" type="file" id="ufile" size="50" /><hr>
						<div align="center"><button class="btn-small btn-info">Send</button></div>
						</form>
					</div>
					<form action="readmessage.php" method="post">
						<input type="hidden" name="from" value="<?echo $row['FROM']?>">
						<input type="hidden" name="to" value="<?echo $row['TO']?>">
						<input type="hidden" name="time" value="<?echo $row['TIME']?>">
						<div align="center"><button class="btn btn-info" onclick = "hidelightbox('<?echo $id?>')">Close</button></div>
					</form>
				</div>
						
						
						
				<?
				$id++;
			}
			echo '</tbody>';
		echo '</table>';
	}
	else echo 'No Messages';
	echo '</div>';	
	//sent messages------------------------------------------------
	$norowssent=mysql_num_rows($sentmessage);
	echo '<div style="width:1250px;font-weight:bold;padding:30px;padding-top:0px" class="container well">';
	echo '<p style="font-weight:bold;margin:0px;padding:0px"align="center">Sent Messages</p>';
	if($norowssent>0){
		echo '<table  style="width:100%;margin-bottom:10px;padding-top:0px" cellpadding="0" cellspacing="0"  class="display" id="example3">';
			echo '<thead>
				  <tr >
					<th>FROM</th>
					<th>SUBJECT</th>
					<th>TIME</th>
					<th>FILE</th>
				  </tr>
				</thead>';
			echo '<tbody>';
			$i=0;
			while($row=mysql_fetch_array($sentmessage)){
				if($i==0){
					echo '<tr class="odd gradeA">';
					$i=1;
				}
				else{
					$i=0;
					echo '<tr class="even gradeA">';
				}
				echo '<td class="center" width="25%">'.$row['FROM'].'</td>
					<td class="center" width="25%"><a color="black" href="#" onclick="showlightbox('.$id.')">'.$row['SUBJECT'].'</a></td>
					<td class="center" width="25%">'.$row['TIME'].'</td>';
					if($row['FILE']!=null){
						echo '<td class="center" width="25%"><a href="'.$row['FILE'].'" >File</a></td></tr>';
					}
					else{
						echo '<td class="center" width="25%">No Attachment</td></tr>';
					}
				//lightbox for showing message details
				?>
				
				<div id='<?echo $id?>' class="white_content">					
					<p align="center">Message</p>
					<div class="well2">
						<?echo $row['MESSAGE']?>
					</div>
					<div align="center"><button class="btn btn-info" onclick = "hidelightbox('<?echo $id?>')">Close</button></div>
				</div>						
				<?
				$id++;
			}
			echo '</tbody>';
		echo '</table>';
	}
	else echo 'No Messages';
	echo '</div>';
	?>
	</body>
</html>

<?
}
else
echo "INVALID ACCESS";
?>
	
		
