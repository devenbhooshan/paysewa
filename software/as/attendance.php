<?php
require "dbconnect.php";
session_start();

//class for generating calendar
class Calendar
{
	var $events;
	

	function Calendar($date,$cid,$compname)
	{
		if(empty($date)) $date = time();
		//echo date('t');
		define('NUM_OF_DAYS', date('t',$date));
		define('CURRENT_DAY', date('j',$date));
		define('CURRENT_MONTH_A', date('F',$date));
		define('CURRENT_MONTH_N', date('n',$date));
		define('CURRENT_YEAR', date('Y',$date));
		define('START_DAY', date('w', mktime(0,0,0,CURRENT_MONTH_N,1, CURRENT_YEAR)));
		define('COLUMNS', date('t',$date));
		define('PREV_MONTH', $this->prev_month());
		define('NEXT_MONTH', $this->next_month());
		define('cid', $cid);
		define('cname', $compname);
		//define('CURRENT_MONTH', $this->current_month());
		$this->events = array();
	}

	function prev_month()
	{
		return mktime(0,0,0,
				(CURRENT_MONTH_N == 1 ? 12 : CURRENT_MONTH_N - 1),
				(checkdate((CURRENT_MONTH_N == 1 ? 12 : CURRENT_MONTH_N - 1), CURRENT_DAY, (CURRENT_MONTH_N == 1 ? CURRENT_YEAR - 1 : CURRENT_YEAR)) ? CURRENT_DAY : 1),
				(CURRENT_MONTH_N == 1 ? CURRENT_YEAR - 1 : CURRENT_YEAR));
	}
	
	function next_month()
	{
		return mktime(0,0,0,
				(CURRENT_MONTH_N == 12 ? 1 : CURRENT_MONTH_N + 1),
				(checkdate((CURRENT_MONTH_N == 12 ? 1 : CURRENT_MONTH_N + 1) , CURRENT_DAY ,(CURRENT_MONTH_N == 12 ? CURRENT_YEAR + 1 : CURRENT_YEAR)) ? CURRENT_DAY : 1),
				(CURRENT_MONTH_N == 12 ? CURRENT_YEAR + 1 : CURRENT_YEAR));
	}
	function makeCalendar()
	{
		?>
		<html>
		<head>
		
		<link rel='stylesheet' href='header.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='minbootstrap.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='button.css' type='text/css' media='print, projection, screen' />
		<script type='text/javascript' src='jquery-1.8.2.min.js'></script> 
		<style>
		.tableHeader{
			width:10px;
			}
		.fname{
			display:none;
		}
		td.center{
			text-align:center;
		}
		.heading{
			background: #8B8B8B;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#A9A9A9, endColorstr=#7A7A7A);
			background: -webkit-gradient(linear, left top, left bottom, from(#A9A9A9), to(#7A7A7A));
			background: -moz-linear-gradient(top,  #A9A9A9,  #7A7A7A);
		}
		</style>
		<script>
		$(document).ready(function(){
		  fnAdjustTable();
		});

		fnAdjustTable = function(){

		  var colCount = $('#firstTr>td').length; //get total number of column

		  var m = 0;
		  var n = 0;
		  var brow = 'mozilla';
		  
		  jQuery.each(jQuery.browser, function(i, val) {
			if(val == true){
			  brow = i.toString();
			}
		  });
		  
		  $('.tableHeader').each(function(i){
			if (m < colCount){

			  if (brow == 'mozilla'){
				$('#firstTd').css("width",$('.tableFirstCol').innerWidth());//for adjusting first td
				$(this).css('width',$('#table_div td:eq('+m+')').width());//In IE there is difference of 2 px
				$('.cell').css("height",$('.tableFirstCol').innerHeight());
				$('.cell').css("width",$('.tableHeader').innerWidth());
			  }
			  else if (brow == 'msie'){
				$('#firstTd').css("width",$('.tableFirstCol').width());
				$(this).css('width',"30px");//In IE there is difference of 2 px
				//$('.right').css("height","50px");
				$('.outercell').css("height","22px");
				$('.cell').css("width",$('.tableHeader').width());
			  }
			  else if (brow == 'safari'){
				$('#firstTd').css("width",$('.tableFirstCol').width());
				$(this).css('width',$('#table_div td:eq('+m+')').width());
				$('.outercell').css("height","22px");
			  }
			  else {
				$('#firstTd').css("width",$('.tableFirstCol').width());
				$(this).css('width',$('#table_div td:eq('+m+')').innerWidth());
				$('.outercell').css("height","22px");
			  }
			}
			m++;
		  });

		  $('.tableFirstCol').each(function(i){
			if(brow == 'mozilla'){
			  $(this).css('height',$('#table_div td:eq('+colCount*n+')').outerHeight());//for providing height using scrollable table column height
			}
			else if(brow == 'msie'){
			  $(this).css('height',$('#table_div td:eq('+colCount*n+')').innerHeight()-2);
			}
			else {
			  $(this).css('height',$('#table_div td:eq('+colCount*n+')').height());
			}
			n++;
		  });

		}

		//function to support scrolling of title and first column
		fnScroll = function(){
		  $('#divHeader').scrollLeft($('#table_div').scrollLeft());
		  $('#firstcol').scrollTop($('#table_div').scrollTop());
		  $('#secondcol').scrollTop($('#table_div').scrollTop());
		  $('#thirdcol').scrollTop($('#table_div').scrollTop());
		  $('#lastcol').scrollTop($('#table_div').scrollTop());
		}
		
		//function for toggling fname
		$(document).ready(function(){
		  $(".name").click(function(){
			$(".fname").toggle();
		  });
		});
		
		/*$(document).ready(function()
		{
			$("tr:odd").css("background-color", "#c0c0c0");
		});*/
		
		/*$(document).ready(function(){
			$(function(){
				$("tr:even").children("td.zeb").css("background-color", "#c0c0c0");
				$("tr:odd").children("td.wid").css("background-color", "#c0c0c0");
				$("tr:odd").children().children("input.cell").css("background-color", "#c0c0c0");
			});
		});*/
		
		//function for moving by arrow keys
		$(document).ready(function () {

			$("input.cell").keyup(function (e) {
				switch (e.keyCode) {
					// left arrow
					case 37:
						e.preventDefault();
						$(this).parent()
							.prev()
							.children("input.cell")
							.focus();
						break;

					// right arrow
					case 39:
						e.preventDefault();
						$(this).parent()
							.next()
							.children("input.cell")
							.focus();
						break;

					// up arrow
					case 40:
						e.preventDefault();
						$(this).parent()
							.parent()
							.next()
							.children("td")
							.children("input.cell[id="
								+ $(this).attr("id") + "]")
							.focus();
						break;

					// down arrow
					case 38:
						e.preventDefault();
						$(this).parent()
							.parent()
							.prev()
							.children("td")
							.children("input.cell[id="
								+ $(this).attr("id") + "]")
							.focus();
						break;
				}
			});
		});
		
		function fillall(id)
		{
			alert(val);
			var val=$(id).val();
			alert(val);
			
			var id1=id.toString();
			var val=document.getElementById("all"+id1).value;
			for (var i=1;i<=n;i++)
			{ 
				document.getElementById(id1+"-"+i).value=val;
			}
		}
		function checkinput(id,no)
		{
			var id1=id+'v'+no;
			var yes=0;
			var arr = ["P","L", "A", "R", "X", "S", "F", "H", "T", "C","p", "l", "a", "r", "x", "s", "f", "h", "t", "c",""];
			var val=$("."+id1).val();
			for(var i=0; i<arr.length; i++) {
				if (arr[i] == val){
					yes=1;
					break;
				}
			}
			if(yes==0){
				alert("Invalid Input");
				$("."+id1).val("");
			}
		}
		function checkrest(id,rest)
		{
			var yes=0;
			var arr=["1", "2", "3", "4", "7", "5", "6"];
			var val=document.getElementById(id).value;
			for(var i=0; i<arr.length; i++) {
				if (arr[i] == val){
					yes=1;
					break;
				}
			}
			if(yes==0){
				alert("Invalid Input Enter value 1 to 7");
				document.getElementById(id).value=rest;
			}
		}
		function checkfile()
		{
			if(document.getElementById("ufile").value==""){
				alert("No file choosen");
				return false;
			}
		}
		</script>
		</head>

		<body>
		<?//----------------------------------header code begin---------------------------------------------------?>
			<?$compname=cname;
			require 'header.php';?>
		<?//-------------------------header code end   ---------------------------------------------------
		
		//$editmonth=date('n',time());
		//$edityear=date('Y',time());
		$leftmonth=0;
		$leftmonth=99999999;
		if((CURRENT_MONTH_N>=date('n',time()) && date('Y',time())==CURRENT_YEAR)||(date('Y',time())<CURRENT_YEAR))
			$edit=1;
		else
			$edit=0;

		//heading prev-month current-month and next-month
		?>
		<div style="background-color:#E7E5E5;width=80%">
			<table width="100%"border="1">
				<tr>
					<td width="1%"><a href="?date=<?echo PREV_MONTH;?>&current=attendance">PREV MONTH</a></td>
					<td width="10%" style="text-align:center"><b><?echo CURRENT_MONTH_A .' - '. CURRENT_YEAR;?></b></td>
					<td width="1%"><a href="?date=<?echo NEXT_MONTH;?>&current=attendance">NEXT MONTH</a></td>
				</tr>
			</table>
			<table width="100%"border="0">
				<tr>
					<td><b>P-Present</b></td>
					<td><b>A-Absent</b></td>
					<td><b>L-Leave</b></td>
					<td><b>F-Festival</b></td>
					<td><b>C-Suspend</b></td>
					<td><b>R-Rest/Weekday</b></td>
					<td><b>H-HalfDay</b></td>
					<td><b>S-Sick</b></td>
					<td><b>X-Left</b></td>
					<td><b>T-Ontour</b></td>
				</tr>
			</table>
		</div>
		<?
		
		//attendance table----
		//fetching names of employees
		//checking monthlydetail first for names of employees
		//if not found then fetch from employeedetail
		$query="SELECT * FROM ".cid."employeedetail WHERE WORKERID IN (SELECT WORKERID FROM ".cid."monthlydetail WHERE ((JOINYEAR <".CURRENT_YEAR.") or (JOINYEAR=".CURRENT_YEAR." and JOINMONTH <= ".CURRENT_MONTH_N.")) and  ((LEFTYEAR >".CURRENT_YEAR.") or (LEFTYEAR=".CURRENT_YEAR." and LEFTMONTH >= ".CURRENT_MONTH_N.")) and MONTH=".CURRENT_MONTH_N." and YEAR=".CURRENT_YEAR.")";
		$result= mysql_query($query);
		$noempinmd=mysql_num_rows($result);//no of employee in monthlydetail
		if (!$result) {
			die('Could not query:' . mysql_error());
		}
		if($noempinmd>0){
			$noemp=$noempinmd;
		}
		else{
			$query="SELECT * FROM ".cid."employeedetail WHERE ((JOINYEAR <".CURRENT_YEAR.") or (JOINYEAR=".CURRENT_YEAR." and JOINMONTH <= ".CURRENT_MONTH_N.")) and  ((LEFTYEAR >".CURRENT_YEAR.") or (LEFTYEAR=".CURRENT_YEAR." and LEFTMONTH >= ".CURRENT_MONTH_N."))";
			$result = mysql_query($query);
			$noemp=mysql_num_rows($result);//no of employee
			if (!$result) {
				die('Could not query:' . mysql_error());
			}
		}
		
		////storing all employee names and details in a 2Darray $namearray
		if(mysql_num_rows($result)>0){
		while($row = mysql_fetch_assoc($result)){
			$namearray[] = $row;
		}
					
		$noemp=count($namearray);//no of employees
		
		
		//festivals calculation
		//$festresult = mysql_query("SELECT DATE,FESTNAME FROM festivals WHERE MONTH=".CURRENT_MONTH_N." and YEAR=".CURRENT_YEAR." UNION SELECT DATE, FESTNAME FROM companyfestival WHERE MONTH =".CURRENT_MONTH_N." AND YEAR =".CURRENT_YEAR." AND COMPID =".cid." AND FLAG =1");//fetching all festivals for current month
		$festresult=mysql_query("SELECT DATE, FESTNAME FROM companyfestival WHERE MONTH =".CURRENT_MONTH_N." AND YEAR =".CURRENT_YEAR." AND COMPID =".cid." AND FLAG =1");//fetching all festivals for current month which company do not want to give
		if (!$festresult) {
			die('Could not query fest query:' . mysql_error());
		}
		$nofest= mysql_num_rows($festresult);//no of festtivals
		for($i=1;$i<=31;$i++){//initializing festarray for full month
			$festarray[$i]=0;
		}
		if ($nofest>0){
			while ($row = mysql_fetch_array($festresult)){
				$date=$row['DATE'];
				$festarray[$date]=1;
				$festname[$date]="'".$row['FESTNAME']."'";
			}
		}
		?>
		<form action="attendancesubmit.php" method="post">
			<div style="background-color:#E7E5E5">
				<table cellspacing="0" cellpadding="0" border="1" >
					<tr>
						<td class="center heading" width="100px"  id="firstTd">
							ID
						</td>
						<td class="center heading" width="200px"  id="firstTd">
							NAME
						</td>
						<td class="center fname heading" width="200px"  id="firstTd">
							FNAME
						</td>
						<td>
							<div id="divHeader" style="overflow:hidden;width:852px;">
								<table cellspacing="0" cellpadding="0" border="1" >
									<tr>
										<td class="heading"><div class="tableHeader">R</div></td>
										<?
										for($i = 1; $i <= NUM_OF_DAYS; $i++)
										{
											if($i == CURRENT_DAY){
												echo '<td class="heading"><div class="tableHeader">'.$i;
												$day=mktime(0 ,0 ,0, CURRENT_MONTH_N, $i, CURRENT_YEAR);
												echo "<br>".date("D",$day).'</div></td>';}
											else{
												echo '<td><div class="tableHeader">'.$i;
												$day=mktime(0 ,0 ,0, CURRENT_MONTH_N, $i, CURRENT_YEAR);
												echo "<br>".date("D",$day).'</div></td>';}
										}
										?>
									</tr>
								</table>
							</div>
						</td>
						<td>
							<div style="width:150px;">
								<table cellspacing="0" cellpadding="0" border="1" >
									<tr>
										<td class="center heading"  height="40px" width="30px" id="firstTd">P</td>
										<td class="center heading"   height="40px" width="30px" id="firstTd">L</td>
										<td class="center heading"  height="40px" width="30px" id="firstTd">F</td>
										<td class="center heading"  height="40px" width="30px"  id="firstTd">R</td>
										<td class="center heading"  height="40px" width="30px"  id="firstTd">EL</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<div id="firstcol" style="overflow:hidden;height:401px">
								<table width="100px" cellspacing="0" cellpadding="0" border="1" >
									<?
									for($i=0;$i<$noemp;$i++){
										echo '<tr>';
											echo '<td class="tableFirstCol" width="200px" height="22px">'.$namearray[$i]['WORKERID'].'</td>';
										echo '</tr>';
									}
									?>
								</table>
							</div>
						</td>
						<td class="name" valign="top">
							<div id="secondcol" style="overflow: hidden;height:401px">
								<table width="200px" cellspacing="0" cellpadding="0" border="1" >
									<?
									for($i=0;$i<$noemp;$i++){
										echo '<tr>';
											echo '<td class="tableFirstCol" width="200px" height="22px">'.$namearray[$i]['NAME'].'<input type="hidden" name='.$namearray[$i]['WORKERID'].'-'.cid.'-JM value='.$namearray[$i]['JOINMONTH'].'>';
											echo '<input type="hidden" name='.$namearray[$i]['WORKERID'].'-'.cid.'-JY'.' value='.$namearray[$i]['JOINYEAR'].'>
												<input type="hidden" name='.$namearray[$i]['WORKERID'].'-'.cid.'-JD'.' value='.$namearray[$i]['JOINDATE'].'></td>';
										echo '</tr>';
									}
									?>
								</table>
							</div>
						</td>
						
						<td class="fname" valign="top">
							<div id="thirdcol" style="overflow: hidden;height:401px">
								<table width="200px" cellspacing="0" cellpadding="0" border="1" >
									<?
									for($i=0;$i<$noemp;$i++){
										echo '<tr>';
											echo '<td class="tableFirstCol" width="200px" height="22px" >'.$namearray[$i]['FNAME'].'</td>';
										echo '</tr>';
									}
									?>
								</table>
							</div>
						</td>
					
						<td valign="top">
							<div id="table_div" style="overflow: scroll;width:870px;height:418px;position:relative" onscroll="fnScroll()" >
								<table  width="895px" cellspacing="0" cellpadding="0" border="1" >
									<?$no=1;
									for($n = 0; $n <$noemp ; $n++){
										$id=$namearray[$n]['WORKERID'];
										$cid=cid;
										echo '<tr id="firstTr">';
										echo '<td  class="outercell" width="25px"><input  class="cell" style="width:100%;height:100%; border:0px"  type="text" id='.$id.' onchange="checkrest('.$id.','.$namearray[$n]['REST'].')" name='.$id.'-'.$cid.' value='.$namearray[$n]['REST'].'></td>';
										for($i = 1; $i <= NUM_OF_DAYS; $i++){
											//class for checking valid inputs
											$checkclass=$id."v".$i;
											$query="select * FROM ".cid."attendance where DATE=".$i." and MONTH=".CURRENT_MONTH_N." and YEAR=".CURRENT_YEAR." and WORKERID=".$id;
											//$festquery="select * FROM ".cid."attendance where DATE=".$i." and MONTH=".CURRENT_MONTH_N." and YEAR=".CURRENT_YEAR." and WORKERID=".$id;
											$result1 = mysql_query($query);
											$fetchatt=mysql_fetch_array($result1);
											$day=mktime(0 ,0 ,0, CURRENT_MONTH_N, $i, CURRENT_YEAR);
											$day=date("N",$day);
											if($fetchatt!=null){//if in db any entry is there for this date then take that
												if($edit==1){
													if($fetchatt['ATTENDANCE']=='R'){
														echo '<td  class="outercell" width="25px"><input  class="cell '.$checkclass.'" style="background-color:red;width:100%;height:100%; border:0px" onchange="checkinput('.$id.','.$i.')" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value='.$fetchatt['ATTENDANCE'].'></td>';
													}
													else
													echo '<td class="outercell" width="25px"><input class="cell '.$checkclass.'" style="width:100%;height:100%; border:0px" onchange="checkinput('.$id.','.$i.')" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value='.$fetchatt['ATTENDANCE'].'></td>';
												}
												else
													echo '<td class="outercell" width="25px"><input class="cell" style="width:100%;height:100%; border:0px" disabled="disabled" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value='.$fetchatt['ATTENDANCE'].'></td>';
											}
											elseif($day==$namearray[$n]['REST']){//else if thier is rest for this date then mark rest
												if($edit==1)
													echo '<td  class="outercell" width="25px"><input class="cell '.$checkclass.'" style="background-color:red;width: 100%;height:100%; border:0px" onchange="checkinput('.$id.','.$i.')" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value="R"></td>';
												else
													echo '<td  class="outercell" width="25px"><input  class="cell " style="background-color:red;width: 100%;height:100%; border:0px" type="text" disabled="disabled" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value="R"></td>';
											}
											elseif($nofest>0){//if thier is fest in this month then on which date and show F in that input
												if($festarray[$i]==1){
													if($edit==1)
														echo '<td class="outercell" width="25px"><input class="cell '.$checkclass.'" style="width:100%;height:100%; border:0px" title='.$festname[$i].' onchange="checkinput('.$id.','.$i.')" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value="F"></td>';
													else
														echo '<td class="outercell" width="25px"><input class="cell" style="width:100%;height:100%; border:0px" type="text" disabled="disabled" id='.$i.' name='.$id.'-'.$cid.'-'.$i.' value="F"></td>';
												}
												else{
													if($edit==1)
													echo '<td class="outercell" width="25px"><input class="cell '.$checkclass.'" style="width:100%;height:100%; border:0px" onchange="checkinput('.$id.','.$i.')" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.'></td>';
												else
													echo '<td class="outercell" width="25px"><input class="cell" style="width:100%;height:100%; border:0px" disabled="disabled" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.'></td>';
												}
											}
											else{//else don nothing simply show a input
												if($edit==1)
													echo '<td  class="outercell" width="25px"><input class="cell '.$checkclass.'" style="width:100%;height:100%; border:0px" onchange="checkinput('.$id.','.$i.')" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.'></td>';
												else
													echo '<td  class="outercell" width="25px"><input class="cell" style="width:100%;height:100%; border:0px" disabled="disabled" type="text" id='.$i.' name='.$id.'-'.$cid.'-'.$i.'></td>';
											}
										}
										echo '</tr>';
										$no++;
									}?>
								</table>
							</div>
						</td>
						<td valign="top">
							<div id="lastcol" style="overflow: hidden;height:401px">
								<table width="150px" cellspacing="0" cellpadding="0" border="1" >
									<?
									//fetching total P,F,El,L and R and showing in right side
									for($i=0;$i<$noemp;$i++){
										$el=($namearray[$i]['TOT_EL'])-($namearray[$i]['USED_EL']);
										$queryforsidecal="select * from ".cid."monthlydetail where MONTH=".CURRENT_MONTH_N." and YEAR=".CURRENT_YEAR." and WORKERID=".$namearray[$i]['WORKERID'];
										$resultforsidecal = mysql_query($queryforsidecal);
										$fetchsidecal=mysql_num_rows($resultforsidecal);
										//fetching NO_P,NO_R,NO_L,NO_F from monthlydetail and EL from the 2Darray
										if($fetchsidecal>0){
												$row3=mysql_fetch_array($resultforsidecal);
												echo '<tr>';
													echo '<td width="30px"  class="tableFirstCol">'.$row3['NO_P'].'</td>
														<td width="30px" class="tableFirstCol">'.$row3['NO_L'].'</td>
														<td width="30px" class="tableFirstCol">'.$row3['NO_F'].'</td>
														<td width="30px" class="tableFirstCol">'.$row3['NO_R'].'</td>
														<td width="30px" class="tableFirstCol">'.$el.'</td>';
												echo '</tr>';
											}
				
										else{
												echo '<tr>';
													echo '<td width="30px" class="tableFirstCol outercell">0</td>
													<td width="30px" class="tableFirstCol outercell">0</td>
													<td width="30px"  class="tableFirstCol outercell">0</td>
													<td width="30px" class="tableFirstCol outercell">0</td>
													<td width="30px" class="tableFirstCol">'.$el.'</td>';
												echo '</tr>';
											}
									}
									?>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<input type="hidden" name='month' value= <?echo CURRENT_MONTH_N?>>
			<input type="hidden" name='year' value= <?echo CURRENT_YEAR?>>
			<input type="hidden" name='nodays' value= <?echo NUM_OF_DAYS?>>
			<input type="hidden" name='compid' value= <?echo cid?>>
			<?
			if($edit==1)
				echo '<div align="center"><input class="btn btn-primary" value="Submit" type="submit"><a href=printatt.php?month='.CURRENT_MONTH_N.'&year='.CURRENT_YEAR.'&noday='.NUM_OF_DAYS.'><button class="btn btn-primary" type="button">Print Attendance</button></a></div>';
			else
				echo '<div align="center"><input class="btn btn-primary" value="Submit" type="submit" disabled="disabled"><a href=printatt.php?month='.CURRENT_MONTH_N.'&year='.CURRENT_YEAR.'&noday='.NUM_OF_DAYS.'><button class="btn btn-primary" type="button">Print Attendance</button></a></div>';
			}
			?>
		</form>
		<div align="center">
		<form action="importattendance.php" method="post" enctype="multipart/form-data" onsubmit="return checkfile()">
			<input name="ufile" type="file" id="ufile" size="50">
			<select style="height:25px;width:80px" name="month">
				<option value="1">JAN</option>
				<option value="2">FEB</option>
				<option value="3">MARCH</option>
				<option value="4">APRIL</option>
				<option value="5">MAY</option>
				<option value="6">JUNE</option>
				<option value="7">JULY</option>
				<option value="8">AUG</option>
				<option value="9">SEPT</option>
				<option value="10">OCT</option>
				<option value="11">NOV</option>
				<option value="12">DEC</option>
			</select>
			<?if(isset($_SESSION['attupload'])){
				if($_SESSION['attupload']!="not"){
					echo "<h4>".$_SESSION['attupload']."</h4>";
					$_SESSION['attupload']="not";
				}
			}?>
			<button type="submit" class="btn btn-primary">Import</button>
		</form>
		<form action="exportatt.php" method="post" >
			<input type="hidden" name='month' value= <?echo CURRENT_MONTH_N?>>
			<input type="hidden" name='year' value= <?echo CURRENT_YEAR?>>
			<input type="hidden" name='compid' value= <?echo cid?>>
			<input type="hidden" name='nodays' value= <?echo NUM_OF_DAYS?>>
		<button type="submit" class="btn btn-primary">Export</button>
		</form>
		</div>
		
		  
		</body>
		</html>
	<?
	}
}
if(isset($_SESSION['compid'])){
	$compid=$_SESSION['compid'];
	$compname=$_SESSION['compname'];
	$cal = new Calendar($_GET['date'],$compid,$compname);
	$cal->makeCalendar();
}
else
echo "INVALID ACCESS";
?>