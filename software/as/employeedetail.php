<?
require "dbconnect.php";
session_start();
if(isset($_SESSION['compid'])){
	$compid=$_SESSION['compid'];
	$compname=$_SESSION['compname'];
	$compdetail=mysql_query("select * from companymaster where COMPID=".$compid);
	$resultdept=mysql_query("select * from compdepartment where COMPID=".$compid." and FLAG=1");
	$resultbranch=mysql_query("select * from compbranch where COMPID=".$compid." and FLAG=1");
	$rowcompdetail=mysql_fetch_array($compdetail);
	$pfrate=$rowcompdetail['PFRATE'];
	$esirate=$rowcompdetail['ESIRATE'];
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>employeedetail</title>
        <link rel='stylesheet' href='bootstrap.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='header.css' type='text/css' media='print, projection, screen' />
        <script type='text/javascript' src='jquery-1.8.2.min.js'></script> 
        <script type='text/javascript' src='bootstrap.min.js'></script>
        <style>
            body{margin:0px; padding:0px; color:black; font-family: 'Helvetica Neue';}
			.bold{
			font-weight:bold
			}
			div.block{
				margin:10px;
				padding:10px;
			}
        </style>
		<script>
		function showdiv(id1,id2)
		{
			if(document.getElementById(id1).checked == true){
				document.getElementById(id2).style.display='block';
				document.getElementById(id1+"hidden").value="Y";
			}
			else if(document.getElementById(id1).checked == false){
				document.getElementById(id2).style.display='none';
				document.getElementById(id1+"hidden").value="N";
			}
		}
		function check(){
                var compname = document.getElementById("compname").value;
                if(compname==""){
                    alert("Enter Compname");
                    return false; 
                }
        }			
		</script>
	</head>
	<body>
	<?require 'header.php';?>
	<div  style="width:94%;font-weight:bold;padding-top:0px;padding-left:10px;padding-right:10px" class="container  well">
		<h3 align="center" >Employee Details</h3><hr>
		<form action="insertemployeedetail.php" method="post"  class="form-horizontal" onsubmit="return check()">
			<div style="width:95%;"class="container well">
				<div  style="float:left;width:32%" class="block">
					<div class="control-group">
						<label class="control-label bold">ID :</label>
						<div class="controls">
						  <input style="height:25px" disabled="disabled" type="text" name="eid" id="eid" placeholder="ID">
						  <input style="height:25px"  type="hidden" name="compid" value="<?echo $compid?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">NAME :</label>
						<div class="controls">
						  <input style="height:25px" type="text" name="ename" id="ename" placeholder="NAME">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">FATHER NAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="efathername" id="efathername" placeholder="NAME">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">HUSBAND NAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="ehusname" id="ehusname" placeholder="NAME">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DEPT :</label>
						<div class="controls">
							<select style="height:25px;" name="edept">
								<option value='NO'>NO</option>
								<?while($rowdept=mysql_fetch_array($resultdept)){
										echo '<option value='.$rowdept['DEPT'].'>'.$rowdept['DEPT'].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BRANCH :</label>
						<div class="controls">
							<select style="height:25px;" name="ebranch">
								<option value='NO'>NO</option>
								<?while($rowbranch=mysql_fetch_array($resultbranch)){
										echo '<option value='.$rowbranch['BRANCH'].'>'.$rowbranch['BRANCH'].'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DESIGNATION  :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="edesig" id="edesig" placeholder="DESIGNATION">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DOB  :</label>
						<div class="controls">
						  <input  style="height:25px" type="date" name="edob" id="edob" value="<?echo date('Y-m-d',strtotime(date('Y/m/d')));?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DOJOINING  :</label>
						<div class="controls">
						 <input  style="height:25px" type="date" name="edoj" id="edoj" value="<?echo date('Y-m-d',strtotime(date('Y/m/d')));?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">SEX  :</label>
						<div class="controls">
							<select style="height:25px;width:90px" name="esex">
								<option value="M">MALE</option>
								<option value="F">FEMALE</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ADDRESS :</label>
						<div class="controls">
						  <textarea id="eaddress" name="eaddress" rows="3" cols="50"></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PIN :</label>
						<div class="controls">
						  <input style="height:25px"type="text" name="epin" placeholder="PIN">
						</div>
					</div>
				<?/*<div class="control-group">
						<label class="control-label bold">PF  :</label>
						<div class="controls">
						  <input  style="height:25px" type="checkbox"  id="epf" onclick="showdiv('epf','pf')">
						  <input  style="height:25px" type="hidden" name="epf" id="epfhidden" value="N">
						</div>
					</div>
					<div id="pf" style="display:none" class="well">
						<div align="center">PF DETAILS</div><hr>
						<div class="control-group">
							<label class="control-label bold">PF-NO :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" name="epfno" id="epfno">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">PF-RATE :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" name="epfvol" id="epfvol">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">PF-ELIG-DATE  :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" value='<?echo date('Y')."-".date('n')."-".date('d')?>' type="date" name="epfeligdate" id="epfeligdate">
							</div>
						</div>
					</div>*/?>
				</div>
				<div style="float:left;width:32%" class="block">
					<div class="control-group">
						<label class="control-label bold">PAYMENT :</label>
						<div class="controls">
							<select style="height:25px;width:120px" name="epayment">
								<option value="M">MONTHLY</option>
								<option value="D">DAILY</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">REST :</label>
						<div class="controls">
							<select style="height:25px;width:80px" name="erest">
								<option value="7">SUN</option>
								<option value="1">MON</option>
								<option value="2">TUE</option>
								<option value="3">WED</option>
								<option value="4">THU</option>
								<option value="5">FRI</option>
								<option value="6">SAT</option>
								
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">INSURANCE NO :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="einno" placeholder="NO">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">RELIGION :</label>
						<div class="controls">
						  <select style="height:25px;width:150px" name="ereligion">
								<option value="hindu">HINDU</option>
								<option value="muslim">MUSLIM</option>
								<option value="christian">CHRISTIAN</option>
								<option value="sikh">SIKH</option>
							</select>
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label bold">PHONE :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="ephone" placeholder="PHONE">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">EMAIL :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="eemail" placeholder="EMAIL">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK NAME :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="ebankname" placeholder="NAME">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK ACCNO :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="ebankaccno" placeholder="ACC NO">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PAN NO :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="epanno" placeholder="PAN NO">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">EXP IN YEARS :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="eexpyrs" placeholder="YEARS">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">QUALIFICATION :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="equali" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OPENING EL :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="el" >
						</div>
					</div>
				<?/*<div class="control-group">
						<label class="control-label bold">ESI  :</label>
						<div class="controls">
						  <input  style="height:25px" type="checkbox" id="eesi" onclick="showdiv('eesi','esi')">
						  <input  style="height:25px" type="hidden" name="eesi" id="compesihidden" value="N">
						</div>
					</div>
					<div id="esi" style="display:none" class="well">
						<div align="center">ESI DETAILS</div><hr>
						<div class="control-group">
							<label class="control-label bold">ESI-NO :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" name="eesino" id="eesino">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">ESI-RATE :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" name="eesivol" id="eesivol">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">ESI-ELIG-DATE  :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" value='<?echo date('Y')."-".date('n')."-".date('d')?>' type="date" name="eesieligdate" id="eesieligdate">
							</div>
						</div>
					</div>*/?>
				</div>
				<div style="float:left;width:26%" class="block">
					<div align="center">ALLOWANCES</div><hr>
					<div align="left"class="control-group">
						<label class="control-label bold">BASIC-RATE :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="ebr" id="ebr" placeholder="BR">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label bold">DA :</label>
						<div class="controls">
						  <input style="height:25px;width:120px"type="text" name="eda" id="eda" placeholder="DA">
						</div>
					</div>
					<div align="left"class="control-group">
						<label class="control-label bold">BASIC LA :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="ela" placeholder="LA">
						</div>
					</div>
					<div align="left"class="control-group">
						<label class="control-label bold">BASIC WA :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="ewa" id="ewa" placeholder="WA">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BASIC HR :</label>
						<div class="controls">
						  <input style="height:25px;width:120px"type="text" name="ehra" placeholder="HRA">
						</div>
					</div>
					<div align="left"class="control-group">
						<label class="control-label bold">SP :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="esp" id="esp" placeholder="SP">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">CV :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="ecv" placeholder="CV">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OTHER1 :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="eother1" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OTHER2 :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="eother2" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OTHER3 :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="eother3" >
						</div>
					</div>
				</div>
			</div>
		  <div align="center"><button class="btn btn-primary">Submit</button></div>
		</form>
		<?//for file uploading of employee detail?>
		<div style="float:right;width:32%" class="block">
			<form action="importempdetail.php" method="post" enctype="multipart/form-data" >
				<input name="ufile" type="file" id="ufile" size="50">
				<?if(isset($_SESSION['empupload'])){
					if($_SESSION['empupload']!="not"){
						echo "<h4>".$_SESSION['empupload']."</h4>";
						$_SESSION['empupload']="not";
					}
				}?>
				<button type="submit" class="btn btn-primary">Import</button>
			</form>
		</div>
	</body>
</html>

<?
}
else
echo "INVALID ACCESS";
?>
	
		
