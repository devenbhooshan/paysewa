$("*") selects all elements.

$("p") selects all <p> elements.

$("p.intro") selects all <p> elements with class="intro".

$("p#intro") selects the first <p> elements with id="intro".

$(":animated") selects all elements that are currently animated.

$(":button") selects all <button> elements and <input> elements of type="button".

$(":even") selects even elements.

$(":odd") selects odd elements.


$(document).ready(function)  	Binds a function to the ready event of a document
(when the document is finished loading)
$(selector).click(function)	Triggers, or binds a function to the click event of selected elements
$(selector).dblclick(function)	Triggers, or binds a function to the double click event of selected elements
$(selector).focus(function)	Triggers, or binds a function to the focus event of selected elements
$(selector).mouseover(function)	Triggers, or binds a function to the mouseover event of selected elements

Function	Description
$(selector).hide()	Hide selected elements
$(selector).show()	Show selected elements
$(selector).toggle()	Toggle (between hide and show) selected elements
$(selector).slideDown()	Slide-down (show) selected elements
$(selector).slideUp()	Slide-up (hide) selected elements
$(selector).slideToggle()	Toggle slide-up and slide-down of selected elements
$(selector).fadeIn()	Fade in selected elements
$(selector).fadeOut()	Fade out selected elements
$(selector).fadeTo()	Fade out selected elements to a given opacity
$(selector).animate()	Run a custom animation on selected elements

$(selector).html(content)	Changes the (inner) HTML of selected elements
$(selector).append(content)	Appends content to the (inner) HTML of selected elements
$(selector).after(content)	Adds HTML after selected elements

CSS Properties	Description
$(selector).css(name)	Get the style property value of the first matched element
$(selector).css(name,value)	Set the value of one style property for matched elements
$(selector).css({properties})	Set multiple style properties for matched elements
$(selector).height(value)	Set the height of matched elements
$(selector).width(value)	Set the width of matched elements






APIPS4623J
12/8/1967


6220180110900307387
april 2010

2.60
2.65
7.26
7.49
2.56
6.80
2.51
2.32
4.08
2.48
3.18
3.35
1.79
3.14
2.64
4.19
1.33
1
3.82
4.82

1373619
1373622
