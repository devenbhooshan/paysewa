.data

input_request: .asciiz "Input the first operand: "
output1: .asciiz "The result is "
newline: .asciiz "\n"

.align 2

.globl main

.text

main: 
	# request input from user
	la $a0, input_request
	li $v0, 4
	syscall
	
	# retrieve input from user
	li $v0, 5
	syscall
	
	# store user input as first operand
	move $a0, $v0
	
	# calculate factorial
	jal fact
	
	# store result
	move $t0, $v0
	
	# display result
	la $a0, output1
	li $v0, 4
	syscall
	
	move $a0, $t0
	li $v0, 1
	syscall
	
	la $a0, newline
	li $v0, 4
	syscall
	
	# exit program
	li $v0, 10
	syscall	
	
fact:
        # make room on stack for $ra and $a0
	move $fp, $sp
	addi $sp, $sp, -8
	
        # store both those registers on the stack
	sw $a0, 0($fp)
	sw $ra, -4($fp)
	
        # recursive condition
	bgtz $a0, fact_L1
	li $v0, 1
	j fact_return
	
        # recursive calls
	fact_L1:
		addi $a0, $a0, -1
		jal fact
		addi $fp, $sp, 8
		lw $a0, 0($fp)
		mult $a0, $v0
		mflo $v0
		j fact_return	
		
        # return sequence
	fact_return:
                # restore $ra and $a0 registers
		lw $ra, -4($fp)
		lw $a0, 0($fp)

		move $sp, $fp		
		
		jr $ra	

