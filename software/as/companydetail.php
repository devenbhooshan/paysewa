<?
require "dbconnect.php";
session_start();
if(isset($_SESSION['compid'])){
	$compid=$_SESSION['compid'];
	$compname=$_SESSION['compname'];
	$result=mysql_query("select * from companymaster where COMPID=".$compid);
	$resultnature=mysql_query("select * from nature");
	$resultarea=mysql_query("select * from area");
	$resultbranch=mysql_query("select * from compbranch where FLAG=1 and COMPID=".$compid);
	$resultbranchoff=mysql_query("select * from compbranch where FLAG=0 and COMPID=".$compid);
	$resultdept=mysql_query("select * from compdepartment where FLAG=1 and COMPID=".$compid);
	$resultdeptoff=mysql_query("select * from compdepartment where FLAG=0 and COMPID=".$compid);
	$resultallo=mysql_query("select * from compallowances where FLAG=1 and COMPID=".$compid);
	$resultallooff=mysql_query("select * from compallowances where FLAG=0 and COMPID=".$compid);
	$row=mysql_fetch_array($result);	
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>companydetail</title>
        <link rel='stylesheet' href='bootstrap.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='header.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='lightbox.css' type='text/css' media='print, projection, screen' />
        <script type='text/javascript' src='jquery-1.8.2.min.js'></script> 
        <script type='text/javascript' src='bootstrap.min.js'></script>
        <style>
            body{margin:0px; padding:0px; color:black; font-family: 'Helvetica Neue';}
			.bold{
			font-weight:bold
			}
			div.block{
				margin:10px;
				padding:10px;
			}
			th.center{
				background-color:#c0c0c0;
			}
        </style>
		<script>
		function showdiv(id1,id2)
		{
			if(document.getElementById(id1).checked == true){
				document.getElementById(id2).style.display='block';
				document.getElementById(id1+"hidden").value="Y";
			}
			else if(document.getElementById(id1).checked == false){
				document.getElementById(id2).style.display='none';
				document.getElementById(id1+"hidden").value="N";
			}
		}
		function check(){
                var compname = document.getElementById("compname").value;
                if(compname==""){
                    alert("Enter Compname");
                    return false; 
                }
        }
		function checkdept(){
                var deptname = document.getElementById("indept").value;
                if(deptname==""){
                    alert("Enter Deptname");
                    return false; 
                }
        }
		function checkbranch(){
                var deptname = document.getElementById("inbranch").value;
                if(deptname==""){
                    alert("Enter Branchname");
                    return false; 
                }
        }
		function checkdeptinput(){
			if(document.getElementById("compdeptyes").checked == true && document.getElementById("compdeptno").checked == true){
				alert("Only 1 input is valid yes or no");
				return false; 
			}
			if(document.getElementById("compdeptyes").checked != true && document.getElementById("compdeptno").checked != true){
				alert("choose 1 input yes or no");
				return false; 
			}
		}
			
		function showlightbox()
		{
			if(document.getElementById("compdeptyes").checked == true){
				document.getElementById('deptblock').style.display='block';
				document.getElementById('fade').style.display='block';
			}
		}
		function showlightboxbranch()
		{
			if(document.getElementById("branch").checked==true){
				document.getElementById('branchblock').style.display='block';
				document.getElementById('fade').style.display='block';
			}
		}
		function hidelightboxbranch()
		{
			document.getElementById('branchblock').style.display='none';
			document.getElementById('fade').style.display='none';
		}
		function showlightboxallo()
		{
			if(document.getElementById("compallo").checked == true){
				document.getElementById('alloblock').style.display='block';
				document.getElementById('fade').style.display='block';
			}
		}
		function hidelightboxallo()
		{
			document.getElementById('alloblock').style.display='none';
			document.getElementById('fade').style.display='none';
		}
		function showpfrate()
		{
			document.getElementById('pfrate').style.display='block';
		}
		function hidepfrate()
		{
			document.getElementById('pfrate').style.display='none';
		}
		function showesirate()
		{
			document.getElementById('esirate').style.display='block';
		}
		function hideesirate()
		{
			document.getElementById('esirate').style.display='none';
		}
		function hidelightbox()
		{
			document.getElementById('deptblock').style.display='none';
			document.getElementById('fade').style.display='none';
		}
			
		</script>
	</head>
	<body>
	<?require 'header.php';
	//code for adddepartment
	$_SESSION['addcompany']="no";
	if(isset($_SESSION['adddept'])){
		if($_SESSION['adddept']=="yes"){
			echo '<div style="display:block"id="fade" class="black_overlay1"></div>';}
		else
			{echo '<div style="display:none"id="fade" class="black_overlay1"></div>';}
	}
	else
		{echo '<div style="display:none"id="fade" class="black_overlay1"></div>';}
	if(isset($_SESSION['adddept'])){
		if($_SESSION['adddept']=="yes"){
			echo '<div style="display:block" align="center" id="deptblock" class="white_content2">';
			$_SESSION['adddept']="no";}
		else
			{echo '<div style="display:none" align="center" id="deptblock" class="white_content2">';}
	}
	else
		{echo '<div style="display:none" align="center" id="deptblock" class="white_content2">';}
	if(isset($_SESSION['adddepterror'])){
		if($_SESSION['adddepterror']=="yes"){
			echo '<h3>Department Already Exists</h3>';
			$_SESSION['adddepterror']="no";}
	}?>
		<form action="submitdepartment.php" method="post" onsubmit="hidelightbox()">
			<table style="background-color:#E7E5E5;width:100%" class="table table-condensed table-hover table-bordered">
				<tr >
					<th class="center">DEPARTMENT</th>
					<th class="center">STATUS</th>
				  </tr>
			<?
			while($rowdept=mysql_fetch_array($resultdeptoff)){
				echo '<tr><td class="center" width="15%">'.$rowdept['DEPT'].'</td>
					<td class="center" width="10%"><input  style="height:25px" type="checkbox" name='.$rowdept['DEPT'].' id='.$rowdept['DEPT'].'></td></tr>';
			}
			while($rowdept=mysql_fetch_array($resultdept)){
				echo '<tr><td class="center" width="15%">'.$rowdept['DEPT'].'</td>
					<td class="center" width="10%"><input  style="height:25px" type="checkbox"  checked="TRUE" name='.$rowdept['DEPT'].' id='.$rowdept['DEPT'].'></td></tr>';
			}?>
			</table>
			<div align="center"><button class=" btn btn-info" type="submit">Submit</button></div>
		</form>
		<form class="form-inline" action="adddepartment.php" method="post" onsubmit="return checkdept()">
		  <input style="height:25px" type="text" name="indept" id="indept"placeholder="DEPT">
		  <button style="height:25px" type="submit" class="btn btn-info">Add</button>
		</form>
	</div>
	<?
	//code for addallowances
	if(isset($_SESSION['addallo'])){
		if($_SESSION['addallo']=="yes"){
			echo '<div style="display:block" id="fade" class="black_overlay1"></div>';}
		else
			{echo '<div style="display:none" id="fade" class="black_overlay1"></div>';}
	}
	else
		{echo '<div style="display:none"id="fade" class="black_overlay1"></div>';}
	if(isset($_SESSION['addallo'])){
		if($_SESSION['addallo']=="yes"){
			echo '<div style="display:block" align="center" id="alloblock" class="white_content2">';
			$_SESSION['addallo']="no";}
		else
			{echo '<div style="display:none" align="center" id="alloblock" class="white_content2">';}
	}
	else
		{echo '<div style="display:none" align="center" id="alloblock" class="white_content2">';}
	if(isset($_SESSION['addalloerror'])){
		if($_SESSION['addalloerror']=="yes"){
			echo '<h3>Allowance Already Exists</h3>';
			$_SESSION['addalloerror']="no";}
	}?>
		<form action="submitallowance.php" method="post" onsubmit="hidelightboxallo()">
			<table style="background-color:#E7E5E5;width:100%" class="table table-condensed table-hover table-bordered">
				<tr >
					<th class="center">ALLOWANCE</th>
					<th class="center">STATUS</th>
				  </tr>
			<?
			while($rowallo=mysql_fetch_array($resultallooff)){
				echo '<tr><td class="center" width="15%">'.$rowallo['ALLOWANCE'].'</td>
					<td class="center" width="10%"><input  style="height:25px" type="checkbox" name='.$rowallo['ALLOWANCE'].' id='.$rowallo['ALLOWANCE'].'></td></tr>';
			}
			while($rowallo=mysql_fetch_array($resultallo)){
				echo '<tr><td class="center" width="15%">'.$rowallo['ALLOWANCE'].'</td>
					<td class="center" width="10%"><input  style="height:25px" type="checkbox"  checked="TRUE" name='.$rowallo['ALLOWANCE'].' id='.$rowallo['ALLOWANCE'].'></td></tr>';
			}?>
			</table>
			<div align="center"><button class=" btn btn-info" type="submit">Submit</button></div>
		</form>
		<form class="form-inline" action="addallowance.php" method="post" onsubmit="return checkallo()">
		  <input style="height:25px" type="text" name="inallo" id="inallo" placeholder="ALLOWANCE">
		  <button style="height:25px" type="submit" class="btn btn-info">Add</button>
		</form>
	</div>
	<?
	//code for branch
	if(isset($_SESSION['addbranch'])){
		if($_SESSION['addbranch']=="yes"){
			echo '<div style="display:block" id="fade" class="black_overlay1"></div>';}
		else
			{echo '<div style="display:none" id="fade" class="black_overlay1"></div>';}
	}
	else
		{echo '<div style="display:none"id="fade" class="black_overlay1"></div>';}
	if(isset($_SESSION['addbranch'])){
		if($_SESSION['addbranch']=="yes"){
			echo '<div style="display:block" align="center" id="branchblock" class="white_content2">';
			$_SESSION['addbranch']="no";}
		else
			{echo '<div style="display:none" align="center" id="branchblock" class="white_content2">';}
	}
	else
		{echo '<div style="display:none" align="center" id="branchblock" class="white_content2">';}
	if(isset($_SESSION['addbrancherror'])){
		if($_SESSION['addbrancherror']=="yes"){
			echo '<h3>Branch Already Exists</h3>';
			$_SESSION['addbrancherror']="no";}
	}?>
		<form action="submitbranch.php" method="post" onsubmit="hidelightboxbranch()">
			<table style="background-color:#E7E5E5;width:100%" class="table table-condensed table-hover table-bordered">
				<tr >
					<th class="center">BRANCH</th>
					<th class="center">STATUS</th>
				  </tr>
			<?
			while($rowbranch=mysql_fetch_array($resultbranchoff)){
				echo '<tr><td class="center" width="15%">'.$rowbranch['BRANCH'].'</td>
					<td class="center" width="10%"><input  style="height:25px" type="checkbox" name='.$rowallo['BRANCH'].' id='.$rowbranch['BRANCH'].'></td></tr>';
			}
			while($rowbranch=mysql_fetch_array($resultbranch)){
				echo '<tr><td class="center" width="15%">'.$rowbranch['BRANCH'].'</td>
					<td class="center" width="10%"><input  style="height:25px" type="checkbox"  checked="TRUE" name='.$rowbranch['BRANCH'].' id='.$rowbranch['BRANCH'].'></td></tr>';
			}?>
			</table>
			<div align="center"><button class=" btn btn-info" type="submit">Submit</button></div>
		</form>
		<form class="form-inline" action="addbranch.php" method="post" onsubmit="return checkbranch()">
		  <input style="height:25px" type="text" name="inbranch" id="inbranch" placeholder="BRANCH">
		  <button style="height:25px" type="submit" class="btn btn-info">Add</button>
		</form>
	</div>
	
	<?//companydetails?>
	<div style="width:1250px;font-weight:bold;padding-top:0px;" id="main" class="container  well">
		<h3 align="center" >Company Details</h3><hr>
		<form action="updatecompdetail.php" method="post"  class="form-horizontal" onsubmit="return check();">
			<div style="width:1200px;"class="container well">
				<div style="float:left;width:32%" class="block">
					<div class="control-group">
						<label class="control-label bold">ID :</label>
						<div class="controls">
						  <input style="height:25px" type="text" disabled="disabled" value="<?echo $row['COMPID']?>" placeholder="ID">
						  <input style="height:25px" type="hidden" value="<?echo $row['COMPID']?>" name="compid">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">NAME :</label>
						<div class="controls">
						  <input style="height:25px" type="text" name="compname"  id="compname" placeholder="NAME" value="<?echo $row['NAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">COMPANY FULLNAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compfullname"  id="compfullname" placeholder="FULLNAME" value="<?echo $row['FULLNAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">NATURE :</label>
						<div class="controls">
							<select style="height:25px;" name="nature">
								<option value='NO'>NO</option>
								<?while($rownature=mysql_fetch_array($resultnature)){
									if($rownature['NATURE']==$row['NATURE']){
										echo '<option selected="selected" value='.$rownature['NATURE'].'>'.$rownature['NATURE'].'</option>';
									}
									else{
										echo '<option value='.$rownature['NATURE'].'>'.$rownature['NATURE'].'</option>';
									}
								}?>
								
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">AREA :</label>
						<div class="controls">
							<option value='NO'>NO</option>
							<select style="height:25px;" name="area">
								<?while($rowarea=mysql_fetch_array($resultarea)){
									echo '<option value='.$rowarea['AREA'].'>'.$rowarea['AREA'].'</option>';
									if($rowarea['AREA']==$row['AREA']){
										echo '<option selected="selected" value='.$rowarea['AREA'].'>'.$rowarea['AREA'].'</option>';
									}
									else{
										echo '<option value='.$rowarea['AREA'].'>'.$rowarea['AREA'].'</option>';
									}
								}?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK NAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compbankname" id="compbankname" placeholder="NAME" value="<?echo $row['BANKNAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK BRANCH  :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compbranch" id="compbranch" placeholder="BRANCH" value="<?echo $row['BANKBRANCH']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK ACCOUNTNO  :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compaccno" id="compaccno" placeholder="NO" value="<?echo $row['BANKACCNO']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK ADDRESS  :</label>
						<div class="controls">
						  <textarea name="compbankaddress" id="compbankaddress" rows="3" cols="50"><?echo $row['BANKADD']?></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PAN NO  :</label>
						<div class="controls">
						  <input  style="height:25px" type="text"  name="comppanno" id="comppanno" placeholder="NO" value="<?echo $row['PANNO']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PF  :</label>
						<div class="controls">
						  <input  style="height:25px" type="checkbox"  id="comppf" onclick="showdiv('comppf','pf')" <?if ($row['PF']=='Y') echo "checked='TRUE'";?>>
						  <input  style="height:25px" type="hidden" name="comppf" id="comppfhidden" value="<?echo $row['PF']?>">
						</div>
					</div>
					<div id="pf" style="display:<?if ($row['PF']=='Y') echo "block";else echo"none";?>" class="well">
						<div align="center">PF DETAILS</div><hr>
						<div class="control-group">
							<label class="control-label bold">EMPLOYEE-PF % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compemppf" id="compemppf" value="<?echo $row['EMPPF']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">COMPANY-PF % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compcomppf" id="compcomppf" value="<?echo $row['COMPF']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">COMP-PENSION % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compcomppns" id="compcomppns" value="<?echo $row['COMPPNS']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">COMPANY-A/C 2 % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compcompac2" id="compcompac2" value="<?echo $row['COMPACC2']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">COMPANY-A/C 21 % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compcompac21" id="compcompac21" value="<?echo $row['COMPACC21']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">COMPANY-A/C 22 % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compcompac22" id="compcompac22" value="<?echo $row['COMPACC22']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">PF-RATE :</label>
							<div class="controls">
								Full-Wage 
								<input   type="radio" name="pfrate" id="pffullwage" value="FW" onclick="hidepfrate()" <?if($row['PFRATETYPE']=='FW')echo 'checked="TRUE"';?>><br>
								Acc-Rules 
								<input   type="radio" name="pfrate" id="pfrules" value="AR" onclick="hidepfrate()" <?if($row['PFRATETYPE']=='AR')echo 'checked="TRUE"';?>><br>
								W-W 
								<input   type="radio" name="pfrate" id="pfrules" value="WW" onclick="hidepfrate()" <?if($row['PFRATETYPE']=='WW')echo 'checked="TRUE"';?>><br>
								Comp-Defined 
								<input   type="radio" name="pfrate" id="pfdefined" value="D" onclick="showpfrate()" <?if($row['PFRATETYPE']=='D')echo 'checked="TRUE"';?>>
								<div style="display:<?if($row['PFRATETYPE']=='D')echo 'block';else echo 'none'?>" id=pfrate>
									<input  style="height:25px;width:140px" type="text" name="comppfrate" id="comppfrate" value="<?if($row['PFRATETYPE']=='D')echo $row['PFRATE']?>">
								</div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">DATE  :</label>
							<div class="controls">
							  <input  style="height:25px;width:140px" type="date" name="comppfdate" id="comppfdate" value="<?echo $row['DATEOFPF']?>">
							</div>
						</div>
					</div>
				</div>
				<div style="float:left;width:32%" class="block">
					<div class="control-group">
						<label class="control-label bold">DEPT :</label>
						<div class="controls">
						<?if($row['DEPT']=='Y'){
							echo 'YES &nbsp <input  style="height:25px" checked="TRUE" type="checkbox"  id="compdeptyes" name="compdeptyes" onclick="showlightbox()">&nbsp&nbsp';
						}
						else
							echo 'YES &nbsp <input  style="height:25px" type="checkbox" name="compdeptyes" id="compdeptyes" onclick="showlightbox()">&nbsp&nbsp';?>
						  
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ALLOWANCES :</label>
						<div class="controls">
							<input  style="height:25px" type="checkbox"  id="compallo" name="compallo" onclick="showlightboxallo()">					  
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BRANCH :</label>
						<div class="controls">
							<input  style="height:25px" type="checkbox"  id="branch" name="branch" onclick="showlightboxbranch()">					  
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ADDRESS :</label>
						<div class="controls">
						  <textarea id="compaddress" name="compaddress" rows="3" cols="50"><?echo $row['ADD1']?></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">CITY :</label>
						<div class="controls">
						  <input style="height:25px" type="text" name="compcity" id="compcity" placeholder="CITY" value="<?echo $row['CITY']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DISTRICT :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compdistrict" id="compdistrict" placeholder="DISTRICT" value="<?echo $row['DISTRICT']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">STATE :</label>
						<div class="controls">
						  <input style="height:25px"type="text" name="compstate" id="compstate" placeholder="STATE" value="<?echo $row['STATE']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PIN :</label>
						<div class="controls">
						  <input style="height:25px"type="text" name="comppin" placeholder="PIN" value="<?echo $row['PIN']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PHONE1 :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compphone1" placeholder="PHONE" value="<?echo $row['PHONE1']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PHONE2 :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compphone2" placeholder="PHONE" value="<?echo $row['PHONE2']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">FAX :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compfax" placeholder="FAX" value="<?echo $row['FAX']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">EMAIL :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compemail" placeholder="EMAIL" value="<?echo $row['EMAIL']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ESI  :</label>
						<div class="controls">
						  <input  style="height:25px" type="checkbox" id="compesi" onclick="showdiv('compesi','esi')" <?if ($row['ESI']=='Y') echo "checked='TRUE'";?>>
						  <input  style="height:25px" type="hidden" name="compesi" id="compesihidden" value="<?echo $row['ESI']?>">
						</div>
					</div>
					<div id="esi" style="display:<?if ($row['ESI']=='Y') echo "block";else echo"none";?>" class="well">
						<div align="center">ESI DETAILS</div><hr>
						<div class="control-group">
							<label class="control-label bold">EMPLOYEE-ESI % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compempesi" id="compempesi" value="<?echo $row['EMPESI']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">COMPANY-ESI % :</label>
							<div class="controls">
							  <input  style="height:25px;width:100px" type="text" name="compcompesi" id="compcompesi" value="<?echo $row['COMPESI']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">ESI-RATE :</label>
							<div class="controls">
								Full-Wage 
								<input   type="radio" name="esirate" id="esifullwage" value="FW" onclick="hideesirate()" <?if($row['ESIRATETYPE']=='FW')echo 'checked="TRUE"';?>><br>
								Acc-Rules 
								<input   type="radio" name="esirate" id="esirules" value="AR" onclick="hideesirate()" <?if($row['ESIRATETYPE']=='AR')echo 'checked="TRUE"';?>><br>
								W-W 
								<input   type="radio" name="esirate" id="esirules" value="WW" onclick="hideesirate()" <?if($row['ESIRATETYPE']=='WW')echo 'checked="TRUE"';?>><br>
								Comp-Defined 
								<input   type="radio" name="esirate" id="esidefined" value="D" onclick="showesirate()" <?if($row['ESIRATETYPE']=='D')echo 'checked="TRUE"';?>>
								<div style="display:<?if($row['ESIRATETYPE']=='D')echo 'block';else echo 'none'?>" id=esirate>
									<input  style="height:25px;width:140px" type="text" name="compesirate" id="compesirate" value="<?if($row['ESIRATETYPE']=='D')echo $row['ESIRATE']?>">
								</div>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">DATE  :</label>
							<div class="controls">
							  <input  style="height:25px;width:140px" type="date" name="compesidate" id="compesidate" value="<?echo $row['DATEOFESI']?>">
							</div>
						</div>
					</div>
				</div>
				<div style="float:left;width:25%" class="block">
					<div align="center">DAYS AND OVERTIME</div>
					<div class="well">
						<div class="control-group">
							<label class="control-label bold">HRS/DAY :</label>
							<div class="controls">
							  <input  style="height:25px;width:60px" type="text" name="comphrs" id="comphrs" value="<?echo $row['HRSDAY']?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">OVERTIME FACTOR :</label>
							<div class="controls">
							  <input  style="height:25px;width:60px" type="text" name="compovertimef" id="compovertime" value="<?echo $row['OVERTIMEF']?>">
							</div>
						</div>
					</div>
					<div align="center">BONUS RATES</div>
					<div class="well">
						<div class="control-group">
							<label class="control-label bold">PER % :</label>
							<div class="controls">
							  <input  style="height:25px;width:60px" type="text" name="compbonus" id="compbonus" value="<?echo $row['BONUSPER']*100?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">MAX BONUS :</label>
							<div class="controls">
							  <input  style="height:25px;width:90px" type="text" name="compmaxbonus" id="compmaxbonus" value="<?echo $row['MAXBONUS']?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		  <div align="center"><button class="btn btn-primary">Submit</button></div>
		</form>
	</body>
</html>

<?
}
else
echo "INVALID ACCESS";
?>
		
		
