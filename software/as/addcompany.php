<?
require "dbconnect.php";
session_start();
if(isset($_SESSION['compid'])){
	$compid=$_SESSION['compid'];
	$compname=$_SESSION['compname'];
	$result=mysql_query("select * from companymaster where COMPID=".$compid);
	$resultnature=mysql_query("select * from nature");
	$resultarea=mysql_query("select * from area");
	$resultbranch=mysql_query("select * from compbranch where FLAG=1 and COMPID=".$compid);
	$resultbranchoff=mysql_query("select * from compbranch where FLAG=0 and COMPID=".$compid);
	$resultdept=mysql_query("select * from compdepartment where FLAG=1 and COMPID=".$compid);
	$resultdeptoff=mysql_query("select * from compdepartment where FLAG=0 and COMPID=".$compid);
	$resultallo=mysql_query("select * from compallowances where FLAG=1 and COMPID=".$compid);
	$resultallooff=mysql_query("select * from compallowances where FLAG=0 and COMPID=".$compid);
	$row=mysql_fetch_array($result);	
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>companydetail</title>
        <link rel='stylesheet' href='bootstrap.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='header.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='lightbox.css' type='text/css' media='print, projection, screen' />
        <script type='text/javascript' src='jquery-1.8.2.min.js'></script> 
        <script type='text/javascript' src='bootstrap.min.js'></script>
        <style>
            body{margin:0px; padding:0px; color:black; font-family: 'Helvetica Neue';}
			.bold{
			font-weight:bold
			}
			div.block{
				margin:10px;
				padding:10px;
			}
			th.center{
				background-color:#c0c0c0;
			}
        </style>
		<script>
		function showdiv(id1,id2)
		{
			if(document.getElementById(id1).checked == true){
				document.getElementById(id2).style.display='block';
				document.getElementById(id1+"hidden").value="Y";
			}
			else if(document.getElementById(id1).checked == false){
				document.getElementById(id2).style.display='none';
				document.getElementById(id1+"hidden").value="N";
			}
		}
		function check(){
                var compname = document.getElementById("compname").value;
                if(compname==""){
                    alert("Enter Compname");
                    return false; 
                }
        }
		function checkdept(){
                var deptname = document.getElementById("indept").value;
                if(deptname==""){
                    alert("Enter Deptname");
                    return false; 
                }
        }
		function checkbranch(){
                var deptname = document.getElementById("inbranch").value;
                if(deptname==""){
                    alert("Enter Branchname");
                    return false; 
                }
        }
		function checkdeptinput(){
			if(document.getElementById("compdeptyes").checked == true && document.getElementById("compdeptno").checked == true){
				alert("Only 1 input is valid yes or no");
				return false; 
			}
			if(document.getElementById("compdeptyes").checked != true && document.getElementById("compdeptno").checked != true){
				alert("choose 1 input yes or no");
				return false; 
			}
		}
			
		function showlightbox()
		{
			if(document.getElementById("compdeptyes").checked == true){
				document.getElementById('deptblock').style.display='block';
				document.getElementById('fade').style.display='block';
			}
		}
		function showlightboxbranch()
		{
			if(document.getElementById("branch").checked==true){
				document.getElementById('branchblock').style.display='block';
				document.getElementById('fade').style.display='block';
			}
		}
		function hidelightboxbranch()
		{
			document.getElementById('branchblock').style.display='none';
			document.getElementById('fade').style.display='none';
		}
		function showlightboxallo()
		{
			if(document.getElementById("compallo").checked == true){
				document.getElementById('alloblock').style.display='block';
				document.getElementById('fade').style.display='block';
			}
		}
		function hidelightboxallo()
		{
			document.getElementById('alloblock').style.display='none';
			document.getElementById('fade').style.display='none';
		}
		function showpfrate()
		{
			document.getElementById('pfrate').style.display='block';
		}
		function hidepfrate()
		{
			document.getElementById('pfrate').style.display='none';
		}
		function showesirate()
		{
			document.getElementById('esirate').style.display='block';
		}
		function hideesirate()
		{
			document.getElementById('esirate').style.display='none';
		}
		function hidelightbox()
		{
			document.getElementById('deptblock').style.display='none';
			document.getElementById('fade').style.display='none';
		}
			
		</script>
	</head>
	<body>
	<?require 'adminheader.php';
	
	///companydetails?>
	<div style="width:1250px;font-weight:bold;padding-top:0px;" id="main" class="container  well">
		<h3 align="center" >Company Details</h3><hr>
			<div style="width:1200px;"class="container well">
				<div style="float:left;width:32%" class="block">
				<form action="insertcompdetail.php" method="post"  class="form-horizontal" onsubmit="return check();">
					<div class="control-group">
						<label class="control-label bold">NAME :</label>
						<div class="controls">
						  <input style="height:25px" type="text" name="compname"  id="compname" placeholder="NAME" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">COMPANY FULLNAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compfullname"  id="compfullname" placeholder="FULLNAME" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ADDRESS :</label>
						<div class="controls">
						  <textarea id="compaddress" name="compaddress" rows="3" cols="50"></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">CITY :</label>
						<div class="controls">
						  <input style="height:25px" type="text" name="compcity" id="compcity" placeholder="CITY" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DISTRICT :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="compdistrict" id="compdistrict" placeholder="DISTRICT" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">STATE :</label>
						<div class="controls">
						  <input style="height:25px"type="text" name="compstate" id="compstate" placeholder="STATE" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PIN :</label>
						<div class="controls">
						  <input style="height:25px"type="text" name="comppin" placeholder="PIN" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PHONE1 :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compphone1" placeholder="PHONE" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PHONE2 :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compphone2" placeholder="PHONE" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">FAX :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compfax" placeholder="FAX" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">EMAIL :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="compemail" placeholder="EMAIL" >
						</div>
					</div>
				
					<div align="center"><button class="btn btn-primary">Submit</button></div>
				</form>
				</div>
				<?//for file uploading?>
				<div style="float:right;width:32%" class="block">
					<form action="importcompanydetail.php" method="post" enctype="multipart/form-data" >
						<input name="ufile" type="file" id="ufile" size="50">
						<?if(isset($_SESSION['compupload'])){
							if($_SESSION['compupload']!="not"){
								echo "<h4>".$_SESSION['compupload']."</h4>";
								$_SESSION['compupload']="not";
							}
						}?>
						<button type="submit" class="btn btn-primary">Import</button>
					</form>
				</div>
			</div>
	</body>
</html>

<?
}
else
echo "INVALID ACCESS";
?>
		
		
