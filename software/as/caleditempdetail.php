<?
require "dbconnect.php";
session_start();
if(isset($_SESSION['compid'])){
	$compid=$_SESSION['compid'];
	$compname=$_SESSION['compname'];
	$wid=$_GET['workerid'];
	$result=mysql_query("select * from ".$compid."employeedetail where WORKERID=".$wid);
	$compdetail=mysql_query("select * from companymaster where COMPID=".$compid);
	$row=mysql_fetch_array($result);
	$rowcdetail=mysql_fetch_array($compdetail);
	//calculation for pf and esi rates and eligibility
	//checking the basic + DA is less then the mentioned pf cap in companymaster
	//if basic + DA is less then employee is eligible for pf else no
	//same for esi checking the total salary(basic+hr+da+all allowances) of employee and settinf esi yes or no
	$pfall=$row['BR']+$row['DA'];
	$esiall=$row['BRTOT'];
	//echo $esiall."esiall<br>";
	$comppfrate=$rowcdetail['PFRATE'];
	$compesirate=$rowcdetail['ESIRATE'];

	$pf='N';
	$esi='N';
	$pfrate=0;
	$esirate=0;
	if($rowcdetail['PF']=='Y'){
		if($rowcdetail['PFRATETYPE']=='WW'){
			$pf='Y';
		}
		else if($rowcdetail['PFRATETYPE']=='AR'){
			if($pfall<=$comppfrate){
				$pf='Y';
				$pfrate=$pfall;
				//echo $pfrate."inar<br>";
			}
		}
		else if($rowcdetail['PFRATETYPE']=='FW'){
			$pf='Y';
			$pfrate=$pfall;
			//echo $pfrate."infw<br>";
		}
		else {
			$pf='Y';
			$pfrate=$comppfrate;
			//echo $pfrate."ind<br>";
		}
	}
	if($rowcdetail['ESI']=='Y'){
		if($rowcdetail['ESIRATETYPE']=='WW'){
			$esi='Y';
		}
		else if($rowcdetail['ESIRATETYPE']=='AR'){
			if($esiall<=$compesirate){
				$esi='Y';
				$esirate=$esiall;
			}
		}
		else if($rowcdetail['ESIRATETYPE']=='FW'){
				$esi='Y';
				$esirate=$esiall;
		}
		else {
			$esi='Y';
			$esirate=$compesirate;
		}
	}
		
			
	
	
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>employeedetail</title>
        <link rel='stylesheet' href='bootstrap.css' type='text/css' media='print, projection, screen' />
		<link rel='stylesheet' href='header.css' type='text/css' media='print, projection, screen' />
        <script type='text/javascript' src='jquery-1.8.2.min.js'></script> 
        <script type='text/javascript' src='bootstrap.min.js'></script>
        <style>
            body{margin:0px; padding:0px; color:black; font-family: 'Helvetica Neue';}
			.bold{
			font-weight:bold
			}
			div.block{
				margin:10px;
				padding:10px;
			}
        </style>
		<script>
		function showdiv(id1,id2)
		{
			if(document.getElementById(id1).checked == true){
				document.getElementById(id2).style.display='block';
				document.getElementById(id1+"hidden").value="Y";
			}
			else if(document.getElementById(id1).checked == false){
				document.getElementById(id2).style.display='none';
				document.getElementById(id1+"hidden").value="N";
			}
		}
		function check(){
			var compname = document.getElementById("compname").value;
			if(compname==""){
				alert("Enter Compname");
				return false; 
			}
        }
		function checkesirate(rate,sal)
		{
			var x=document.getElementById("eesivol").value;
			if(x>sal){
				alert("ESI-Rate cannot be more then Total salary");
				alert(x+"---"+sal);
				document.getElementById("eesivol").value="";
			}
		}
		function checkpfrate(rate,sal)
		{
			var x=document.getElementById("epfvol").value;
			if(x>sal){
				alert("PF-Rate cannot be more then BR+DA");
				alert(x+"---"+sal);
				document.getElementById("epfvol").value="";
			}
		}
			
		</script>
	</head>
	<body>
	<?require 'header.php';?>
	<div style="width:94%;font-weight:bold;padding-top:0px" class="container  well">
		<h3 align="center" >Employee Details</h3><hr>
		<form action="updateempdetail.php" method="post"  class="form-horizontal" onsubmit="return check()">
			<div style="width:95%;"class="container well">
				<div style="float:left;width:32%" class="block">
					<div class="control-group">
						<label class="control-label bold">ID :</label>
						<div class="controls">
							<input style="height:25px" disabled="disabled" type="text" placeholder="ID" value="<?echo $row['WORKERID']?>">
							<input style="height:25px" type="hidden" name="eid" value="<?echo $row['WORKERID']?>">
							<input style="height:25px" type="hidden" name="compid" value="<?echo $compid?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">NAME :</label>
						<div class="controls">
						  <input style="height:25px" type="text" name="ename" id="ename" placeholder="NAME" value="<?echo $row['NAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">FATHER NAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="efathername" id="efathername" placeholder="NAME" value="<?echo $row['FNAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">HUSBAND NAME :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="ehusname" id="ehusname" placeholder="NAME" value="<?echo $row['HNAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DEPT :</label>
						<div class="controls">
							<select style="height:25px;" name="edept">
								<option value='NO'>NO</option>
								<?while($rowdept=mysql_fetch_array($resultdept)){
									if($rowdept['DEPT']==$row['DEPT']){
										echo '<option selected="selected" value='.$rowdept['DEPT'].'>'.$rowdept['DEPT'].'</option>';
									}
									else{
										echo '<option value='.$rowdept['DEPT'].'>'.$rowdept['DEPT'].'</option>';
									}
								}?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BRANCH :</label>
						<div class="controls">
							<select style="height:25px;" name="ebranch">
								<option value='NO'>NO</option>
								<?while($rowbranch=mysql_fetch_array($resultbranch)){
									if($rowbranch['BRANCH']==$row['BRANCH']){
										echo '<option selected="selected" value='.$rowbranch['BRANCH'].'>'.$rowbranch['BRANCH'].'</option>';
									}
									else{
										echo '<option value='.$rowbranch['BRANCH'].'>'.$rowbranch['BRANCH'].'</option>';
									}
								}?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DESIGNATION  :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="edesig" id="edesig" placeholder="DESIGNATION" value="<?echo $row['DESIGNATION']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DOB  :</label>
						<div class="controls">
						  <input  style="height:25px" type="date" name="edob" id="edob" value="<?echo date('Y-m-d',strtotime($row['DOB']))?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DOJOINING  :</label>
						<div class="controls">
						 <input  style="height:25px" type="date" name="edoj" id="edoj" value="<?echo date('Y-m-d',strtotime($row['JOINDATE']."-".$row['JOINMONTH']."-".$row['JOINYEAR']))?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">SEX  :</label>
						<div class="controls">
							<select style="height:25px;width:90px" name="esex" value="<?echo $row['SEX']?>">
								<option value="M">MALE</option>
								<option value="F">FEMALE</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ADDRESS :</label>
						<div class="controls">
						  <textarea id="eaddress" name="eaddress" rows="3" cols="50"><?echo $row['ADD1']?></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PIN :</label>
						<div class="controls">
						  <input style="height:25px"type="text" name="epin" placeholder="PIN" value="<?echo $row['PIN']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PF  :</label>
						<div class="controls">
							<input  style="height:25px" type="checkbox"  id="epf" onclick="showdiv('epf','pf')" <?if ($pf=='Y') echo "checked='TRUE'";?>>
							<input  style="height:25px" type="hidden" name="epf" id="pfhidden" value="<?echo $pf;?>">						  
						</div>
					</div>
					<div id="pf" style="display:<?if ($pf=='Y') echo "block";else echo"none";?>" class="well">
						<div align="center">PF DETAILS</div><hr>
						<div class="control-group">
							<label class="control-label bold">PF-NO :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" name="epfno" id="epfno" value="<?echo $row['PFNO']?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">PF-RATE :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" onchange="checkpfrate('<?echo $comppfrate;?>','<?echo $pfall;?>')" name="epfvol" id="epfvol" value="<?echo $pfrate?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">PF-ELIG-DATE  :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="date" name="epfeligdate" id="epfeligdate" value="<?echo $row['PFELIGDATE']?>">
							</div>
						</div>
					</div>
				</div>
				<div style="float:left;width:32%" class="block">
					<div class="control-group">
						<label class="control-label bold">PAYMENT :</label>
						<div class="controls">
							<select style="height:25px;width:120px" name="epayment" value="<?echo $row['PAYMENT']?>">
								<option value="M">MONTHLY</option>
								<option value="D">DAILY</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">REST :</label>
						<div class="controls">
							<select style="height:25px;width:80px" name="erest" value="<?echo $row['REST']?>">
								<option value="7">SUN</option>
								<option value="1">MON</option>
								<option value="2">TUE</option>
								<option value="3">WED</option>
								<option value="4">THU</option>
								<option value="5">FRI</option>
								<option value="6">SAT</option>
								
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">INSURANCE NO :</label>
						<div class="controls">
						  <input  style="height:25px" type="text" name="einno" placeholder="NO" value="<?echo $row['INNO']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">RELIGION :</label>
						<div class="controls">
						  <select style="height:25px;width:150px" name="ereligion" value="<?echo $row['RELIGION']?>">
								<option value="hindu">HINDU</option>
								<option value="muslim">MUSLIM</option>
								<option value="christian">CHRISTIAN</option>
								<option value="sikh">SIKH</option>
							</select>
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label bold">PHONE :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="ephone" placeholder="PHONE" value="<?echo $row['PHONE']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">EMAIL :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="eemail" placeholder="EMAIL" value="<?echo $row['EMAIL']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK NAME :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="ebankname" placeholder="NAME" value="<?echo $row['BANKNAME']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BANK ACCNO :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="ebankaccno" placeholder="ACC NO" value="<?echo $row['BANKACCNO']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">PAN NO :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="epanno" placeholder="PAN NO" value="<?echo $row['PANNO']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">EXP IN YEARS :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="eexpyrs" placeholder="YEARS" value="<?echo $row['EXPINYEARS']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">QUALIFICATION :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="equali" value="<?echo $row['QUALIFICATION']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OPENING EL :</label>
						<div class="controls">
						  <input  style="height:25px"type="text" name="el" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">ESI  :</label>
						<div class="controls">
						  <input  style="height:25px" type="checkbox" id="eesi" onclick="showdiv('eesi','esi')" <?if ($esi=='Y') echo "checked='TRUE'";?>>
						  <input  style="height:25px" type="hidden" name="eesi" id="esihidden" value="<?echo $esi?>">
						</div>
					</div>
					<div id="esi" style="display:<?if ($esi=='Y') echo "block";else echo"none";?>" class="well">
						<div align="center">ESI DETAILS</div><hr>
						<div class="control-group">
							<label class="control-label bold">ESI-NO :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="text" name="eesino" id="eesino" value="<?echo $row['ESINO']?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">ESI-RATE :</label>
							<div class="controls">
								<input  style="height:25px;width:150px" type="text" onchange="checkesirate('<?echo $compesirate;?>','<?echo $esiall;?>')"name="eesivol" id="eesivol" value="<?echo $esirate?>">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label bold">ESI-ELIG-DATE  :</label>
							<div class="controls">
							  <input  style="height:25px;width:150px" type="date" name="eesieligdate" id="eesieligdate" value="<?echo $row['ESIELIGDATE']?>"> 
							</div>
						</div>
					</div>
				</div>
				<div style="float:left;width:26%" class="block">
					<div align="center">ALLOWANCES</div><hr>
					<div class="control-group">
						<label class="control-label bold">BASIC-RATE :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="ebr" id="ebr" placeholder="RATE" value="<?echo $row['BR']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">DA :</label>
						<div class="controls">
						  <input style="height:25px;width:120px"type="text" name="eda" id="eda" placeholder="DA" value="<?echo $row['DA']?>">
						</div>
					</div>
					<div align="left"class="control-group">
						<label class="control-label bold">BASIC LA :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="ela" placeholder="LA" value="<?echo $row['BRLA']?>">
						</div>
					</div>
					<div align="left"class="control-group">
						<label class="control-label bold">BASIC WA :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="ewa" id="ewa" placeholder="WA" value="<?echo $row['BRWA']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">BASIC HR :</label>
						<div class="controls">
						  <input style="height:25px;width:120px"type="text" name="ehra" placeholder="HRA" value="<?echo $row['BRHR']?>">
						</div>
					</div>
					<div align="left"class="control-group">
						<label class="control-label bold">SP :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px" type="text" name="esp" id="esp" placeholder="SP" value="<?echo $row['BRSP']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">CV :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="ecv" placeholder="CV" value="<?echo $row['BRCV']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OTHER1 :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="eother1" value="<?echo $row['OTHER1']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OTHER2 :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="eother2" value="<?echo $row['OTHER2']?>">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label bold">OTHER3 :</label>
						<div class="controls">
						  <input  style="height:25px;width:120px"type="text" name="eother3" value="<?echo $row['OTHER3']?>">
						</div>
					</div>
				</div>
			</div>
		  <div align="center"><button onclick="unsetedit()" class="btn btn-primary">Submit</button></div>
		</form>
	</body>
</html>

<?
}
else
echo "INVALID ACCESS";
?>
	
		
