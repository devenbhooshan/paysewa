<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Feeding</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
.worker_list_name{
font-size:12px;	

	
}
	#worker_list_table td{
		
		min-width:100px;
		border-left:thin;
		text-align:center;
		
	}
	.entry_exist{
	background-color:#CFC;	
		
		
	}
      body {
        padding-top: 0px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->


  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
   
          <a class="brand" href="#">PI Jain</a>
          <div class="nav-collapse collapse">
           
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid" style="padding-top:5px">
      <div class="row-fluid">
      <div class="span4">
      	<div id="client_list">
          
        </div>			 <!--Client List-->
        <table>
        <tbody>
               
        <tr>
        <td id="select_contractoryn">
       
       </td>
       <td id="select_deductionyn">
       </td>
       <td>
       &nbsp;&nbsp;&nbsp;
       </td>
       <td>
        <div class="control-group">
		       <label class="control-label" for="inputA">&nbsp; Arear</label>
	          <div class="controls">
              <input type="radio"  name="ariel" id="ariel1">Yes
              <input type="radio"  name="ariel"  id="ariel2" checked="checked">No
              </div>
         </div> 
         </td>
         </tr>
         </tbody>
       </table>
		  
      </div> <!--Feed Type/Arear/Client-->
      
      <div class="span3">
		<?php  include("month_year_list.php")?>
    	    <div id='previous_entry_date'>
            </div> 
      </div>  <!--Month Year -->
      <div class="span3">
		<div id="department_list">
        
        </div>
        
      </div>  <!--Department List-->
        <div class="span2">
      	 
        <div id="contractor_list" > </div>
		  
    		

      </div> <!--contractor / Button-->
      </div> <!-- Top menu-->

      <div class="row-fluid">
        <div class="span5">
          <div class="well sidebar-nav">
            <ul class="nav nav-list" >
            
              <li class="nav-header">Worker List</li>
		   			<table>	 
        			    <tbody id="worker_list_table">           
				          
			              <div id="worker_list">
                          
              
              </div>
             </tbody>
             </table>
             
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span7" style="background-color:#F5F5F5; border:thin;   " >
         
            <div id="form_area" style="padding-left:10px; padding-top:10px;">
            </div>
            
         
         
        </div><!--/span-->
      </div><!--/row-->

      <hr>

<!--      <footer>
        <p>&copy; Company 2013</p>
      </footer>
 -->     

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap-transition.js"></script>
    <script src="../js/bootstrap-alert.js"></script>
    <script src="../js/bootstrap-modal.js"></script>
    <script src="../js/bootstrap-dropdown.js"></script>
    <script src="../js/bootstrap-scrollspy.js"></script>
    <script src="../js/bootstrap-tab.js"></script>
    <script src="../js/bootstrap-tooltip.js"></script>
    <script src="../js/bootstrap-popover.js"></script>
    <script src="../js/bootstrap-button.js"></script>
    <script src="../js/bootstrap-collapse.js"></script>
    <script src="../js/bootstrap-carousel.js"></script>
    <script src="../js/bootstrap-typeahead.js"></script>
    <script src="js/javascript.js"></script>

  </body>
</html>
