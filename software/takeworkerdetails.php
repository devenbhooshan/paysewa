<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Worker Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" >
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    
    <style type="text/css">

      /* Sticky footer styles
      -------------------------------------------------- */



      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -60px;
      }

      /* Set the fixed height of the footer here */
      #push,
      #footer {
        height: 60px;
      }
      #footer {
        background-color: #f5f5f5;
      }

      /* Lastly, apply responsive CSS fixes as necessary */
      @media (max-width: 767px) {
        #footer {
          margin-left: -20px;
          margin-right: -20px;
          padding-left: 20px;
          padding-right: 20px;
        }
      }
</style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
      <div id='wrap'>
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">PI Jain Company</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#" target="_blank">About Us</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">Help</a></li>
              <?php
                if(isset($_SESSION['SESS_MEMBER_USERNAME'])) 
                echo '<li><a href="logout.php">Logout</a></li>';
				?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div> 
    
        <div class="container-fluid">
    		<div class="row-fluid">
            
    			<div class="span3">
					<br>	    
				 	<div style="max-width:100%; overflow:auto; max-height:500px; ">
         				<!-- Here the client list is printed-->		
                       <div id='client_list'></div> 
					</div>
                    <!-- Company Name is printed here-->	
                     <?php 
					 include("dbinfo.php");
					 if(array_key_exists('id',$_GET)&&$_GET['id']!=0){
						$clientid=$_GET['id'];
						$name=mysql_fetch_array(mysql_query("select name from clientdetails where id='$clientid'"));
						//echo "<h2>".$name['name']."</h2>";
					}
					else //echo "<h2>No Client Selected </h2>";
					?>
       			</div>
                
                <div class="span3" >
                	<!-- Drop down for the Worker type: All or Current user-->
                	<div id='worker_type_level_1'>
                    
                	</div>
        		</div>
    			<div class="span3">
                	<!-- Drop down for the order by Name, ESI, EPF , Joinind Date , DOB -->
                	<div id="worker_type_level_2">
                    </div>
                </div>

                <div class="span3">
                	<!-- Drop Down list for workers Name -->
                	<div id="worker_list">
                    
                    </div>
                	
           		</div>

    			
			    <!--Body content-->
                           
					
		
                
       			</div>
    		</div><!--Row fluid ends here-->
        	    
            
    	</div><!-- Container fluid ends here-->
			<div id='menu_worker'>

			</div>

        			<hr> 
					<div id='worker_form_area'></div>
		
    </div>
</div> <!-- /container -->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="jsfunction/corefunctions.js"></script>
        <script src="jsfunction/javascript.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>
	<script>
	$.ajax({
		method: "GET",
		url: "client_list.php?requestcode=1",
		success: function(data){
		document.getElementById('client_list').innerHTML=data;				
				
			
			
		}
		
		
	});
	
	</script>
  </body>
</html>
