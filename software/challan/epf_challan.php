<?php
require('../Classes/PHPPdf/fpdf.php');
require("../dbinfo.php"); // requires

class PDF extends FPDF{
	
	function Footer()
{
	$this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
} // footer
	public $challan_of="EPF Challan Summary";
	public $table_name="crpfchallan";
	public $total_wage_app;
	
	function topheading(){
		$this->total_wage_app=$_REQUEST['total_wage_app'];
				$this->SetFont('Arial','B','20');
			$this->SetXY(70,5);
			$this->Cell(10,10,$this->challan_of);
			$this->SetFont('Arial','B','10');
			$this->Ln(10);
			$this->Cell(20,10,'');
			$this->Cell(30,10,"Name of the Employer:",'','','R');
			$clientid=$_REQUEST['clientId'];
			$query=mysql_query("select clientdetails.name,cesidetail.esiNo from clientdetails inner join cesidetail on clientdetails.id=cesidetail.clientId where clientdetails.id='$clientid'");
			if(mysql_num_rows($query)>0){
				$row=mysql_fetch_array($query);	
				$name=$row['name'];
				$esiNo=$row['esiNo'];
			}
			$this->SetFont('Arial','','10');
			if(strlen($name)>=35)
				$name=substr($name,0,35)."...";
				
			$this->Cell(70,10,$name);
			$this->SetFont('Arial','B','10');
			$this->Cell(20,10,"Code No:",'','','R');
			$this->SetFont('Arial','','10');
			$this->Cell(30,10,$esiNo);	
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(10);
		

		
	} // top heading
	
	
	function show_fields(){
		$this->SetFont('Arial','B',8);		
		$this->Cell('10','10','Month','','','R');
		$this->Cell('10','10','Year','','','R');
		$this->Cell('22','10','Salary/','','','R');
		$this->Cell('22','10','Date of','','','R');
		$this->Cell('22','10','A/C No 1','','','R');
		$this->Cell('20','10','A/C No 1','','','R');
		$this->Cell('20','10','A/C','','','R');
		$this->Cell('20','10','A/C','','','R');
		$this->Cell('20','10','A/C','','','R');
		$this->Cell('12','10','A/C','','','R');
		if($this->total_wage_app=='1')
		$this->Cell('16','10','Total','','','R');
		$this->Ln(5);
		$this->Cell('20','10','','','');
		$this->Cell('22','10','Wages','','','R');
		$this->Cell('22','10','Payment','','','R');
		$this->Cell('22','10','Employee','','','R');
		$this->Cell('20','10','Employer','','','R');
		$this->Cell('20','10','No 2','','','R');
		$this->Cell('20','10','No 10','','','R');
		$this->Cell('20','10','No 21','','','R');
		$this->Cell('12','10','No 22','','','R');
		if($this->total_wage_app=='1')
		$this->Cell('16','10','Wage','','','R');
		$this->Ln(9);
			$this->Cell(195,10,'','T'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','T'); // line break
			$this->Ln(1);
	} // Show Fields
	
	function show_data(){
		
			$clientid=$_REQUEST['clientId'];
		$monthfrom=$_REQUEST['monthfrom'];
		$monthto=$_REQUEST['monthto'];
		$yearfrom=$_REQUEST['yearfrom'];
		$yearto=$_REQUEST['yearto'];
		$type=$_REQUEST['type'];
		$query=mysql_query("select * from $this->table_name where clientId='$clientid' and type='$type' and year>='$yearfrom' and month>='$monthfrom' and year<='$yearto' and month<='$monthto' order by year desc");
		if(mysql_num_rows($query)>0){
			while($row=mysql_fetch_array($query)){
			$month=$row['month'];
			$year=$row['year'];
			$epfWage=$row['epfWage'];
			$ac1E=$row['ac1E'];
			$ac1W=$row['ac1W'];
			$ac2E=$row['ac2E'];
			$ac10E=$row['ac10E'];
			$ac21E=$row['ac21E'];
			$ac22E=$row['ac22E'];
			$totalWage=$row['totalWage'];
			$challanDepDate=$row['challanDepDate'];	
			$this->SetFont('Arial','B',8);
			$month_list=array('','Jan','Feb','Mar','Apr','May','June','Jul','Aug','Sep','Oct','Nov','Dec');		
			$this->Cell('10','10',$month_list[$month],'','','R');
			$this->Cell('10','10',$year,'','','R');
			$this->Cell('22','10',$epfWage,'','','R');
			$this->Cell('22','10',$challanDepDate,'','','R');
			$this->Cell('22','10',$ac1E,'','','R');
			$this->Cell('20','10',$ac1W,'','','R');
			$this->Cell('20','10',$ac2E,'','','R');
			$this->Cell('20','10',$ac10E,'','','R');
			$this->Cell('20','10',$ac21E,'','','R');
			$this->Cell('12','10',$ac22E,'','','R');
			if($this->total_wage_app=='1')
			$this->Cell('16','10',$totalWage,'','','R');
			$this->Ln(5);
	
			}
			
		}
		
			$this->Ln(4);
			$this->Cell(195,10,'','T'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','T'); // line break
			$this->Ln(1);
	}
	
}
$pdf = new PDF();
$pdf->SetFont('Arial','',10);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->topheading();
$pdf->show_fields();
$pdf->show_data();
$pdf->Output();



?>