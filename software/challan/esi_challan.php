<?php
require('../Classes/PHPPdf/fpdf.php');
require("../dbinfo.php"); // requires

class PDF extends FPDF{
	
	function Footer()
{
	$this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
} // footer
	public $challan_of="ESI Challan Summary";
	public $table_name="cresichallan";
	
	function topheading(){
			$this->SetFont('Arial','B','20');
			$this->SetXY(70,5);
			$this->Cell(10,10,$this->challan_of);
			$this->SetFont('Arial','B','10');
			$this->Ln(10);
			$this->Cell(20,10,'');
			$this->Cell(30,10,"Name of the Employer:",'','','R');
			$clientid=$_REQUEST['clientId'];
			$query=mysql_query("select clientdetails.name,cesidetail.esiNo from clientdetails inner join cesidetail on clientdetails.id=cesidetail.clientId where clientdetails.id='$clientid'");
			if(mysql_num_rows($query)>0){
				$row=mysql_fetch_array($query);	
				$name=$row['name'];
				$esiNo=$row['esiNo'];
			}
			$this->SetFont('Arial','','10');
			if(strlen($name)>=35)
				$name=substr($name,0,35)."...";
				
			$this->Cell(70,10,$name);
			$this->SetFont('Arial','B','10');
			$this->Cell(20,10,"Code No:",'','','R');
			$this->SetFont('Arial','','10');
			$this->Cell(30,10,$esiNo);	
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(10);
		
	} // top heading
	
	function show_fields(){
		$query_for_column_names=mysql_query("show columns from $this->table_name");
		$this->SetFont('Arial','',9);
		
		$this->Cell(8,10,'Month','','','R');
		$this->Cell(8,10,'Year','','','R');
		$this->Cell('23','10',"Staff",'','0','R');

		$this->Cell('23','10',"Staff",'','0','R');
		$this->Cell('23','10',"ESI Workers",'','0','R');
		$this->Cell('23','10',"Wages",'','0','R');
		$this->Cell('23','10',"Employee's",'','0','R');
		$this->Cell('23','10',"Employer's",'','0','R');
		$this->Cell('23','10',"Total",'','0','R');
		$this->Cell('23','10',"Date of",'','0','C');
		$this->Ln(5);
		$this->Cell(16,10,'','','','R');
		$this->Cell('23','10',"Contractor",'','0','R');
		$this->Cell('23','10',"Contractor",'','0','R');
		$this->Cell('23','10',"",'','0','R');
		$this->Cell('23','10',"",'','0','R');
		$this->Cell('23','10',"Share",'','0','R');
		$this->Cell('23','10',"Share",'','0','R');
		$this->Cell('23','10',"",'','0','R');
		$this->Cell('23','10',"Payment",'','0','C');
		$this->Ln(3);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(10);
	
	} // show_fields
	
	function show_data(){
		$clientid=$_REQUEST['clientId'];
		$monthfrom=$_REQUEST['monthfrom'];
		$monthto=$_REQUEST['monthto'];
		$yearfrom=$_REQUEST['yearfrom'];
		$yearto=$_REQUEST['yearto'];
		$type=$_REQUEST['type'];
		$query=mysql_query("select * from $this->table_name where clientId='$clientid' and type='$type' and year>='$yearfrom' and month>='$monthfrom' and year<='$yearto' and month<='$monthto' order by year desc");
		if(mysql_num_rows($query)>0){
			while($row=mysql_fetch_array($query)){
				$month=$row['month'];
				$year=$row['year'];
				$totalWorkers=$row['totalWorkers'];
				$totalContWorkers=$row['totalContWorkers'];
				$totalWage=$row['totalWage'];
				$totalContWage=$row['totalContWage'];
				$esiWorker=$row['esiWorker'];
				$esiWage=$row['esiWage'];
				$wrkerShare=$row['wrkerShare'];
				$emprShare=$row['emprShare'];
				$challanAmount=$row['challanAmount'];
				$challanDepDate=$row['challanDepDate'];
				$month_list=array('','Jan','Feb','Mar','Apr','May','June','Jul','Aug','Sep','Oct','Nov','Dec');
				$this->Cell('8',10,$month_list[$month],'','','R');
				$this->Cell('8',10,$year,'','','R');
				$this->Cell('23',10,$totalWorkers,'','','R');
						$x=$this->GetX()-23;
						$y=$this->GetY();
						$this->Ln(5);
						$this->SetX($x);
						$this->Cell('23','10',$totalContWorkers,'','0','R');
						$this->SetY($y);
						$this->SetX($x+23);
				
				$this->Cell('23',10,$totalWage,'','','R');
						$x=$this->GetX()-23;
						$y=$this->GetY();
						$this->Ln(5);
						$this->SetX($x);
						$this->Cell('23','10',$totalContWage,'','0','R');
						$this->SetY($y);
						$this->SetX($x+23);
				
				$this->Cell('23',10,$esiWorker,'','','R');
				$this->Cell('23',10,$esiWage,'','','R');
				$this->Cell('23',10,$wrkerShare,'','','R');
				$this->Cell('23',10,$emprShare,'','','R');
				$this->Cell('23',10,$challanAmount,'','','R');
				$this->Cell('23',10,$challanDepDate,'','','R');
				$this->Ln(10);
			}
		}
			$y=$this->GetY();
			$this->SetY($y-5);	
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(10);
		
	}
	
	}
	$pdf = new PDF();
	$pdf->SetFont('Arial','',10);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->topheading();
	$pdf->show_fields();
	$pdf->show_data();
	$pdf->Output();
	
?>