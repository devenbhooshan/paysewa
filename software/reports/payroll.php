<?php
require('../Classes/PHPPdf/fpdf.php');
require("../dbinfo.php"); // requires
class PDF extends FPDF
{

public $year="2013";
public $month;
public $clientid; //class variables

function init(){
	if(array_key_exists('year',$_GET))
	$this->year=$_REQUEST['year'];
	else $this->year="2013";
	
	if(array_key_exists('month',$_GET))
	$this->month=$_REQUEST['month'];
	else $this->month=1;
	
	if(array_key_exists('clientId',$_GET))
	$this->clientid=$_REQUEST['clientId'];
	else $this->clientid=1;
} // init
function show_data(){
	$query_for_id=mysql_query("select id,workerId from transaction_master where clientId='$this->clientid' and month='$this->month' and year='$this->year'");
	if(mysql_num_rows($query_for_id)>0){
			$slno=1;
			$bottomY;
		while($row_for_id=mysql_fetch_array($query_for_id)){
			$id=$row_for_id['id'];
			$workerid=$row_for_id['workerId'];
			$topx=$this->GetX();
			$topY=$this->GetY(); //finding top x,y
			$top_1_width=14;
			$top_1_height=5;
			$this->Cell($top_1_width,$top_1_height,$slno,'',1); // Serial Number
			$this->Ln();
			$this->Cell($top_1_width,$top_1_height,'');      // &
			$this->Ln();
			$this->Cell($top_1_width,$top_1_height,$workerid);      // Code
			//$this->Ln(1); //Serial number and code
					 
			$top_2_width;
			$top_2_height;
			$margin;
			$padding=0;
			$query_for_particulars=mysql_query("select name,fName,esiNo,pfNo,doj from workerdetail where id='$workerid'");
				if(mysql_num_rows($query_for_particulars)>0){
					$row_for_particulars=mysql_fetch_array($query_for_particulars);	
					$name=$row_for_particulars['name'];
					$fname=$row_for_particulars['fName'];
					$esino=$row_for_particulars['esiNo'];
					$pfno=$row_for_particulars['pfNo'];
					$doj=$row_for_particulars['doj'];
					$this->SetXY($topx+$top_1_width,$topY);
					$top_2_width=38;
					$top_2_height=$top_1_height;
					$this->Cell($top_2_width,$top_2_height,$name,"",1); // Name
					$this->SetX($topx+$top_1_width+$padding);
					$this->Cell($top_2_width,$top_2_height,$fname,"",1);      // F Name
					$this->SetX($topx+$top_1_width+$padding);
					$this->Cell($top_2_width,$top_2_height,'XXXXX',"",1);      // NOW
					$this->SetX($topx+$top_1_width+$padding);
					$this->Cell($top_2_width,$top_2_height,$esino,"",1);      // I Numb
					$this->SetX($topx+$top_1_width+$padding);
					$this->Cell($top_2_width,$top_2_height,$pfno."/".$doj);      // PF No/JDate
					$margin=$topx+$top_1_width+$top_2_width+$padding;
					$bottomY=$this->GetY();
					 	
				}
			 //Particulares
			$top_3_height;
			$top_3_width;
			$query_for_days=mysql_query("select dw,fhd,wh,tpd from transaction_master where id='$id'");
			if(mysql_num_rows($query_for_days)>0){
				$row_for_days=mysql_fetch_array($query_for_days);
				$dw=$row_for_days['dw'];
				$fhd=$row_for_days['fhd'];
				$wh=$row_for_days['wh'];
				$tpd=$row_for_days['tpd'];
				$this->SetXY($margin,$topY);
				$top_3_width=8;
				$top_3_height=$top_1_height;
				$this->Cell($top_3_width,$top_3_height,$dw,"",1); // DW
				$this->SetX($margin);
				$this->Cell($top_3_width,$top_3_height,$fhd,"",1);      // FHD
				$this->SetX($margin);
				$this->Cell($top_3_width,$top_3_height,$wh,"",1);      // WHD
				$this->SetX($margin);
				$this->Cell($top_3_width,$top_3_height,$tpd,'',1);      // TPD
				$this->SetX($margin);
			} // days
					
			$query_leave_type=mysql_query("select shortName as leave_type from cleavedetails where  clientId='$this->clientid' and yn='Y'");
			while($row=mysql_fetch_array($query_leave_type)){
				$leave=$row['leave_type'];
				$query_for_leave_value=mysql_query("select $leave from transaction_master where id='$id'");
				$row_leave_value=mysql_fetch_array($query_for_leave_value);
				$leave_value=$row_leave_value[$leave];
				$this->Cell($top_3_width,$top_3_height,$leave_value,"",0,1);      
				$this->SetX($margin);
				
			}
			$margin=$margin+$top_3_width;
			$bottomY=$this->GetY()>$bottomY?$this->GetY():$bottomY; // leave
			
			$this->SetXY($margin,$topY);
			$top_4_width=17;
			$top_4_height=$top_1_height;
			$query_leave_type=mysql_query("select name as leave_type from callowancedetail where  clientId='$this->clientid' order by name asc");
			while($row=mysql_fetch_array($query_leave_type)){
				$name=$row['leave_type'];
				$query_for_rate=mysql_query("select rate as $name  from wwagestrudynamic where workerId='$workerid' and name='$name'");
				
				$row_for_rate=mysql_fetch_array($query_for_rate);
				$rate=$row_for_rate[$name];
				$this->Cell($top_4_width,$top_4_height,$rate,"",1);      
				$this->SetX($margin);
			}
			$bottomY=$this->GetY()>$bottomY?$this->GetY():$bottomY;
			$margin=$margin+$top_4_width;// rates
			
			
			$this->SetXY($margin,$topY);
			$top_5_width=17;
			$top_5_height=$top_1_height;
			$query_leave_type=mysql_query("select name as leave_type from callowancedetail where  clientId='$this->clientid' order by name asc");
			while($row=mysql_fetch_array($query_leave_type)){
				$name=$row['leave_type'];
				$query_for_rate=mysql_query("select $name from transaction_master where id='$id'");
				
				$row_for_rate=mysql_fetch_array($query_for_rate);
				$rate=$row_for_rate[$name];
				$this->Cell($top_5_width,$top_5_height,$rate,"",1);      
				$this->SetX($margin);
			}
			$bottomY=$this->GetY()>$bottomY?$this->GetY():$bottomY; //earn
			
			$margin=$margin+$top_5_width;
			
			$this->SetXY($margin,$topY);
			$top_6_width=17;
			$top_6_height=$top_1_height;
			$query_leave_type=mysql_query("select name as leave_type from callowancedetail where  clientId='$this->clientid' order by name asc");
			while($row=mysql_fetch_array($query_leave_type)){
				$name=$row['leave_type']."AREAR";
				$query_for_rate=mysql_query("select $name from transaction_master where id='$id'");
				
				$row_for_rate=mysql_fetch_array($query_for_rate);
				$rate=$row_for_rate[$name];
				$this->Cell($top_6_width,$top_6_height,$rate,"",1);      
				$this->SetX($margin);
			}
			$bottomY=$this->GetY()>$bottomY?$this->GetY():$bottomY; // arear
			
			$query_for_pi_tax=mysql_query("select PI,oTax,EL from transaction_master where id='$id'");
			if(mysql_num_rows($query_for_pi_tax)>0){
			$row_for_pi_tax=mysql_fetch_array($query_for_pi_tax);
			$pi=$row_for_pi_tax['PI'];
			$otax=$row_for_pi_tax['oTax'];
			$el=$row_for_pi_tax['EL'];	
			$margin=$margin+$top_6_width;
			$this->SetXY($margin,$topY);
			$top_7_width=15;
			$top_7_height=$top_1_height;
			$this->Cell($top_7_width,$top_7_height,$pi,"",1); // Name
			$this->SetX($margin);
			$this->Cell($top_7_width,$top_7_height,$otax,"",1); // Name
			$this->SetX($margin);
			$this->Cell($top_7_width,$top_7_height,$el,"",1); // Name
			$this->SetX($margin);
			$this->Cell($top_7_width,$top_7_height,'EL Wage',"",1); // Name
			$this->SetX($margin);
		   	$this->Cell($top_7_width,$top_7_height,'T.AMT',"",1); // Name
			$this->SetX($margin);
	
			
			$margin=$margin+$top_7_width;
			} //pi, ot, el days
			$query_for_ded1=mysql_query("select esiWage,epfWage,esiContriWorker,epfContriWorker,pensionByEmployer from transaction_master where id='$id'");
			if(mysql_num_rows($query_for_ded1)>0){	
			$row_for_ded1=mysql_fetch_array($query_for_ded1);
			$esiWage=$row_for_ded1['esiWage'];
			$epfWage=$row_for_ded1['epfWage'];
			$esiCW=$row_for_ded1['esiContriWorker'];
			$epfCW=$row_for_ded1['epfContriWorker'];
			$pen=$row_for_ded1['pensionByEmployer'];
			
			$this->SetXY($margin,$topY);
			$top_8_width=15;
			$top_8_height=$top_1_height;
			$this->Cell($top_8_width,$top_8_height,$esiWage,"",1); // Name
			$this->SetX($margin);
			$this->Cell($top_8_width,$top_8_height,$esiCW,"",1); // Name
			
			$this->SetX($margin);
			$this->Cell($top_8_width,$top_8_height,$epfWage,"",1); // Name
			$this->SetX($margin);
			$this->Cell($top_8_width,$top_8_height,$epfCW,"",1); // Name
			$this->SetX($margin);
			if(strlen($pen)==0 ||$pen==null)
			$this->Cell($top_8_width,$top_8_height,'N',"",1); // Name
			else $this->Cell($top_8_width,$top_8_height,'Y',"",1); 
			$this->SetXY($margin+$top_8_width,$topY);
			} // ded 1
			
			$deduction_type=array('advYN','dedYN','tdsYN','canteenYN','loanYN','otherded1YN','otherded2YN');
			$len=sizeof($deduction_type);
			$i=0;
			$ded_total=0;	
			while($i<$len){
				$ded=$deduction_type[$i];
				
				$querry=mysql_query("select id from clientdetails where id=(select clientId from workerdetail where id='$workerid')  and $ded='1'");
				if(mysql_num_rows($querry)>0){
					$ded=strtoupper(substr($ded,0,-2));
					$query_for_ded=mysql_query("select $ded from transaction_master where id='$id'");
					$row_for_ded2=mysql_fetch_array($query_for_ded);
					$ded_value=$row_for_ded2[$ded];
					$ded_total+=$ded_value;
					$this->Cell(18,$top_1_height,$ded_value,"",1);      
					$this->SetX($margin+$top_8_width);	
					}
			$i++;
			
			}
			$this->Cell(18,$top_1_height,$ded_total,"",0);
			$bottomY=($this->GetY())>$bottomY?$this->GetY():$bottomY;
			$top_8_width=33;
			$this->SetXY(0,$bottomY-5);
			$this->Ln(1);
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','1'); // line break
			$this->Ln(1);
			$slno++; // line break and increament of slno
			
		}
		
		//Total
		$this->SetFont('Arial','B','10');
		$this->Cell(14,10,'Totals');
		$this->SetFont('Arial','','8');
		$this->Cell(38,10,'');
		$this->Cell(8,10,'D','','','R'); // dw
		$this->Cell(17,10,'D','','','R');
		
		$this->Cell(17,10,'D','','','R'); // Earn 
		$this->Cell(17,10,'D','','','R'); // Arear
		$this->Cell(15,10,'D','','','R'); // PI OT
		$this->Cell(15,10,'D','','','R'); // ESI WAGE
		$this->Cell(15,10,'Dss','','','R'); // ADV
		$this->Cell(17,10,'D','','','R'); // dw
		
		
	}
	
	
}

//columns

function leave($m,$y,$id,$w,$h,$marg){
 $query_leave_type=mysql_query("select shortName as leave_type from cleavedetails where  clientId='$id' and yn='Y'");
	while($row=mysql_fetch_array($query_leave_type)){
		$name=$row['leave_type'];
		$this->Cell($w,$h,$name,"",0,'R');      
		$this->SetX($marg);
	}
	return $this->GetY();
} // leave

function show_rates_column($m,$y,$id,$w,$h,$marg){
 $query_leave_type=mysql_query("select name as leave_type from callowancedetail where  clientId='$id' order by name asc");
	while($row=mysql_fetch_array($query_leave_type)){
		$name=$row['leave_type'];
		$this->Cell($w,$h,$name,"",1,'R');      
		$this->SetX($marg);
	}
	return $this->GetY();
} // rate
function show_earn_column($m,$y,$id,$w,$h,$marg){
 $query_leave_type=mysql_query("select name as leave_type from callowancedetail where  clientId='$id' order by name asc");
	while($row=mysql_fetch_array($query_leave_type)){
		$name=$row['leave_type'];
		$this->Cell($w,$h,$name,"",1,'R');      
		$this->SetX($marg);
	}
	return $this->GetY();
} // earn
function show_arear_column($m,$y,$id,$w,$h,$marg){
 $query_leave_type=mysql_query("select name as leave_type from callowancedetail where  clientId='$id' order by name asc");
	while($row=mysql_fetch_array($query_leave_type)){
		$name=$row['leave_type'];
		$this->Cell($w,$h,$name,"",1,'R');      
		$this->SetX($marg);
	}
	return $this->GetY();
} // arear


function show_deduction_column($m,$y,$id,$w,$h,$marg){
$deduction_type=array('advYN','dedYN','tdsYN','canteenYN','loanYN','otherded1YN','otherded2YN');
$len=sizeof($deduction_type);
$i=0;
while($i<$len){
	$ded=$deduction_type[$i];
	$querry=mysql_query("select id from clientdetails where id='$id' and $ded='1'");
	if(mysql_num_rows($querry)>0){
		
		$ded=strtoupper(substr($ded,0,-2));
		$this->Cell(18,$h,$ded,"",1,'R');      
		$this->SetX($marg);	
	}
			

	$i++;
}

$this->Cell(18,$h,"TOTAL","",1,'R');

	return $this->GetY();
}



function Footer()
{
	$this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
} //footer

function heading(){
	$month=array('','JAN','FEB','MAR','APR','MAY','JUNE','JULY','AUG','SEPT','OCT','NOV','DEC');
	$companyname="Company Name Here";
	$query=mysql_query("select name from clientdetails where id='$this->clientid'");
	if(mysql_num_rows($query)>0){
		$row=mysql_fetch_array($query);
		
		$companyname=$row['name'];	
		if(strlen($companyname)>30){

			$companyname=substr($companyname,0,30)."...";	
		}
	}
	
	$this->SetXY(0,0);
	$this->Cell(0,10,"-",0,0);
	$this->Ln();
	
	$this->SetFont('Arial','B',20);
	$this->Cell(128,10,$companyname,0,0); // company name + date 
	$this->SetFont('Arial','',15);
	$this->Cell(20,10,$month[$this->month],0,0);
	$this->Cell(0,10,$this->year,0,0);
	
	$this->Ln(1);
		
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(10);
	
	
}
 // heading



function topheading()
{
	$height_of_the_pdf_file=$this->h;
	$weight_of_the_pdf_file=$this->w;
	$cor_y_bottom;
	$padding=0;
	
	 //pdf file height,width
	$this->heading(); // top heading
	
	
	$this->SetFont('Arial','B',8);
	$this->Cell(14,10,'','',0);
	$this->Cell(38,10,'PARTICULAR','',0);
	$this->Cell(8,10,'Days','',0,'R');
	$this->Cell(17,10,'--RATES--','',0,'R');
	$this->Cell(17,10,'--EARN--','',0,'R');
	$this->Cell(17,10,'--AREAR--','',0,'R');
	$this->Cell(15,10,'','',0);
	$this->Cell(27,10,'DEDUCTION','',0,'R');
	$this->Cell(17,10,'Bank A/C','',0,'R');
	$this->Cell(30,10,'DATE OF PAYMENT','',0);
	$this->Ln(7); // topest column
	
	
	
	$font_column_name=8;
	$cor_x_top=$this->GetX();
	$cor_y_top=$this->GetY();
	$this->SetFont('Arial','',$font_column_name);
	
	$top_1_width=14;
	$top_1_height=5;
	
	$this->Cell($top_1_width,$top_1_height,'S.NO.'); // Serial Number
	$this->Ln();
	$this->Ln();
	
	$this->Cell($top_1_width,$top_1_height,'&');      // &
	$this->Ln();
	
	$this->Cell($top_1_width,$top_1_height,'CODE');      // Code
	$this->Ln();
	 // serial number ,code

	
	
		
	$this->SetXY($cor_x_top+$top_1_width+$padding,$cor_y_top);
	$top_2_width=38;
	$top_2_height=$top_1_height;
	
	
	$this->Cell($top_2_width,$top_2_height,'NAME OF THE EMPLOYEE',"",1); // Name
	$this->SetX($cor_x_top+$top_1_width+$padding);
	$this->Cell($top_2_width,$top_2_height,'FATHER NAME',"",1);      // F Name
 	$this->SetX($cor_x_top+$top_1_width+$padding);
	$this->Cell($top_2_width,$top_2_height,'NATURE OF WORK',"",1);      // NOW
	$this->SetX($cor_x_top+$top_1_width+$padding);
	$this->Cell($top_2_width,$top_2_height,'INSURANCE NUMBER',"",1);      // I Numb
	$this->SetX($cor_x_top+$top_1_width+$padding);
	$this->Cell($top_2_width,$top_2_height,'PF NO/JDATE');      // PF No/JDate
	
	 
	 
	 
	 //Particulars
	 $margin=$cor_x_top+$top_1_width+$top_2_width+$padding;
	
	$this->SetXY($margin,$cor_y_top);
	$top_3_width=8;
	$top_3_height=$top_1_height;
	
	
	$this->Cell($top_3_width,$top_3_height,'DW',"",1,'R'); // DW
	$this->SetX($margin);
	$this->Cell($top_3_width,$top_3_height,'FHD',"",1,'R');      // FHD
	$this->SetX($margin);
	$this->Cell($top_3_width,$top_3_height,'WHD',"",1,'R');      // WHD
	$this->SetX($margin);
	$this->Cell($top_3_width,$top_3_height,'TPD','',1,'R');      // TPD
	$this->SetX($margin);
	$cor_y_bottom=$this->leave($this->month,$this->year,$this->clientid,$top_3_width,$top_3_height,$margin);
	 
	 
	 
	 //Rates
	$margin=$margin+$top_3_width;
	
	$this->SetXY($margin,$cor_y_top);
	$top_4_width=17;
	$top_4_height=$top_1_height;
	
	
	$temp=$this->show_rates_column($this->month,$this->year,$this->clientid,$top_4_width,$top_4_height,$margin);
	$cor_y_bottom=($temp)>$cor_y_bottom?$temp:$cor_y_bottom;
	
	
	
	
	
	//Earn
	$margin=$margin+$top_4_width;
	
	$this->SetXY($margin,$cor_y_top);
	$top_5_width=17;
	$top_5_height=$top_1_height;
	$temp=$this->show_earn_column($this->month,$this->year,$this->clientid,$top_5_width,$top_5_height,$margin);
	$cor_y_bottom=($temp)>$cor_y_bottom?$temp:$cor_y_bottom;
	
		
		
	//Arears
	$margin=$margin+$top_5_width;
	
	$this->SetXY($margin,$cor_y_top);
	$top_6_width=17;
	$top_6_height=$top_1_height;
	
	
	$temp=$this->show_arear_column($this->month,$this->year,$this->clientid,$top_6_width,$top_6_height,$margin);

	$cor_y_bottom=($temp)>$cor_y_bottom?$temp:$cor_y_bottom;
	
	
	// total amount+PI+OT
	$margin=$margin+$top_6_width;
	
	$this->SetXY($margin,$cor_y_top);
	$top_7_width=15;
	$top_7_height=$top_1_height;
	
	
	$this->Cell($top_7_width,$top_7_height,'PI',"",1,'R'); // Name
	$this->SetX($margin);
	$this->Cell($top_7_width,$top_7_height,'OT',"",1,'R'); // Name
	$this->SetX($margin);
	$this->Cell($top_7_width,$top_7_height,'EL Days',"",1,'R'); // Name
	$this->SetX($margin);
	$this->Cell($top_7_width,$top_7_height,'EL Wage',"",1,'R'); // Name
	$this->SetX($margin);
   	$this->Cell($top_7_width,$top_7_height,'T.AMT',"",1,'R'); // Name
	$this->SetX($margin);
	
	// Deductions
	$margin=$margin+$top_7_width;
	
	$this->SetXY($margin,$cor_y_top);
	$top_8_width=15;
	$top_8_height=$top_1_height;
	
	
	$this->Cell($top_8_width,$top_8_height,'ESI Wage',"",1,'R'); // Name
	$this->SetX($margin);
	$this->Cell($top_8_width,$top_8_height,'E.S.I.',"",1,'R'); // Name	
	$this->SetX($margin);
	$this->Cell($top_8_width,$top_8_height,'EPF Wage',"",1,'R'); // Name

	$this->SetX($margin);
	$this->Cell($top_8_width,$top_8_height,'P.F.',"",1,'R'); // Name
	$this->SetX($margin);
	$this->Cell($top_8_width,$top_8_height,'E.P.S.',"",1,'R'); // Name
	$this->SetXY($margin+$top_8_width,$cor_y_top);

	
	$temp=$this->show_deduction_column($this->month,$this->year,$this->clientid,$top_8_width,$top_8_height,$margin+$top_8_width);
	$cor_y_bottom=($temp)>$cor_y_bottom?$temp:$cor_y_bottom;
	
	$top_8_width=33;
	
	
	//net payable
	$margin=$margin+$top_8_width;
	
	$this->SetXY($margin,$cor_y_top);
	$top_9_width=15;
	$top_9_height=$top_1_height;
	
	
	$this->Cell($top_9_width,$top_9_height,'',"",1); // Name
	$this->SetX($margin);
	$this->Cell($top_9_width,$top_9_height,'',"",1); // Name
	$this->SetX($margin);
	$this->Cell($top_9_width,$top_9_height,'',"",1); // Name
	$this->SetX($margin);
	$this->Cell($top_9_width,$top_9_height,'NET',"",1); // Name
	$this->SetX($margin);
	$this->Cell($top_9_width,$top_9_height,'PAYABLE',"",1); // Name
	
	$cor_y_bottom=$cor_y_bottom<$this->GetY()?$this->GetY():$cor_y_bottom;
	$this->SetX($margin);
	
	// Date of Payement and Signature
	$margin=$margin+$top_9_width;
	$this->SetXY($margin,$cor_y_top);
	$top_10_width=30;
	$top_10_height=$top_1_height;
	$this->Cell($top_10_width,$top_10_height,'',"",1); // Name
	
	
	//line breaks 
	
	$this->SetXY(0,$cor_y_bottom-10);
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(15);
}
		
		
}


$pdf = new PDF();
$header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
$pdf->init();
$pdf->SetFont('Arial','',10);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->topheading();
$pdf->show_data();


if(array_key_exists('request',$_GET)){
$query=mysql_query("select path from payroll_file_path where clientId='$pdf->clientid'");
if(mysql_num_rows($query)>0){
	$row=mysql_fetch_array($query);
	$path=$row['path'];	
}
else {
	$query=mysql_query("select name from clientdetails where id='$pdf->clientid'");
	$row=mysql_fetch_array($query);
	$name=$row['name'];
	$directory_path="data/".$name."_".$pdf->clientid."/"."PAYROLL/";
	$directory_created=mkdir($directory_path,0777,true);
	if($directory_created==false)
	{
	//echo "Something bad happen";
	//	return;
	}
	$path=$directory_path;
	mysql_query("insert into payroll_file_path(clientId,path)values('$pdf->clientid','$path') ");	
	
}
//$request=$_REQUEST['request'];
$pdf->Output($path.$pdf->month."_".$pdf->year.".pdf");
echo $path.$pdf->month."_".$pdf->year.".pdf";
			
		
}
else 
$pdf->Output();


?>