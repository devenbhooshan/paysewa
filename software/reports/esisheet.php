<?php
require('../Classes/PHPPdf/fpdf.php');
require("../dbinfo.php"); // requires
include("core_functions.php");
class PDF extends FPDF
{

public $year="2013";
public $month;
public $clientid; //class variables

function init(){
	if(array_key_exists('year',$_GET))
	$this->year=$_REQUEST['year'];
	else $this->year="2013";
	
	if(array_key_exists('month',$_GET))
	$this->month=$_REQUEST['month'];
	else $this->month=1;
	
	if(array_key_exists('clientId',$_GET))
	$this->clientid=$_REQUEST['clientId'];
	else $this->clientid=1;
} // init
function show_data(){
	$query_for_id=mysql_query("select id,workerId from transaction_master where clientId='$this->clientid' and month='$this->month' and year='$this->year' " );
	if(mysql_num_rows($query_for_id)>0){
			$slno=1;
			$bottomY;
			$totaldays=0;
			$totalesiapp=0;
			$totalwage=0;
			$totlaesicontri=0;
			$total_esi_exempted_wage=0;
			$totalndays=0;
			
		while($row_for_id=mysql_fetch_array($query_for_id)){
			$id=$row_for_id['id'];
			$workerid=$row_for_id['workerId'];
			$topx=$this->GetX();
				$this->SetFont('Arial','',8);	
			$topY=$this->GetY(); //finding top x,y
			
			$query_for_particulars=mysql_query("select name,fName,esiNo,pfNo from workerdetail where id='$workerid'");
				if(mysql_num_rows($query_for_particulars)>0){
					$row_for_particulars=mysql_fetch_array($query_for_particulars);	
					$name=ucfirst(strtolower($row_for_particulars['name']));
					if(strlen($name)>20)
						$name=substr($name,0,20)."..";
								
					$fname=ucfirst(strtolower($row_for_particulars['fName']));
					if(strlen($fname)>20)
						$fname=substr($fname,0,20)."..";
					$esino=$row_for_particulars['esiNo'];
					$pfno=$row_for_particulars['pfNo'];
					$query_for_details=mysql_query("select esiReasonCode,statusEsiYN,tpd,esiContriWorker,lastdatesi,joinDate,esiWage from  transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and workerId='$workerid'");
					if(mysql_num_rows($query_for_details)>0){
						$row_for_details=mysql_fetch_array($query_for_details);
						$tpd=$row_for_details['tpd'];
						$esicon=$row_for_details['esiContriWorker'];
						$lastdate=$row_for_details['lastdatesi'];
						$doj=$row_for_details['joinDate'];
						$esiwage=$row_for_details['esiWage'];
						$esiyn=$row_for_details['statusEsiYN'];
						$esireasoncode=$row_for_details['esiReasonCode'];
						if($esiyn==1){ $totalesiapp++;
						$totaldays+=$tpd;
					$totalwage+=$esiwage;
					$totlaesicontri+=$esicon;
					
						$this->Cell(10,10,$slno,'',0); // Serial Number
					$this->Cell(11,10,$workerid,'',0);      // Code
		
					$this->Cell(38,10,$name,"",0); // Name

					$this->Cell(30,10,$fname,"",0);      // F Name

					$this->Cell(22,10,$esino,"",0);      // I Numb
						$this->Cell(10,10,$tpd,"",0,'R');
					$this->Cell(17,10,$esiwage,'',0,'R');
					$this->Cell(15,10,$esicon,'',0,'R');
					$this->Cell(8,10,$esireasoncode,'',0,'R');
					$this->Cell(16,10,$lastdate,'',0,'R');
						if(strlen($doj)==10){
						if(strcmp($doj{3},'0')==0){
							$doj_month=$doj{4};
						}
						else 
					$doj_month=$doj{3}.$doj{4};
					$doj_year=$doj{6}.$doj{7}.$doj{8}.$doj{9};
					//echo $doj." ".$doj_month." ".$doj_year."<br>";
					if(strcmp($doj_month,$this->month)==0 && strcmp($doj_year,$this->year)==0)
					$this->Cell(17,10,$doj,'',0,'R');
					}
					$this->Ln(5);
				
						}
						else { $totalndays+=$tpd;
								$total_esi_exempted_wage+=$esiwage;
						}
					}
					//echo strlen($doj)."<br>";
				
					 	
				}
			
			$slno++; // line break and increament of slno
		}
		
			$this->Cell(195,3,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,3,'','B','1'); // line break
			$this->Ln(1);
		$this->SetFont('Arial','B',7);	
		$this->Cell(20,10,$totalesiapp,'',0);
		$this->Cell(89+2,10,strtoupper('<    NO of Emp (  ESI APPLICABLE  )  DETAILS   >'),'','0');
		$this->Cell(10,10,$totaldays,'','0','R'); 	
		$this->Cell(17,10,$totalwage,'','0','R'); 	
		$this->Cell(15,10,$totlaesicontri,'','0','R'); 	
				$this->Ln(5);
		$this->Cell(20,10,($slno-$totalesiapp-1),'',0);		
		$this->Cell(89+2,10,strtoupper('<    NO of Emp (  ESI APPLICABLE  )  DETAILS   >'),'','0');
		$this->Cell(10,10,$totalndays,'','0','R'); 	
		$this->Cell(17,10,$total_esi_exempted_wage,'','0','R'); 	 // something to be done here
		$this->Cell(15,10,'0','','0','R'); 		
				$this->Ln(5);
		$this->Cell(20,10,($slno-1),'',0);				
		$this->Cell(89+2,10,strtoupper('Employer Contribution'),0,'');
		$this->Cell(10,'10','','','','');
		$this->Cell(17,'10','','','','');
		$query_for_esi_rule=mysql_query("select emprShare from govtesirule order by id");
		if(mysql_num_rows($query_for_esi_rule)>0){
			$row_esiempshare=mysql_fetch_array($query_for_esi_rule);
			$esiempshare=$row_esiempshare['emprShare'];
				
		}
		
		$emplcontri=($totalwage*$esiempshare)/100;
		$this->Cell(15,'10',ceil($emplcontri),'','','R');
				$this->Ln(5);
				$this->Cell(20,10,'','',0);				
		$this->Cell(89+2,10,strtoupper('Grand Total/challan amount'),0,'');
		$this->Cell(10,10,$totaldays+$totalndays,'','0','R'); 		
		$this->Cell(17,10,$totalwage,'','0','R'); 	
		$this->Cell(15,10,ceil($totlaesicontri+$emplcontri),'','0','R'); 	
				$this->Ln(1);
				/*
		$this->Cell(109,10,'OTHER ADDTION/ AREAR',0,'','C');
				$this->Cell(10,10,'','','0'); 		
		$this->Cell(17,10,'','','0'); 
		$this->Cell(17,10,'AREA','','0');
		 	$this->Ln(5);
		$this->Cell(109,10,'CHALLAN AMOUNT',0,'','C');	
		//$this->Cell(10,10,'','','0'); 
		$this->Cell(10,10,$totlaesicontri,'','0'); 
		$this->Cell(5,10,'+','','0','R'); 
		$this->Cell(10,10,ceil($emplcontri),'','0','R');
		$this->Cell(5,10,'=','','0','L');
		*/
		$challan_amount=ceil($emplcontri+$totlaesicontri);
		//$this->Cell(10,10,$challan_amount,'','0','L');
		
		$query=mysql_query("select id from cresichallan where clientId='$this->clientid' and month='$this->month' and year='$this->year' ");		 
			if(mysql_num_rows($query)==0){
		$query="INSERT INTO `cresichallan`( `clientId`, `month`,year, `type`, `totalWorkers`, `totalContWorkers`, `totalWage`, `totalContWage`, `esiWorker`, `wrkerShare`, `emprShare`, `challanAmount`) VALUES ('$this->clientid','$this->month','$this->year','','$slno-1','0','$totalwage','0','$totlaesicontri','0','$emplcontri','$challan_amount')";
		
		mysql_query($query);
		
			}
		
		
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','1'); // line break
			$this->Ln(5);
			$this->Cell(20,10,'UPLOAD DATE','','0','R');	
			$this->Cell(50,10,'','B','R');	
		
			$this->Cell(20,10,'CHALLAN NUMBER','','0','R');		
			$this->Cell(50,10,'','B','R');
			
			$this->Cell(20,10,'UPLOADED BY','','0','R');		
			$this->Cell(50,10,'','B','R');
			
	}
	
	
}


function Footer()
{
	$this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
} //footer

function heading(){
	$month=array('','JAN','FEB','MAR','APR','MAY','JUNE','JULY','AUG','SEPT','OCT','NOV','DEC');
	$companyname="Company Name Here";
	$query=mysql_query("SELECT clientdetails.areaCode,clientdetails.address,clientdetails.city,clientdetails.state,clientdetails.name, cesidetail.esiNo FROM clientdetails INNER JOIN cesidetail ON cesidetail.clientId = clientdetails.id WHERE clientdetails.id ='$this->clientid'");
	//echo "SELECT clientdetails.areaCode,clientdetails.address,clientdetails.city,clientdetails.state,clientdetails.name, cesidetail.esiNo FROM clientdetails INNER JOIN cesidetail ON cesidetail.clientId = clientdetails.id WHERE clientdetails.id ='$this->clientid'";
	if(mysql_num_rows($query)>0){
		$row=mysql_fetch_array($query);
		
		$companyname=$row['name'];	
		$esino=$row['esiNo'];
		$address=$row['address'];
		$city=$row['city'];
		$state=$row['state'];
		$areacode=$row['areaCode'];
		if(strlen($companyname)>25){

			$companyname=(substr($companyname,0,25)."...");	
		}
		if(strlen($address)>25) $address=substr($address,0,25)."...";
	}
	//
	
	$this->SetXY(0,0);
	$this->Cell(0,10,"-",0,0);
	$this->Ln(2);
	
	$this->SetFont('Arial','',10);
	$this->Cell(60,10,strtoupper($companyname),0,0); // company name + date 
	$this->Cell(60,10,'',0,0);
	
	$this->Ln(4);
	
	$this->Cell(60,10,strtoupper($address).",",0,0);
	$this->Cell(90,10,'FORM 7',0,0,'C');
	$this->Cell(60,10,'ESI NO :- '.$esino,0,0);
	$this->Ln(4);
	$query_for_city_name=mysql_query("select city_name from cities where city_id='$city'");
	if(mysql_num_rows($query_for_city_name)>0){
	$row_city_name=mysql_fetch_array($query_for_city_name);
	$city_name=$row_city_name['city_name'];	
	}
	else $city_name=$city;
	$this->Cell(60,10,strtoupper($city_name).",",0,0);
	$this->Cell(90,10,'REGISTER FROM EMPLOYEES',0,0,'C');
	$query_for_area=mysql_query("select area_name from carea_code where area_code='$areacode'");
	if(mysql_num_rows($query_for_area)>0){
		$row_code=mysql_fetch_array($query_for_area);
		$areaname=$row_code['area_name'];	
	}
	else $areaname="INPUT AREA";
	$this->Cell(60,10,'AREA   :- '.strtoupper($areaname),0,0);
	$this->Ln(4);
	$this->Cell(60,10,strtoupper($state),0,0);
	$this->Cell(90,10,"(REGULATION 32)",0,0,'C');
	$this->Cell(10,10,$month[$this->month],0,0);
	$this->Cell(0,10,$this->year,0,0);
	
	$this->Ln(1);
		
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(10);
	
	
}
 // heading



function topheading()
{
	$height_of_the_pdf_file=$this->h;
	$weight_of_the_pdf_file=$this->w;
	$cor_y_bottom;
	$padding=0;
	
	 //pdf file height,width
	$this->heading(); // top heading
	
	$top_1_width=14;
	$top_1_height=5;
	
	$this->SetFont('Arial','B',8);	
	$this->Cell(10,10,'S.NO.','',0); // Serial Number

	$this->Cell(11,10,'CODE','',0);
	$this->Cell(38,10,'NAME OF EMPLOYEE','',0);
	$this->Cell(30,10,'FATHER'."'".'S NAME','',0);
	$this->Cell(22,10,'INSUR NO ','',0);
	$this->Cell(10,10,'DAYS','',0,'R');
	$this->Cell(17,10,'WAGES','',0,'R');
	$this->Cell(15,10,'ESI','',0,'R');
	$this->Cell(8,10,'Resn','',0);
	$this->Cell(17,10,'L_DATE','',0);
	$this->Cell(17,10,'J_DATE','',0);
	$this->Ln(1);
		
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(10);
	
	
}
		
		
}


$pdf = new PDF();

$pdf->init();
$pdf->SetFont('Arial','',10);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->topheading();

$pdf->show_data();


$query_for_id=mysql_query("select id from transaction_master where clientId='$pdf->clientid' and month='$pdf->month' and year='$pdf->year'");
if(mysql_num_rows($query_for_id)>0){
$row_id=mysql_fetch_array($query_for_id);
$id=$row_id['id'];
$filename=$pdf->month."_".$pdf->year.".xlsx";
make_mc_template($filename,$id);
if(array_key_exists('request',$_GET)){
$query=mysql_query("select path from esi_file_path where clientId='$pdf->clientid' and type='4'");
if(mysql_num_rows($query)>0){
	$row=mysql_fetch_array($query);
	$path=$row['path'];	
}
else {
	$query=mysql_query("select name from clientdetails where id='$pdf->clientid'");
	$row=mysql_fetch_array($query);
	$name=$row['name'];
	$directory_path="data/".$name."_".$pdf->clientid."/"."ESI/";
	$directory_created=mkdir($directory_path."REPORT",0777,true);
	if($directory_created==false)
	{
	echo "Something bad happen";
	//	return;
	}
	$path=$directory_path."REPORT/";
	mysql_query("insert into esi_file_path(clientId,path,type)values('$pdf->clientid','$path','4') ");	
	
}
$request=$_REQUEST['request'];
$pdf->Output($path.$pdf->month."_".$pdf->year.".pdf");
echo $path.$pdf->month."_".$pdf->year.".pdf";



}
else 
$pdf->Output();
}
?>