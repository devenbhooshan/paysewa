<?php
require('../Classes/PHPPdf/fpdf.php');
require("../dbinfo.php"); // requires
require("core_functions.php");

class PDF extends FPDF
{

public $year="2013";
public $month;
public $clientid; //class variables

function init(){
	if(array_key_exists('year',$_GET))
	$this->year=$_REQUEST['year'];
	else $this->year="2013";
	
	if(array_key_exists('month',$_GET))
	$this->month=$_REQUEST['month'];
	else $this->month=1;
	
	if(array_key_exists('clientId',$_GET))
	$this->clientid=$_REQUEST['clientId'];
	else $this->clientid=1;
} // init
function show_data(){
	$query_for_id=mysql_query("select workerdetail.pfNo,transaction_master.id,transaction_master.workerId from transaction_master inner join workerdetail  on workerdetail.id=transaction_master.workerId where transaction_master.clientId='$this->clientid' and transaction_master.month='$this->month' and transaction_master.year='$this->year' order by workerdetail.pfNo asc");
	//echo "select workerdetail.pfNo,transaction_master.id,transaction_master.workerId from transaction_master inner join workerdetail  on workerdetail.id=transaction_detail.workerId where clientId='$this->clientid' and month='$this->month' and year='$this->year' order by workerdetail.pfNo asc";
	if(mysql_num_rows($query_for_id)>0){
			$slno=0;
			$bottomY;
			$total_gross=0;
			$total_pf=0;
			$total_fpf=0;
			$total_pns_n_app=0;
			$total_break_days=0;
			$no_of_pf_app=0;
			$total_left_member=0;
			$total_epfWageArear=0;
			$total_epfWrArear=0;
			$total_epsEmpArear=0;
			$total_epfWrArear_wage=0;
			$total_epsEmpArear_wage=0;
						
		while($row_for_id=mysql_fetch_array($query_for_id)){
			$id=$row_for_id['id'];
			$workerid=$row_for_id['workerId'];
			$topx=$this->GetX();
				$this->SetFont('Arial','',8);	
			$topY=$this->GetY(); //finding top x,y
			
			$query_for_particulars=mysql_query("select name,fName,esiNo,pfNo from workerdetail where id='$workerid'");
				if(mysql_num_rows($query_for_particulars)>0){
					$row_for_particulars=mysql_fetch_array($query_for_particulars);	
					$name=$row_for_particulars['name'];
					$fname=$row_for_particulars['fName'];
					$esino=$row_for_particulars['esiNo'];
					$pfno=$row_for_particulars['pfNo'];




					$query_for_details=mysql_query("select statusPnsYN,mDays,statusEpfYN,tpd,breakDay,epfContriWorker,lastdateepf,joinDate,epfWage,pfReasonCode,epfWageArear,epfWrArear,epsEmpArear,epfEmpArear from  transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and workerId='$workerid'");
				
					if(mysql_num_rows($query_for_details)>0){
						//echo "select mDays,statusEpfYN,tpd,epfContriWorker,lastdateepf,joinDate,epfWage,pfReasonCode from  transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and workerId='$workerid'"."<br>";
						$row_for_details=mysql_fetch_array($query_for_details);
						$tpd=$row_for_details['tpd'];
						$break=$row_for_details['breakDay'];
						$epfcontriworker=$row_for_details['epfContriWorker'];
						$lastdate=$row_for_details['lastdateepf'];
						$doj=$row_for_details['joinDate'];
						$epfwage=$row_for_details['epfWage'];
						$epfyn=$row_for_details['statusEpfYN'];
						$pnsyn=$row_for_details['statusPnsYN'];
						$epfWageArear=$row_for_details['epfWageArear'];
						if($epfyn==1) {
							$slno++;
							$no_of_pf_app++;
											
					$this->Cell(10,10,$slno,'',0); // Serial Number
					$this->Cell(11,10,$workerid,'',0);      // Code
					$this->Cell(38,10,$name,"",0); // Name
					$this->Cell(16,10,$doj,'',0);
					$this->Cell(16,10,$lastdate,'',0);
					$this->Cell(20,10,$pfno,'',0);
					$this->Cell(16,10,$epfwage,'',0,'R');
					$this->Cell(16,10,$epfcontriworker,'',0,'R');
					
						
						

						$pfreasoncode=$row_for_details['pfReasonCode'];
						$mDays=$row_for_details['mDays'];

						if(strcmp($pfreasoncode,"-8")==0){
							
							$pfreasoncode='';
						}else $total_left_member++;

					
					if($pnsyn==1){
					$query_for_fpf_percentage=mysql_query("select AcNo10E as pf_percentage,maxLimitAc10 from govtpfrule order by id desc");
					if(mysql_num_rows($query_for_fpf_percentage)>0){
						$row_fpf_percentage=mysql_fetch_array($query_for_fpf_percentage);
						$fpf_percentage=$row_fpf_percentage['pf_percentage'];	
						$fpf=round(($fpf_percentage*$epfwage)/100);
						$maxLimitAc10=$row_fpf_percentage['maxLimitAc10'];
						$fpf=$fpf>$maxLimitAc10?$maxLimitAc10:$fpf;
						
					$this->Cell(16,10,$fpf,'',0,'R');
					}
					}
					else 
					{$fpf=0; $total_pns_n_app++;
					$this->Cell(16,10,$fpf,'',0,'R');
					}
					$this->Cell(16,10,$epfcontriworker-$fpf,'',0,'R');
					$this->Cell(7,10,$pfreasoncode,'',0);
					$this->Cell(17,10,$break,'',0);
					
					$total_gross+=$epfwage;
					$total_pf+=$epfcontriworker;
					$total_fpf+=$fpf;
					
					$total_break_days+=$break;
					
					if($epfWageArear>0){
						$this->Ln(5);
						$epfWrArear=$row_for_details['epfWrArear'];
						$epsEmpArear=$row_for_details['epsEmpArear'];
						$epfEmpArear=$row_for_details['epfEmpArear'];
						$this->Cell(21,10,'','',0);
						$this->Cell(38,10,'....DO... Arear','',0);
						$this->Cell(52,10,'','',0);
						$this->Cell(16,10,$epfWageArear,'',0,'R');
						$this->Cell(16,10,$epfWrArear,'',0,'R');
						$this->Cell(16,10,$epsEmpArear,'',0,'R');
						$this->Cell(16,10,$epfEmpArear,'',0,'R');
						$this->Cell(16,10,'','',0);
						$total_gross+=$epfWageArear;
						$query_for_max_limits=mysql_query("select maxEpfLimit,maxPnsLimit from govtpfrule order by id desc ");
						$row_for_max_limits=mysql_fetch_array($query_for_max_limits);
						$maxEpfLimit=$row_for_max_limits['maxEpfLimit'];
						$maxPnsLimit=$row_for_max_limits['maxPnsLimit'];;
						
						
						if($pnsyn==1){
						
						$total_epsEmpArear+=$epsEmpArear;
						$total_epsEmpArear_wage+=$epfWageArear>$maxEpfLimit?$maxPnsLimit:$epfWageArear;			
						}
						$total_pf+=$epfWrArear;
						$total_fpf+=$epsEmpArear;
						$total_epfWageArear+=$epfWageArear;
						$total_epfWrArear+=$epfWrArear;
						$total_epfWrArear_wage+=$epfWageArear>$maxEpfLimit?$maxEpfLimit:$epfWageArear;
						
					
					}
					
					
					
					
								$this->Ln(5);			}
 	
				}
				}
				
			 // line break and increament of slno
		}
			
			$query_for_detail_pf21_pns=mysql_query("select SUM(pf21Wage) as pf21Wage_sum,SUM(pnsWage) as pnsWage_sum from transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid'  ");
//			echo "select SUM(pf21Wage) as pf21Wage_sum,SUM(pnsWage) as pnsWage_sum from transaction master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and workerId='$workerid' ";
			$row_for_pf21_pns=mysql_fetch_array($query_for_detail_pf21_pns);
			
			
			$ac21wage=$row_for_pf21_pns['pf21Wage_sum'];
			$ac10wage=$row_for_pf21_pns['pnsWage_sum'];
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(10);
			/*$this->Cell('30',10,'A/C 21 WAGES:','','0');
			$this->Cell('25',10,$ac21wage,'','0');
			$this->Cell('30',10,'A/C 10 WAGES:','','0');
			$this->Cell('26',10,$ac10wage,'','0');
			*/
			$this->Cell('111',10,'','','0');
			$this->Cell('16',10,$total_gross,'','0','R');
			$this->Cell('16',10,$total_pf,'','0','R');
			$this->Cell('16',10,$total_fpf,'','0','R');
			$this->Cell('16',10,$total_pf-$total_fpf,'','0','R');
			$this->Cell('7',10,'','','0');
			$this->Cell('17',10,$total_break_days,'','0');
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(10);
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'A/C NO 01','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell(20,10,$no_of_pf_app,'','0','C');
			
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'A/C NO 10','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell(20,10,$slno-$total_pns_n_app-1,'','0','C');
			
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'A/C NO 21','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell(20,10,$no_of_pf_app,'','0','C');
			$this->Ln(5);
			$this->Cell(10,10,'','','0');
			$this->Cell('30',10,'A/C NO 01','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell('20',10,$total_gross-$total_epfWageArear,'','0','C');
	
			
			$this->Cell(10,10,'','','0');
			$this->Cell('30',10,'A/C 10 WAGES:','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell('20',10,$ac10wage,'','0','C');
			$this->Cell(10,10,'','','0');
			
			$this->Cell('30',10,'A/C 21 WAGES:','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell('20',10,$ac21wage,'','0','C');
			$this->Cell(10,10,'','','0');
	
			$this->Ln(5);


			$this->Cell(10,10,'','','0');
			$this->Cell('30',10,'A/C NO 01(AREAR)','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell('20',10,$total_epfWageArear,'','0','C');
	
			
			$this->Cell(10,10,'','','0');
			$this->Cell('30',10,'A/C 10 WAGES:','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell('20',10,$total_epfWrArear_wage,'','0','C');
			$this->Cell(10,10,'','','0');
			
			$this->Cell('30',10,'A/C 21 WAGES:','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell('20',10,$total_epsEmpArear_wage,'','0','C');
			$this->Cell(10,10,'','','0');
			
			$this->Ln(5);
			
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'TOTAL','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell(20,10,$total_gross,'','0','C');
			
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'TOTAL','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell(20,10,$ac10wage+$total_epfWrArear_wage,'','0','C');
			
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'TOTAL','','0');
			$this->Cell(2,10,':-','','0');
			$this->Cell(20,10,$ac21wage+$total_epsEmpArear_wage,'','0','C');
			
			
	
			$this->Ln(5);
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'NO OF (PF EXM WORKER)','','0');
			$this->Cell(2,10,'','','0');
			$this->Cell(20,10,$slno-$no_of_pf_app-1,'','0','C');
			
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'NO OF (LEFT WORKER)','','0');
			$this->Cell(2,10,'','','0');
			$this->Cell(20,10,$total_left_member,'','0','C');
			$this->Cell(10,10,'','','0');
			$this->Cell(30,10,'NO OF (TOTAL WORKER)','','0');
			$this->Cell(2,10,'','','0');
			$this->Cell(20,10,$slno-1,'','0','C');
			
			
			
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(10);  //summary
				$this->SetFont('Arial','B',8);	
			$this->Cell(40,10,'','','0');	
			$this->Cell(10,10,'','','0');
			$this->Cell(20,10,'A/C 1','','0');
			$this->Cell(20,10,'A/C 2','','0');
			$this->Cell(20,10,'A/C 10','','0');
			$this->Cell(20,10,'A/C 21','','0');
			$this->Cell(20,10,'A/C 22','','0');
			$this->Cell(20,10,'TOTAL','','0');
							$this->SetFont('Arial','',8);
		$this->Ln(5);
		$total_1=0;
		$total_2=0;
		$total_3=0;
		$total_4=0;
		$total_5=0;
		$total_6=0;
		
		$query_for_pf_rule=mysql_query("select AcNo2E as q1,AcNo21E as q2 ,AcNo22E as q3 from govtpfrule order by id desc");
		$row_for_pf_rule=mysql_fetch_array($query_for_pf_rule);
		$acno2e=$row_for_pf_rule['q1'];
		$acno21e=$row_for_pf_rule['q2'];
		$acno22e=$row_for_pf_rule['q3'];
		
		$this->Cell(40,10,"1: EMPLOYER'S SHARE OF CONT.",'','0');
			$this->Cell(10,10,'','','0');
			$this->Cell('20',10,$total_pf-$total_fpf,'','0');
			$employer_share_contri=$total_pf-$total_fpf;
			$total_1+=$total_pf-$total_fpf;
			$this->Cell('20',10,'','','0');
			$this->Cell('20',10,$total_fpf,'','0');
			$total_3+=$total_fpf;
			$this->Cell('20',10,round(($acno21e*$ac21wage)/100),'','0');
			$total_4+=round(($acno21e*$ac21wage)/100);
			$this->Cell('20',10,round(($ac21wage*$acno22e)/100),'','0');
			$total_5+=round(($ac21wage*$acno22e)/100);
			$this->Cell('16',10,$total_pf+round(($acno21e*$ac21wage)/100)+round(($ac21wage*$acno22e)/100),'','0');
			$total_6+=$total_pf+round(($acno21e*$ac21wage)/100)+round(($ac21wage*$acno22e)/100);
				$this->Ln(5);
						$this->Cell(40,10,"2: EMPLOYEE'S SHARE OF CONT.",'','0');
				$this->Cell(10,10,'','','0');
			$this->Cell(20,10,$total_pf,'','0');
			$employee_share_contri=$total_pf;
			$total_1+=$total_pf;
						$this->Cell('20',10,'','','0');
			$this->Cell('20',10,'','','0');
			$this->Cell('20',10,'','','0');
			$this->Cell('20',10,'','','0');
			$this->Cell('16',10,$total_pf,'','0');
			$total_6+=$total_pf;
			
			$this->Ln(5);
					$this->Cell(40,10,"3: ADMIN CHARGES",'','0');
			$this->Cell(10,10,'','','0');
			$this->Cell(20,10,'','','0');
			$this->Cell('20',10,round(($total_gross*$acno2e)/100),'','0');
			$total_2+=round(($total_gross*$acno2e)/100);
			$this->Cell('20',10,'','','0');
			$this->Cell('20',10,'','','0');
			$this->Cell('20',10,'','','0');
			$this->Cell('16',10,round(($total_gross*$acno2e)/100),'','0');
			$total_6+=round(($total_gross*$acno2e)/100);
			
			$this->Ln(1);
			$this->Cell(10,10,'','','0');
			$this->Cell(115+40,10,'','B','0'); // line break
			$this->Ln(1);
			$this->Cell(10,10,'','','0');
			$this->Cell(115+40,10,'','B','0'); // line break
			
			$this->Ln(10);
			$this->Cell(40,10,'','','0');
			$this->Cell(10,10,'','','0');
			$this->Cell(20,10,$total_1,'','0');
			$this->Cell('20',10,$total_2,'','0');
			$this->Cell('20',10,$total_3,'','0');
			$this->Cell('20',10,$total_4,'','0');
			$this->Cell('20',10,$total_5,'','0');
			$this->Cell('16',10,$total_6,'','0');
			
			$this->Ln(1);
			$this->Cell(10,10,'','','0');
			$this->Cell(115+40,10,'','B','0'); // line break
			$this->Ln(1);
			$this->Cell(10,10,'','','0');
			$this->Cell(115+40,10,'','B','0'); // line break
			 // ac 1 ac 2 ac 10 ac 21 ac 22 total
			$this->Ln(10);		
			$this->Cell(20,10,'','','0');	
			$this->Cell(20,10,'PAST','','0');
			$this->Cell(20,10,'ADD','','0');	
			$this->Cell(20,10,'LEFT','','0');	
			$this->Cell(20,10,'PRESENT','','0');			
			
			$this->Ln(10);	
			
			$query_for_workers_stats=mysql_query("select workerId from transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and transaction_master.statusEpfYN='1' ");	
			$past=0;
			$add=0;
			$present=0;
			$left=0;
				
			$previous_month;
			$previous_year;
			
			if($this->month==1)
				{					$previous_month=12;		$previous_year=$this->year-1;
				}
			else {	$previous_month=$this->month-1; $previous_year=$this->year;			}
			
			$query_for_previous_month=mysql_query("select id from transaction_master where month='$previous_month' and year='$previous_year' and clientId='$this->clientid' and transaction_master.statusEpfYN='1' and transaction_master.pfReasonCode='-8' ");
			$past=mysql_num_rows($query_for_previous_month);
			$this->Cell(20,10,'','','0');	
			$this->Cell(20,10,$past,'','0');
			
			$query_for_left=mysql_query("select id from transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and lastdateepf!='' and transaction_master.statusEpfYN='1' ");
			$left=mysql_num_rows($query_for_left);
		
			$query_for_present=mysql_query("select id from transaction_master where month='$this->month' and year='$this->year' and clientId='$this->clientid' and lastdateepf='' and transaction_master.statusEpfYN='1' ");
			$present=mysql_num_rows($query_for_present);
		
			while($row_for_workers_stats=mysql_fetch_array($query_for_workers_stats)){
			$w_id=$row_for_workers_stats['workerId'];
			$query_for_worker_previous_presence_checking=mysql_query("select id from transaction_master where month='$previous_month' and year='$previous_year' and workerId='$w_id' and transaction_master.statusEpfYN='1' ");	
			if(mysql_num_rows($query_for_worker_previous_presence_checking)>0){
			//	$past++;	
			}
			else $add++;
			//$present++;
				}
				
			
			$this->Cell(20,10,$add,'','0');	
			$this->Cell(20,10,$left,'','0');	
			$this->Cell(20,10,$present,'','0');			
			if($present!=($past+$add-$left))	
			$this->Cell(20,10,"ERROR: PRESENT IS NOT EQUAL TO PAST + ADD - LEFT",'','0');
			
			 // past + add + left + present
			
			$query=mysql_query("select id from crpfchallan where clientId='$this->clientid' and month='$this->month' and year='$this->year' ");		 
			if(mysql_num_rows($query)==0){
			 $query="INSERT INTO `crpfchallan`(`id`, `clientId`, `ruleId`, `month`,year, `type`, `totalWorker`, `totalcontWorker`, `totalWage`, `totalContWage`, `epfWage`, `epsWage`, `pf21Wage`, `ac1W`, `ac1E`, `ac2E`, `ac10E`, `ac21E`, `ac22E`, `challanAmount`) VALUES ('','$this->clientid','','$this->month','$this->year','','$slno-1','0','$total_gross','','$ac21wage','$ac10wage','','$employer_share_contri','$employee_share_contri','$total_2','$total_3','$total_4','$total_5','$total_6')";
			 mysql_query($query);
			}
			// echo $query;
			
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','0'); // line break
			$this->Ln(10);
			
			 // line break
			
			$this->Cell(20,10,'FORM  # 5(ADD)','','0');	
			$this->Ln(5);
			$this->Cell(20,10,'','','0');
				$this->Cell(40,10,"Name",'','0');
				
				
				$this->Cell(40,10,"Father/Husband Name",'','0');
				
				$this->Cell(30,10,"PF Ac No",'','0');
				$this->Cell(20,10,"DOB",'','0');			
				$this->Cell(20,10,"DOJ",'','0');
				$this->Ln(5);
			$query_for_present_members=mysql_query("select workerId from transaction_master where  month='$this->month' and year='$this->year' and clientId='$this->clientid' and transaction_master.statusEpfYN='1' ");
//			echo "<br>select workerId from transaction_master where  month='$this->month' and year='$this->year' and clientId='$this->clientid' <br>";
			if(mysql_num_rows($query_for_present_members)>0){
				while($row_for_present_members=mysql_fetch_array($query_for_present_members)){
					
					$w_id=$row_for_present_members['workerId'];
			$query_for_worker_previous_presence_checking=mysql_query("select id from transaction_master where month='$previous_month' and year='$previous_year' and workerId='$w_id' ");	
			if(mysql_num_rows($query_for_worker_previous_presence_checking)==0){
						
					$new_worker_detail=mysql_query("select workerdetail.name,workerdetail.FH,workerdetail.fName,workerdetail.hName,workerdetail.dob,workerdetail.dojEpf,workerdetail.pfNo from workerdetail where id='$w_id' and epfYN='1'");
					//echo "select workerdetail.name,workerdetail.FH,workerdetail.fName,workerdetail.hName,workerdetail.dob,workerdetail.dojEpf,workerdetail.epfNo from workerdetail where id='$w_id'";
			if(mysql_num_rows($new_worker_detail)>0){
				
				$row_for_new_worker_detail=mysql_fetch_array($new_worker_detail);
				$name=$row_for_new_worker_detail['name'];
				$fh=$row_for_new_worker_detail['FH'];
				$fName=$row_for_new_worker_detail['fName'];
				$hName=$row_for_new_worker_detail['hName'];
				$dob=$row_for_new_worker_detail['dob'];
				$dojEpf=$row_for_new_worker_detail['dojEpf'];
				$epfNo=$row_for_new_worker_detail['pfNo'];
				$this->Cell(20,10,'','','0');
				$this->Cell(40,10,$name,'','0');
				
				if(strcmp($fh,"F")==0)
				$this->Cell(40,10,$fName,'','0');
				else $this->Cell(40,10,$hName,'','0');
				$this->Cell(30,10,$epfNo,'','0');
				$this->Cell(20,10,$dob,'','0');			
				$this->Cell(20,10,$dojEpf,'','0');
				$this->Ln(5);
			}
			
					
					
			}	
					
				}
				
			}
			 // form number 5
						 
			 
			$this->Ln(5);
 			$this->Cell(20,10,'FORM # 10(LEFT)','',0);
			$this->Ln(5);
			$this->Cell(20,10,'','',0);
			$this->Cell(40,10,'Name','',0);
			$this->Cell(40,10,'Father/Husband Name','',0);
			$this->Cell(30,10,'PF Ac No','',0);
			$this->Cell(20,10,'Left Date','',0);
			$this->Cell(20,10,'Left Reason','',0);
			$this->Ln(5);

			
			$query_for_present_members=mysql_query("select workerId from transaction_master where  month='$this->month' and year='$this->year' and clientId='$this->clientid' and lastdateepf!=''");

			if(mysql_num_rows($query_for_present_members)>0){
				while($row_for_present_members=mysql_fetch_array($query_for_present_members)){
					$w_id=$row_for_present_members['workerId'];
					$query="select workerdetail.name,workerdetail.FH,workerdetail.fName,workerdetail.hName,workerdetail.pfNo,transaction_master.lastdateepf,transaction_master.pfReasonCode from  workerdetail inner join transaction_master on transaction_master.workerId=workerdetail.id and workerdetail.id='$w_id' and transaction_master.month='$this->month' and transaction_master.year='$this->year' and transaction_master.clientId='$this->clientid' and workerdetail.epfYN='1'";
					$new_worker_detail=mysql_query($query);
					//echo $query;
					if(mysql_num_rows($new_worker_detail)>0){
						$row_for_new_worker_detail=mysql_fetch_array($new_worker_detail);
						$name=$row_for_new_worker_detail['name'];
						$fh=$row_for_new_worker_detail['FH'];
						$fName=$row_for_new_worker_detail['fName'];
						$hName=$row_for_new_worker_detail['hName'];
						$left_date=$row_for_new_worker_detail['lastdateepf'];
						$exit_reason=$row_for_new_worker_detail['pfReasonCode'];
						if( strcmp($exit_reason,"-8")==0)
							$exit_reason="Exit Reason Not filled";
						$epfNo=$row_for_new_worker_detail['pfNo'];
						$this->Cell(20,10,'','','0');
						$this->Cell(40,10,$name,'','0');
						if(strcmp($fh,"F")==0)
							$this->Cell(40,10,$fName,'','0');
						else $this->Cell(40,10,$hName,'','0');
						$this->Cell(30,10,$epfNo,'','0');
						$this->Cell(30,10,$left_date,'','0');
						$this->Cell(30,10,$exit_reason,'','0');
						$this->Ln(5);
					}		
				}
				
			} // form number 10
			
			
			
			$this->Cell(195,10,'','B'); // line break
			$this->Ln(1);
			$this->Cell(195,10,'','B','1'); // line break
			$this->Ln(5);
			$this->Cell(20,10,'UPLOAD DATE','','0','R');	
			$this->Cell(50,10,'','B','R');	
			$this->Cell(20,10,'CHALLAN NUMBER','','0','R');		
			$this->Cell(50,10,'','B','R');
			$this->Cell(20,10,'UPLOADED BY','','0','R');		
			$this->Cell(50,10,'','B','R');
			
		
	}
	
	
}


function Footer()
{
	$this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
} //footer

function heading(){
	$month=array('','JAN','FEB','MAR','APR','MAY','JUNE','JULY','AUG','SEPT','OCT','NOV','DEC');
	$companyname="Company Name Here";
	$query=mysql_query("SELECT clientdetails.areaCode,clientdetails.address,clientdetails.city,clientdetails.state,clientdetails.name, cesidetail.esiNo FROM clientdetails INNER JOIN cesidetail ON cesidetail.clientId = clientdetails.id WHERE clientdetails.id ='$this->clientid'");
	if(mysql_num_rows($query)>0){
		$row=mysql_fetch_array($query);
		
		$companyname=$row['name'];	
		$esino=$row['esiNo'];
		
		$query_for_epf_no=mysql_query("select pfNo from cpfdetail where clientId='$this->clientid'");
		$row_for_epf_no=mysql_fetch_array($query_for_epf_no);
		
		$epfno=$row_for_epf_no['pfNo'];
		$address=$row['address'];
		$city=$row['city'];
		$state=$row['state'];
		$areacode=$row['areaCode'];
		
		if(strlen($companyname)>25){

			$companyname=(substr($companyname,0,25)."...");	
		}
		if(strlen($address)>25) $address=substr($address,0,25)."...";
	}
	//
	
	$this->SetXY(0,0);
	$this->Cell(0,10,"-",0,0);
	$this->Ln(2);
	
	$this->SetFont('Arial','',10);
	$this->Cell(130,10,strtoupper($companyname),0,0); // company name + date 

	
	
	


	$this->Cell(60,10,'PF A/C NO :- '.$epfno,0,0);
	
	$this->Ln(4);
	
	$this->Cell(100,10,"EMPLOYEE PF & EPS STATEMENT FOR THE MONTH  OF ",0,0);
	$this->Cell(10,10,$month[$this->month],0,0);
	$this->Cell(20,10,$this->year,0,0);
	$query_for_area=mysql_query("select area_name from carea_code where area_code='$areacode'");
	if(mysql_num_rows($query_for_area)>0){
		$row_code=mysql_fetch_array($query_for_area);
		$areaname=$row_code['area_name'];	
	}
	else $areaname="INPUT AREA";
	$this->Cell(60,10,'AREA   :- '.strtoupper($areaname),0,0);
	
	
	$this->Ln(1);		
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(10);
	
	
} // heading



function topheading()
{
	$height_of_the_pdf_file=$this->h;
	$weight_of_the_pdf_file=$this->w;
	$cor_y_bottom;
	$padding=0;
	
	 //pdf file height,width
	$this->heading(); // top heading
	
	$top_1_width=14;
	$top_1_height=5;
	
	$this->SetFont('Arial','B',8);	
	$this->Cell(10,10,'S.NO.','',0); // Serial Number
	$this->Cell(11,10,'CODE','',0);
	$this->Cell(38,10,'NAME OF THE EMPLOYEE','',0);
	$this->Cell(16,10,'J_DATE','',0);
	$this->Cell(16,10,'L_DATE','',0);
	$this->Cell(20,10,'PF AC NO ','',0);
	$this->Cell(16,10,'WAGE','',0,'R');
	$this->Cell(16,10,'PF','',0,'R');
	$this->Cell(16,10,'E.PNS','',0,'R');
	$this->Cell(16,10,'E.PF','',0,'R');
	$this->Cell(7,10,'L-R','',0);
	$this->Cell(17,10,'brk days','',0);
	
	
	$this->Ln(1);
		
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(1);
	$this->Cell(195,10,'','B'); // line break
	$this->Ln(15);
	
	
} // top heading

		
		
}


$pdf = new PDF();

$pdf->init();
$pdf->SetFont('Arial','',10);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->topheading();

$pdf->show_data();
//$destination=$_SERVER['DOCUMENT_ROOT'].'pijain/reports/';

$filename=$pdf->month."_".$pdf->year.'.txt';

$query_for_id=mysql_query("select id from transaction_master where clientId='$pdf->clientid' and month='$pdf->month' and year='$pdf->year'");
if(mysql_num_rows($query_for_id)>0){
$row_id=mysql_fetch_array($query_for_id);
$id=$row_id['id'];

makeECRfile($filename,$id);

if(array_key_exists('request',$_GET)){
$query=mysql_query("select path from epf_file_path where clientId='$pdf->clientid' and type='4'");
if(mysql_num_rows($query)>0){
	$row=mysql_fetch_array($query);
	$path=$row['path'];	
}
else {
	$query=mysql_query("select name from clientdetails where id='$pdf->clientid'");
	$row=mysql_fetch_array($query);
	$name=$row['name'];
	$directory_path="data/".$name."_".$pdf->clientid."/"."PF/";
	$directory_created=mkdir($directory_path."REPORT",0777,true);
	if($directory_created==false)
	{
	echo "Something bad happen";
	//	return;
	}
	$path=$directory_path."REPORT/";
	mysql_query("insert into epf_file_path(clientId,path,type)values('$pdf->clientid','$path','4') ");	
	
}
$request=$_REQUEST['request'];
$pdf->Output($path.$pdf->month."_".$pdf->year.".pdf");
echo $path.$pdf->month."_".$pdf->year.".pdf";
			
		
}
else 
$pdf->Output();



}else echo "No data in Transaction File";




?>